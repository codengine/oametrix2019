#!/usr/bin/env bash

cd /apps/oametrix/backend/oametrix2019/openaccess
source /apps/oametrix/backend/ENV/bin/activate

pip install -r requirements.txt
python manage.py migrate
python manage.py openaccess_init_data
python manage.py openaccess_init_data2

