from rest_framework.schemas.generators import distribute_links, insert_into, LinkNode
from rest_framework.compat import coreapi
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.schemas import SchemaGenerator
from rest_framework.views import APIView
from rest_framework_swagger import renderers


def get_links(self, request=None):
    """
    Return a dictionary containing all the links that should be
    included in the API schema.
    """
    links = LinkNode()

    # Generate (path, method, view) given (path, method, callback).
    paths = []
    view_endpoints = []
    for path, method, callback in self.endpoints:
        view = self.create_view(callback, method, request)
        path = self.coerce_path(path, method, view)
        paths.append(path)
        view_endpoints.append((path, method, view))

    # Only generate the path prefix for paths that will be included
    if not paths:
        return None
    prefix = self.determine_path_prefix(paths)

    for path, method, view in view_endpoints:
        # if not self.has_view_permissions(path, method, view):
        #     continue
        link = view.schema.get_link(path, method, base_url=self.url)
        subpath = path[len(prefix):]
        keys = self.get_keys(subpath, method, view)
        insert_into(links, keys, link)

    return links


def get_schema(self, request=None, public=False):
    """
    Generate a `coreapi.Document` representing the API schema.
    """
    if self.endpoints is None:
        inspector = self.endpoint_inspector_cls(self.patterns, self.urlconf)
        self.endpoints = inspector.get_api_endpoints()

    links = get_links(self, None if public else request)
    if not links:
        return None

    url = self.url
    if not url and request is not None:
        url = request.build_absolute_uri()

    distribute_links(links)

    return coreapi.Document(
        title=self.title, description=self.description,
        url=url, content=links
    )


class SwaggerSchemaView(APIView):
    permission_classes = [AllowAny]
    renderer_classes = [
        renderers.OpenAPIRenderer,
        renderers.SwaggerUIRenderer
    ]

    def get(self, request):
        generator = SchemaGenerator(title='Open Access API')
        schema = get_schema(generator, request=request)

        return Response(schema)
