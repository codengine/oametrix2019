
import os

from core.openaccessio.file_manager import FileManager

PROJECT_PATH = os.path.abspath(".")

# Read the settings.json file

settings_json_path = os.path.join(PROJECT_PATH, "settings.json")

settings_json = FileManager.read_json(settings_json_path)

default_env = "DEV"

deployment_mode = settings_json.get('mode', default_env)

if deployment_mode == 'DEV':
    from config.settings.dev import *
elif deployment_mode == 'STAGE':
    from config.settings.stage import *
elif deployment_mode == "PROD":
    from config.settings.prod import *



