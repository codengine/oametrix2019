from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.urls import path
from django.conf.urls import include
from swagger import SwaggerSchemaView

urlpatterns = [
    path('', SwaggerSchemaView.as_view()),
    path('admin/', admin.site.urls),
    url(r'^api/%s/' % settings.API_VERSION, include("openaccess.api.urls")),
]
