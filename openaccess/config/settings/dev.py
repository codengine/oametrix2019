print("Running in DEV")

# Development environment settings
DEBUG = True

from config.settings.base import *

# ALLOWED_HOSTS = ['localhost', '127.0.0.1']

ALLOWED_HOSTS += ['dev.api.oametrix.io', 'api.dev.oametrix.io']

