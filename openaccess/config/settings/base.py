import os

from core.openaccessio.file_manager import FileManager

PROJECT_PATH = os.path.abspath(".")

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SITE_ROOT = os.path.dirname(os.path.abspath(__file__))

SECRET_KEY = '34ml4ru)-r6tnabvuwdxmo*_xq4kjgan23vi69qx80ufnhz3&-'

ALLOW_AJAX_ONLY = True  # If the API should allow ajax request only
ENABLE_CORS = True  # If CORS should be enabled for cross site requests


STATIC_ROOT = os.path.join(SITE_ROOT, 'static')

STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    # os.path.join(SITE_ROOT, 'static'),
)

ALLOWED_HOSTS = ['localhost', '127.0.0.1']

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'rest_framework_swagger',
    'rest_framework.authtoken',
    'django_global_request'
]

if ENABLE_CORS:
    INSTALLED_APPS += ['corsheaders']

from config.apps import OA_APPS

INSTALLED_APPS += OA_APPS



MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware'
]

if ENABLE_CORS:
    MIDDLEWARE += ['corsheaders.middleware.CorsMiddleware']
MIDDLEWARE += [
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'core.middlewares.logging.LoggingMiddleware',
    'django_global_request.middleware.GlobalRequestMiddleware'
]

ROOT_URLCONF = 'urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'wsgi.application'

# ===================== Database Configuration Start =======================

try:
    import pymysql
    pymysql.install_as_MySQLdb()
except ImportError:
    pass

settings_json_path = os.path.join(PROJECT_PATH, "settings.json")

settings_json = FileManager.read_json(settings_json_path)

DATABASE = settings_json.get('DATABASE', {})

DB_HOST_NAME = DATABASE.get('host', '')
DB_PORT = DATABASE.get('port', '')
DB_USER = DATABASE.get('user', '')
DB_PASSWORD = DATABASE.get('password', '')
DB_NAME = DATABASE.get('name', '')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql', #'django.db.backends.mysql',
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'HOST': DB_HOST_NAME,
        'PORT': DB_PORT,
        'OPTIONS': {
            # 'init_command': 'SET sql_mode="STRICT_TRANS_TABLES"',
            # 'charset': 'utf8mb4'
        }
    }
}

# ======================== Database Configuration End ===========================

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTHENTICATION_BACKENDS = (
    'core.db.backends.authentication.OpenAccessAuthBackend',
    'django.contrib.auth.backends.ModelBackend',
)

SITE_ID = 1

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',),

    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
    ),
    'DEFAULT_PAGINATION_CLASS': 'core.api.pagination.oa_pagination.OAStandardPageNumberPagination',
    'PAGE_SIZE': 20,
    'DEFAULT_METADATA_CLASS': None,
    # 'DEFAULT_THROTTLE_CLASSES': (
    #     'rest_framework.throttling.AnonRateThrottle',
    #     'rest_framework.throttling.UserRateThrottle'
    # ),
    # 'DEFAULT_THROTTLE_RATES': {
    #     'anon': '1/hour',
    #     'user': '30/hour'
    # }
}

# SWAGGER SETTINGS
SWAGGER_SETTINGS = {
    'SECURITY_DEFINITIONS': {
        'api_key': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'Authorization'
        }
    },
}

API_VERSION = "v1"

LOG_ENABLED = True

# =========================== LOGGING CONFIGURATION START ==============================

import logging.config

LOG_DIRECTORY = PROJECT_PATH + "/log/"
if not os.path.exists(LOG_DIRECTORY):
    os.makedirs(LOG_DIRECTORY)

LOGGING_CONFIG = None

app_loggers = {app_name: {
    'handlers': ['console_dev', 'log_file_production_warning', 'log_file_production_error', 'mail_admins'],
    'level': 'INFO',
    'propagate': True,
} for app_name in OA_APPS}

app_loggers['django'] = {
    'handlers': ['console_dev', 'log_file_production_warning', 'log_file_production_error', 'mail_admins'],
    'propagate': True,
    'level': 'WARNING',
}

logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        }
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        },
        'log_file_dev': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': LOG_DIRECTORY + "dev_log.log",
            'maxBytes': 50000,
            'backupCount': 20,
            'formatter': 'verbose',
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'log_file_production_warning': {
            'level': 'WARNING',
            'filters': ['require_debug_false'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': LOG_DIRECTORY + "prd_log.log",
            'maxBytes': 50000,
            'backupCount': 20,
            'formatter': 'verbose',
        },
        'log_file_production_error': {
            'level': 'WARNING',
            'filters': ['require_debug_false'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': LOG_DIRECTORY + "prd_log.log",
            'maxBytes': 50000,
            'backupCount': 20,
            'formatter': 'verbose',
        },
        'console_dev': {
            'level': 'INFO',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        }
    },
    'formatters': {
        'file': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'default': {
            'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'
        },
        'print_message': {
            'format': '%(message)s'
        }
    },
    'loggers': app_loggers
})

# =========================== LOGGING CONFIGURATION END ==============================


# CORS Config
if ENABLE_CORS:
    # CORS_ORIGIN_WHITELIST = (
    #     'google.com',
    #     'hostname.example.com'
    # )
    CORS_ORIGIN_ALLOW_ALL = True
    CORS_ALLOW_METHODS = (
        'GET',
        'POST',
        'PUT',
        'PATCH',
        'DELETE',
        'OPTIONS'
    )
    CORS_ALLOW_HEADERS = (
        'x-requested-with',
        'content-type',
        'accept',
        'origin',
        'authorization',
        'x-csrftoken'
    )

AUTH_USER_MODEL = 'core.User'

FRONTEND_URL = settings_json.get('FRONTEND_URL', '')


ACCOUNT_ACTIVATION_URL = FRONTEND_URL + '/active-account?security_code='
RESET_PASSWORD_URL = FRONTEND_URL + '/reset-password?security_code='

# Celery Config
# CELERY_BROKER_URL = settings_json.get('CELERY_BROKER', 'amqp://guest:guest@localhost//')
# CELERY_ACCEPT_CONTENT = ['json']
# CELERY_TASK_SERIALIZER = 'json'

EMAIL_CONFIG = settings_json.get('EMAIL_CONFIG', {})

EMAIL_HOST = EMAIL_CONFIG.get('email_host', 'dime126.dizinc.com')
EMAIL_PORT = EMAIL_CONFIG.get('email_port', '465')
EMAIL_HOST_USER = EMAIL_CONFIG.get('email_host_user', 'info@hisab-nikash.com')
EMAIL_HOST_PASSWORD = EMAIL_CONFIG.get('email_host_password', 'ddsemail2016')

NGROK_HOST = settings_json.get('NGROK_HOST', '')
ALLOWED_HOSTS += [NGROK_HOST]

# CELERY STUFF
BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'