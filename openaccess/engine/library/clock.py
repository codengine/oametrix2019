from datetime import datetime


class Clock(object):
    @classmethod
    def utc_now(cls):
        return datetime.utcnow()

    @classmethod
    def start_of_year(cls):
        return datetime.utcnow().replace(month=1, day=1, hour=0, minute=0, second=0)

    @classmethod
    def end_of_year(cls):
        return datetime.utcnow().replace(month=12, day=31, hour=23, minute=59, second=59)