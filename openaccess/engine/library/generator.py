import uuid


class Generator(object):
    @staticmethod
    def generate_unique_token(token_length=10):
        return uuid.uuid4().hex[:token_length]

if __name__ == "__main__":
    token = Generator.generate_unique_token()
    print(token)

