import inspect
from django.apps import apps
from config.apps import OA_APPS
from core.models.base_entity import BaseEntity
import smtplib
from email.mime.text import MIMEText
from django.conf import settings


def get_installed_apps():
    app_labels = [a.label for a in apps.app_configs.values()]
    return app_labels


def get_all_models(app_name=None):
    app_models = {}
    for app in OA_APPS:
        if app_name and app_name != app:
            continue
        all_models = apps.get_models(app)
        oa_models = []
        for m in all_models:
            parents = inspect.getmro(m)
            if BaseEntity in parents:
                if BaseEntity in parents and not inspect.isabstract(m):
                    oa_models += [m]
        app_models[app] = oa_models
    return app_models


class Mailer():

    def send_email(self, recipient, message):
        try:
            # START confirmation email

            sender = 'no-reply@oametrix.com'
            message = message
            msg = MIMEText(message, 'html')
            msg['Subject'] = "OaMetrix: Registration"
            msg['From'] = sender
            msg['To'] = recipient

            server = smtplib.SMTP_SSL(settings.EMAIL_HOST, settings.EMAIL_PORT)
            server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
            server.sendmail(sender, [recipient], msg.as_string())
            server.quit()

            # END confirmation email
            return True
        except:
            return False
