# Generated by Django 2.1.2 on 2019-02-17 15:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('openaccess', '0003_auto_20190215_1956'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='apcfundrequest',
            name='created_by',
        ),
        migrations.RemoveField(
            model_name='apcfundrequest',
            name='created_by_group',
        ),
        migrations.RemoveField(
            model_name='apcfundrequest',
            name='created_by_role',
        ),
        migrations.RemoveField(
            model_name='apcfundrequest',
            name='updated_by',
        ),
        migrations.RemoveField(
            model_name='apcfundrequest',
            name='updated_by_group',
        ),
        migrations.RemoveField(
            model_name='apcfundrequest',
            name='updated_by_role',
        ),
        migrations.DeleteModel(
            name='APCFundRequest',
        ),
    ]
