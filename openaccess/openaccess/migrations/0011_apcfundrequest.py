# Generated by Django 2.1.2 on 2019-02-25 13:25

import core.api.mixins.modelmixins.filter_model_mixin
import core.api.mixins.modelmixins.routable_model_mixin
import core.api.mixins.modelmixins.searchable_model_mixin
import core.api.mixins.modelmixins.serializer_model_mixin
import core.api.mixins.viewmixins.viewsets_post_callback_methods_mixin
import core.mixins.modelmixin.common_methods_mixin
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20190215_0124'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('openaccess', '0010_articlefull_grant_number'),
    ]

    operations = [
        migrations.CreateModel(
            name='APCFundRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField()),
                ('date_updated', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(default=False)),
                ('approval_status', models.CharField(default='pending', max_length=200)),
                ('approve_date', models.DateTimeField(blank=True, null=True)),
                ('year', models.IntegerField(max_length=10)),
                ('publish_fee', models.TextField(blank=True, null=True)),
                ('read_fee', models.TextField(blank=True, null=True)),
                ('amount', models.DecimalField(decimal_places=3, max_digits=20)),
                ('activities', models.ManyToManyField(to='core.ApprovalActivity')),
                ('approved_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('approved_by_group', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='core.Group')),
                ('approved_by_role', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='core.Role')),
                ('counter_organisation', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='core.Organisation')),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('created_by_group', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='core.Group')),
                ('created_by_role', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='core.Role')),
                ('currency', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='core.Currency')),
                ('organisation', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='core.Organisation')),
                ('updated_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('updated_by_group', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='core.Group')),
                ('updated_by_role', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='core.Role')),
            ],
            options={
                'abstract': False,
                'ordering': ['id'],
            },
            bases=(models.Model, core.mixins.modelmixin.common_methods_mixin.CommonMethodsMixin, core.api.mixins.modelmixins.routable_model_mixin.RoutableModelMixin, core.api.mixins.viewmixins.viewsets_post_callback_methods_mixin.ViewSetsPostCallBackMethodsMixin, core.api.mixins.modelmixins.serializer_model_mixin.SerializerModelMixin, core.api.mixins.modelmixins.searchable_model_mixin.SearchableModelMixin, core.api.mixins.modelmixins.filter_model_mixin.FilterDataFilterMixin),
        ),
    ]
