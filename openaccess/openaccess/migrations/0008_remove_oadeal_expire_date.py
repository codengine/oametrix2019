# Generated by Django 2.1.2 on 2019-02-18 12:13

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('openaccess', '0007_auto_20190218_1144'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='oadeal',
            name='expire_date',
        ),
    ]
