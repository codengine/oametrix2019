import logging
from django.core.management.base import BaseCommand
from core.models.organisation.organisation import Organisation
from openaccess.models.finance.organisation_wallet import OrganisationWallet

__author__ = 'Sohel'

logger = logging.getLogger('core')


class Command(BaseCommand):
    def handle(self, *args, **options):
        logger.info("Running init data2")

        all_organisation = Organisation.objects.all()
        for organisation_instance in all_organisation:
            logger.info("Adding wallet to organisation: %s" % organisation_instance.name)

            OrganisationWallet.add_new_wallet_to_organisation(organisation_instance)

            logger.info("Wallet added to: %s" % organisation_instance.name)

        logger.info("Init data 2 done.")




