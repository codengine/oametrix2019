from __future__ import absolute_import, unicode_literals
from celery import task
from celery.schedules import crontab
from datetime import timedelta


@task()
def hello_task():
    print("Hello Task!")
