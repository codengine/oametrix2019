from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class OpenAccessConfig(AppConfig):
    name='openaccess'
    verbose_name = _('openaccess config')

    def ready(self):
        import openaccess.signals.handlers


