from django.db import models
from core.models.base_entity import BaseEntity
from openaccess.models.article.content_type import ContentType
from rest_framework import serializers


class PublicationPrice(BaseEntity):
    content_type = models.ForeignKey(ContentType, on_delete=models.DO_NOTHING)
    pub_fee_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    pub_fee_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    pub_fee_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    submission_fee_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    submission_fee_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    submission_fee_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    colour_charge_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    colour_charge_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    colour_charge_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    page_charge_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    page_charge_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    page_charge_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    other_charge_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    other_charge_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    other_charge_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(PublicationPrice, cls).get_serializer()

        class PublicationPriceSerializer(BaseSerializer):
            content_type_name = serializers.SerializerMethodField()

            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'content_type', 'content_type_name', 'pub_fee_gbp', 'pub_fee_eur', 'pub_fee_usd', 'submission_fee_gbp',
                    'submission_fee_eur',
                    'submission_fee_usd', 'colour_charge_gbp', 'colour_charge_eur', 'colour_charge_usd',
                    'page_charge_gbp', 'page_charge_eur', 'page_charge_usd',
                    'other_charge_gbp', 'other_charge_eur', 'other_charge_usd', 'is_active')
                read_only_fields = ('id',)

            def get_content_type_name(self, obj):
                return obj.content_type.name

        return PublicationPriceSerializer
