from django.db import models
from core.models.base_entity import BaseEntity
from core.models.organisation.organisation import Organisation


class PublisherCorrectionRequestConfig(BaseEntity):
    publisher = models.ForeignKey(Organisation, on_delete=models.DO_NOTHING)
    days = models.IntegerField(default=0)

    @classmethod
    def get_allowed_correction_days(cls, publisher_instance):
        instances = PublisherCorrectionRequestConfig.objects.filter(publisher_id=publisher_instance.pk)
        if instances.exists():
            return instances.first().days
        return 10