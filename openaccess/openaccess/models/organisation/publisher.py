from django.db.models.query_utils import Q

from core.constants.view_action import ViewAction
from core.models.managers.generic_query_manager import GenericQueryManager
from core.models.organisation.organisation import Organisation


class Publisher(Organisation):

    objects = GenericQueryManager(filters=[Q(**{'domain__name':'Publisher'})])

    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        return []

    @classmethod
    def allow_view_actions(cls):
        return [ ViewAction.LIST.value ]

    @classmethod
    def permission_model(cls):
        return Organisation

    class Meta:
        proxy = True



