from django.db import models
from openaccess.models.article.article_abstract import ArticleAbstract
from openaccess.models.article.author import ArticleAuthor
from openaccess.models.article.content_type import ContentType
from openaccess.models.policy.licence import Licence


class Book(ArticleAbstract):
    publisher_name = models.CharField(max_length=1000)
    content_type = models.ForeignKey(ContentType, on_delete=models.DO_NOTHING)

    author = models.ForeignKey(ArticleAuthor, related_name='+', on_delete=models.DO_NOTHING)
    co_authors = models.ManyToManyField(ArticleAuthor, related_name='+')

    journal_name = models.CharField(max_length=1000, null=True, blank=True)
    journal_acronym = models.CharField(max_length=100, null=True, blank=True)

    pisbn = models.CharField(max_length=100, null=True, blank=True)
    eisbn = models.CharField(max_length=100, null=True, blank=True)
    funder_name = models.CharField(max_length=1000, null=True, blank=True)
    grant_number = models.CharField(max_length=1000, null=True, blank=True)
    fund_acknowledgement = models.CharField(max_length=1000, null=True, blank=True)

    pub_fee_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    pub_fee_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    pub_fee_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    submission_fee_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    submission_fee_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    submission_fee_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    colour_charge_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    colour_charge_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    colour_charge_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    page_charge_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    page_charge_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    page_charge_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    other_charge_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    other_charge_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    other_charge_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)

    submission_date = models.DateField(null=True, blank=True)
    acceptance_date = models.DateField(null=True, blank=True)
    pub_date = models.DateField(null=True, blank=True)

    status = models.CharField(max_length=100, null=True, blank=True)
    pages = models.IntegerField(null=True, blank=True)

    license = models.ForeignKey(Licence, null=True, blank=True, on_delete=models.DO_NOTHING)



