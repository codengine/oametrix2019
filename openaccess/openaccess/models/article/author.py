from django.db import models

from core.loader.loader import load_model
from core.models.base_entity import BaseEntity
from core.models.domain.segment import Segment
from django.db.models.query_utils import Q


class ArticleAuthor(BaseEntity):
    salutation = models.CharField(max_length=100, null=True, blank=True)
    first_name = models.CharField(max_length=200, null=True, blank=True)
    middle_name = models.CharField(max_length=200, null=True, blank=True)
    last_name = models.CharField(max_length=200, null=True, blank=True)
    email = models.EmailField(max_length=1000, null=True, blank=True)
    affiliation = models.CharField(max_length=2000, null=True, blank=True)
    affiliated_organisation = models.ForeignKey('core.Organisation', null=True, blank=True, on_delete=models.DO_NOTHING)
    position = models.CharField(max_length=1000, null=True, blank=True)
    department = models.ForeignKey(Segment, null=True, blank=True, on_delete=models.DO_NOTHING)
    orcid_id = models.CharField(max_length=1000, null=True, blank=True)
    pmc_id = models.CharField(max_length=1000, null=True, blank=True)

    def get_organisation(self):
        Organisation = load_model('core', 'Organisation')
        if self.affiliated_organisation:
            return self.affiliated_organisation
        else:
            if self.email:
                email_domain = self.email[self.email.index('@') + 1:]
                organisation_instances = Organisation.objects.filter(email_domain=email_domain)
                if organisation_instances.exists():
                    return organisation_instances.first()
            elif self.affiliation:
                organisation_instances = Organisation.objects.filter(name=self.affiliation)
                if organisation_instances.exists():
                    return organisation_instances.first()

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(ArticleAuthor, cls).get_serializer()

        class ArticleAuthorSerializer(BaseSerializer):
            department = Segment.get_serializer()(required = False)

            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'salutation', 'first_name', 'middle_name', 'last_name', 'email', 'affiliation', 'position',
                    'department', 'orcid_id', 'pmc_id', 'is_active')
                read_only_fields = ('id',)

        return ArticleAuthorSerializer

    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        user_domain_meta = user.domain_meta

        segment_id = None
        if user_domain_meta:
            segment = user_domain_meta.segment
            segment_id = segment.id
        filters = [
            Q(**{'department_id': segment_id})
        ]

        return filters
