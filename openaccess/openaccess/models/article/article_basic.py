from django.db import models
from openaccess.models.article.article_abstract import ArticleAbstract
from openaccess.models.article.author import ArticleAuthor
from openaccess.models.article.content_type import ContentType
from rest_framework import serializers


class ArticleBasic(ArticleAbstract):
    content_type = models.ForeignKey(ContentType, on_delete=models.DO_NOTHING)
    author = models.ForeignKey(ArticleAuthor, related_name='+', null=True, blank=True, on_delete=models.DO_NOTHING)
    co_authors = models.ManyToManyField(ArticleAuthor, related_name='+')
    journal_name = models.CharField(max_length=200, null=True, blank=True)
    journal_acronym = models.CharField(max_length=100, null=True, blank=True)
    publisher_name = models.CharField(max_length=1000, null=True, blank=True)
    funder_name = models.CharField(max_length=1000, null=True, blank=True)
    status = models.CharField(max_length=20, null=True, blank=True)
    pissn = models.CharField(max_length=100, null=True, blank=True)
    eissn = models.CharField(max_length=100, null=True, blank=True)
    fund_acknowledgement = models.TextField(null=True, blank=True)
    sub_system_acronym = models.CharField(max_length=100, null=True, blank=True)
    grant_number = models.CharField(max_length=100, null=True, blank=True)
    submission_date = models.DateField(null=True, blank=True)  # The date when article_reviewer was submitted
    acceptance_date = models.DateField(null=True, blank=True)  # approval date

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(ArticleBasic, cls).get_serializer()

        class ArticleBasicSerializer(BaseSerializer):
            author = ArticleAuthor.get_serializer()(required=False)
            co_authors = ArticleAuthor.get_serializer()(many=True, required=False)

            content_type_name = serializers.SerializerMethodField()

            class Meta(BaseSerializer.Meta):

                fields = (
                    'id', 'title', 'doi', 'article_id', 'content_type', 'content_type_name', 'author', 'co_authors', 'journal_name',
                    'journal_acronym', 'publisher_name',
                    'funder_name', 'status', 'pissn', 'eissn', 'fund_acknowledgement', 'sub_system_acronym',
                    'grant_number',
                    'submission_date', 'acceptance_date', 'is_active')
                read_only_fields = ('id',)

            def get_content_type_name(self, obj):
                return obj.content_type.name

        return ArticleBasicSerializer
