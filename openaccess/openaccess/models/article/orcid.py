from django.db import models
from core.models.base_entity import BaseEntity
from django.db.models.query_utils import Q


class Orcid(BaseEntity):
    salutation = models.CharField(max_length=100, null=True, blank=True)
    first_name = models.CharField(max_length=200, null=True, blank=True)
    middle_name = models.CharField(max_length=200, null=True, blank=True)
    last_name = models.CharField(max_length=200, null=True, blank=True)
    orcid_id = models.CharField(max_length=1000, null=True, blank=True)
    affiliation = models.CharField(max_length=2000, null=True, blank=True)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(Orcid, cls).get_serializer()

        class OrcidSerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = (
                'id', 'salutation', 'first_name', 'middle_name', 'last_name', 'orcid_id', 'affiliation', 'is_active')
                read_only_fields = ('id',)

        return OrcidSerializer


    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        user_meta = user.user_meta
        orcid_id = None
        if user_meta:
            orcid_id = user_meta.orcid_id
        filters = [
            Q(**{'orcid_id': orcid_id})
        ]
        return filters
