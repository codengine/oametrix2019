from django.db import models
from core.models.base_entity import BaseEntity
from openaccess.models.article.article_full import ArticleFull
from openaccess.models.contract.author_apc_fund_request import AuthorAPCFundRequest
from openaccess.models.contract.oa_deal import OADeal
from openaccess.models.policy.apc_option import APCOption
from openaccess.models.policy.licence import Licence
from rest_framework import serializers, status
from openaccess.models.finance.organisation_wallet import OrganisationWallet
from openaccess.models.finance.wallet import Wallet
from openaccess.models.policy.apc_option import APCOption
from django.db import transaction
from openaccess.models.finance.oa_token import OAToken
from core.constants.approval_status import ApprovalStatusEnum
from openaccess.models.transactions.deposit_credit_transaction import DepositCreditTransaction
from openaccess.models.transactions.offset_fund_transaction import OffsetFundTransaction
from openaccess.models.transactions.oa_token_transaction import OATokenTransaction
from openaccess.models.transactions.article_approve_transaction_breakdown import ArticleApproveTransactionBreakdown
from engine.library.clock import Clock


class ArticleApprove(BaseEntity):
    article = models.ForeignKey(ArticleFull, on_delete=models.DO_NOTHING)
    licence = models.ForeignKey(Licence, on_delete=models.DO_NOTHING)
    apc_option = models.ForeignKey(APCOption, on_delete=models.DO_NOTHING)
    approval_source = models.CharField(max_length=1000, null=True, blank=True)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(ArticleApprove, cls).get_serializer()

        class ArticleApproveSerializer(BaseSerializer):
            token_id = serializers.CharField(max_length=200, allow_blank=True, allow_null=True, required=False)
            breakdown = ArticleApproveTransactionBreakdown.get_serializer()(many=True, required=False)

            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'article', 'licence', 'apc_option', 'approval_source', 'breakdown', 'token_id', 'is_active')
                read_only_fields = ('id',)

            def is_valid(self, raise_exception=False):
                self.initial_data['is_active'] = True
                valid = super(ArticleApproveSerializer, self).is_valid(raise_exception=raise_exception)
                apc_option_id = self.initial_data['apc_option']
                apc_option_instance = APCOption.objects.get(pk=int(apc_option_id))
                if apc_option_instance.is_oa_token():
                    token_id = self.initial_data.get('token')
                    try:
                        token_id = int(token_id)
                        token_instance = OAToken.objects.get(pk=token_id)
                        if not token_instance.is_valid():
                            raise serializers.ValidationError({"error": "The provided token must be available for use"},
                                                              status.HTTP_400_BAD_REQUEST)
                    except Exception as exp:
                        raise serializers.ValidationError(
                            {"error": "Invalid token. Token must be supplied when apc option is chosen Token"},
                            status.HTTP_400_BAD_REQUEST)

                if apc_option_instance.is_deposit_fund():
                    currency = self.initial_data.get('currency')
                    if currency is None:
                        raise serializers.ValidationError(
                            {"error": "You must send currency"},
                            status.HTTP_400_BAD_REQUEST)

                return valid

            def create(self, validated_data):
                request = self.context['request']
                with transaction.atomic():

                    validated_data['is_active'] = True

                    breakdown_items = validated_data.pop('breakdown') or []

                    instance = super(ArticleApproveSerializer, self).create(validated_data)

                    for breakdown in breakdown_items:
                        article_instance = breakdown.get('article')
                        publisher_instance = article_instance.publisher
                        article_author = article_instance.author
                        author_institution = article_author.get_organisation()

                        pre_payment_deal_instance = OADeal.get_prepayment_deal(publisher_instance=publisher_instance,
                                                                               institution_instance=author_institution)

                        if not pre_payment_deal_instance:
                            raise serializers.ValidationError({"error": "OADeal does not exist between "
                                                                        "publisher and institution"},
                                                              status.HTTP_400_BAD_REQUEST)
                        breakdown['currency'] = pre_payment_deal_instance.currency
                        breakdown["is_active"] = True
                        breakdown.update({
                            "created_by": self.context['request'].user,
                            "created_by_group": self.context['request'].user.group,
                            "created_by_role": self.context['request'].user.role,
                        })
                        breakdown_instance = ArticleApproveTransactionBreakdown(**breakdown)
                        breakdown_instance.save()

                    # changing status
                    now_dt = Clock.utc_now().date()
                    article_instance = ArticleFull.objects.get(id=instance.article.pk)
                    article_instance.status = ApprovalStatusEnum.APPROVED.value
                    article_instance.approved_date = now_dt
                    article_instance.save()

                    author_affilicated_organisation = article_instance.author.get_organisation()
                    if not author_affilicated_organisation:
                        raise serializers.ValidationError({"error": "Author institution not found"},
                                                          status.HTTP_400_BAD_REQUEST)

                    pre_payment_deal_instance = OADeal.get_prepayment_deal(
                        publisher_instance=article_instance.publisher,
                        institution_instance=author_affilicated_organisation)

                    if not pre_payment_deal_instance:
                        raise serializers.ValidationError({"error": "OADeal does not exist between "
                                                                    "publisher and institution"},
                                                          status.HTTP_400_BAD_REQUEST)

                    currency_instance = pre_payment_deal_instance.currency

                    apc_option_instance = instance.apc_option
                    if apc_option_instance.is_deposit_fund():
                        # Now check if this user's organisation(institution) has sufficient fund in publisher's wallet
                        publisher_instance = article_instance.publisher

                        org_wallet_instances = OrganisationWallet.objects.filter(
                            organisation_id=publisher_instance.pk)
                        if not org_wallet_instances.exists():
                            raise serializers.ValidationError({"error": "No wallet found for publisher"},
                                                              status.HTTP_400_BAD_REQUEST)
                        org_wallet_instance = org_wallet_instances.first().wallet

                        total_charge = article_instance.get_total_charge_ammount(currency=self.initial_data['currency'],
                                                                                 pre_payment_deal_instance=pre_payment_deal_instance)

                        org_wallet_instance.debit_deposit_credit(organisation_instance=author_affilicated_organisation,
                                                                 currency_instance=currency_instance,
                                                                 balance=total_charge)
                        # Add the debit transaction now
                        dc_transaction = DepositCreditTransaction()
                        dc_transaction.add_debit_transaction(article_instance=article_instance,
                                                             currency_instance=currency_instance,
                                                             institution_instance=author_affilicated_organisation,
                                                             publisher_instance=article_instance.publisher,
                                                             amount=total_charge,
                                                             note='Article Approved by Institution admin')

                    elif apc_option_instance.is_offset_fund():
                        publisher_instance = article_instance.publisher
                        org_wallet_instances = OrganisationWallet.objects.filter(
                            organisation_id=publisher_instance.pk)
                        if not org_wallet_instances.exists():
                            raise serializers.ValidationError({"error": "No wallet found for publisher"},
                                                              status.HTTP_400_BAD_REQUEST)
                        org_wallet_instance = org_wallet_instances.first().wallet

                        total_charge = article_instance.get_total_charge_ammount(currency=self.initial_data['currency'],
                                                                                 pre_payment_deal_instance=pre_payment_deal_instance)

                        org_wallet_instance.debit_offset_fund_credit(
                            organisation_instance=author_affilicated_organisation,
                            currency_instance=currency_instance,
                            balance=total_charge)

                        of_transaction = OffsetFundTransaction()
                        of_transaction.add_debit_transaction(article_instance=article_instance,
                                                             currency_instance=currency_instance,
                                                             institution_instance=author_affilicated_organisation,
                                                             publisher_instance=article_instance.publisher,
                                                             amount=total_charge,
                                                             note='Article Approved by Institution admin')
                    elif apc_option_instance.is_oa_token():
                        token_instance = OAToken.objects.get(pk=int(self.initial_data['token']))
                        publisher_instance = instance.article.publisher
                        org_wallet_instances = OrganisationWallet.objects.filter(
                            organisation_id=instance.article.publisher.pk)
                        if not org_wallet_instances.exists():
                            raise serializers.ValidationError({"error": "No wallet found for publisher"},
                                                              status.HTTP_400_BAD_REQUEST)
                        org_wallet_instance = org_wallet_instances.first().wallet

                        article = self.validated_data['article']
                        institution_wallet_instances = OrganisationWallet.objects.filter(
                            organisation_id=article.author.get_organisation().pk)
                        if not institution_wallet_instances.exists():
                            raise serializers.ValidationError({"error": "No wallet found for Institution"},
                                                              status.HTTP_400_BAD_REQUEST)
                        institution_wallet_instance = institution_wallet_instances.first()
                        institution_wallet_instance.wallet.remove_token(token_instance)
                        token_instance.is_used = True
                        token_instance.save()
                        oa_token_transaction = OATokenTransaction()
                        oa_token_transaction.add_debit_transaction(article_instance=article_instance,
                                                                   institution_instance=author_affilicated_organisation,
                                                                   publisher_instance=article_instance.publisher,
                                                                   token_instance=token_instance,
                                                                   note='Article Approved by Institution admin')

                    # Now check whether was a APC Fund request for this article
                    author_fund_request_instances = AuthorAPCFundRequest.objects.filter(article_id=instance.article.pk)
                    if author_fund_request_instances.exists():
                        # Raise the signal here
                        pass

                    return instance

        return ArticleApproveSerializer
