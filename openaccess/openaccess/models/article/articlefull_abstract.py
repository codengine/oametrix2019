from django.db import models
from core.constants.approval_status import ApprovalStatusEnum
from openaccess.models.article.article_abstract import ArticleAbstract
from openaccess.models.article.content_type import ContentType
from core.models.organisation.organisation import Organisation
from openaccess.models.article.author import ArticleAuthor
from openaccess.models.policy.licence import Licence
from openaccess.models.publication.publication import Publication


class ArticleFullAbstract(ArticleAbstract):
    content_type = models.ForeignKey(ContentType, on_delete=models.DO_NOTHING)

    publisher = models.ForeignKey(Organisation, null=True, blank=True, related_name="+", on_delete=models.DO_NOTHING)
    publication = models.ForeignKey(Publication, on_delete=models.DO_NOTHING, null=True, blank=True)

    author = models.ForeignKey(ArticleAuthor, related_name='+', null=True, blank=True, on_delete=models.DO_NOTHING)
    co_authors = models.ManyToManyField(ArticleAuthor, related_name='+', blank=True)

    funder = models.ForeignKey(Organisation, null=True, blank=True, related_name="+", on_delete=models.DO_NOTHING)
    funder_name = models.CharField(max_length=1000, null=True, blank=True)
    grant_number = models.CharField(max_length=100, null=True, blank=True)

    submission_date = models.DateField(null=True, blank=True)  # The date when article_reviewer was submitted
    acceptance_date = models.DateField(null=True, blank=True)  # approval date

    pub_date = models.DateField(null=True, blank=True)
    vol_number = models.CharField(max_length=100, null=True, blank=True)
    issue_number = models.CharField(max_length=100, null=True, blank=True)
    page_no_from = models.IntegerField(null=True, blank=True)
    page_no_to = models.IntegerField(null=True, blank=True)

    licence = models.ForeignKey(Licence, null=True, blank=True, on_delete=models.CASCADE)

    impact_factor = models.CharField(max_length=20, null=True, blank=True)
    hindex = models.CharField(max_length=20, null=True, blank=True)
    indexed = models.CharField(max_length=20, null=True, blank=True)
    sherpa_romeo_info = models.CharField(max_length=300, null=True, blank=True)

    pub_fee_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    pub_fee_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    pub_fee_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    submission_fee_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    submission_fee_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    submission_fee_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    colour_charge_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    colour_charge_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    colour_charge_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    page_charge_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    page_charge_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    page_charge_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    other_charge_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    other_charge_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    other_charge_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)

    status = models.CharField(max_length=100, default=ApprovalStatusEnum.PENDING.value)
    approved_date = models.DateField(null=True, blank=True)

    @property
    def total_charge(self):
        return 0

    # ((APC + all other charges) - discount)+VAT

    def get_total_charge_ammount(self, currency, pre_payment_deal_instance):
        total_amount = 0
        if currency == 'usd':
            if self.pub_fee_usd:
                total_amount += self.pub_fee_usd
            if self.submission_fee_usd:
                total_amount += self.submission_fee_usd
            if self.page_charge_usd:
                total_amount += self.page_charge_usd
            if self.other_charge_usd:
                total_amount += self.other_charge_usd
            if self.colour_charge_usd:
                total_amount += self.colour_charge_usd

        if currency == 'gbp':
            if self.pub_fee_gbp:
                total_amount += self.pub_fee_gbp
            if self.submission_fee_gbp:
                total_amount += self.submission_fee_gbp
            if self.page_charge_gbp:
                total_amount += self.page_charge_gbp
            if self.other_charge_gbp:
                total_amount += self.other_charge_gbp
            if self.colour_charge_gbp:
                total_amount += self.colour_charge_gbp

        if currency == 'eur':
            if self.pub_fee_eur:
                total_amount += self.pub_fee_eur
            if self.submission_fee_eur:
                total_amount += self.submission_fee_eur
            if self.page_charge_eur:
                total_amount += self.page_charge_eur
            if self.other_charge_eur:
                total_amount += self.other_charge_eur
            if self.colour_charge_eur:
                total_amount += self.colour_charge_eur

        discount = total_amount * ((pre_payment_deal_instance.discount) / 100)
        total_amount -= discount

        vat = total_amount * ((pre_payment_deal_instance.vat_percentage) / 100)
        total_amount += vat

        return total_amount

    def is_approved(self):
        return self.status == ApprovalStatusEnum.APPROVED.value

    def is_rejected(self):
        return self.status == ApprovalStatusEnum.REJECTED.value

    class Meta:
        abstract = True
