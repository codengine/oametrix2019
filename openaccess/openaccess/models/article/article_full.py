from datetime import timedelta
from core.api.actions.view_action import ViewAction
from core.constants.approval_status import ApprovalStatusEnum
from engine.library.clock import Clock
from openaccess.models.article.articlefull_abstract import ArticleFullAbstract
from openaccess.models.article.author import ArticleAuthor
from core.models.organisation.organisation import Organisation
from openaccess.models.configuration.publisher_correction_request_config import PublisherCorrectionRequestConfig
from openaccess.models.contract.author_apc_fund_request import AuthorAPCFundRequest
from openaccess.models.contract.correction_request import CorrectionRequest
from openaccess.models.publication.publication import Publication
from openaccess.models.policy.licence import Licence
from rest_framework import serializers
from django.db.models.query_utils import Q


class ArticleFull(ArticleFullAbstract):

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(ArticleFull, cls).get_serializer()

        class ArticleFullSerializer(BaseSerializer):
            author = ArticleAuthor.get_serializer()(required=False)
            co_authors = ArticleAuthor.get_serializer()(many=True, required=False)
            publisher = Organisation.get_serializer()(required=False)
            publication = Publication.get_serializer()(required=False)
            funder = Organisation.get_serializer()(required=False)
            licence = Licence.get_serializer()(required=False)

            content_type_name = serializers.SerializerMethodField()

            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'title', 'doi', 'article_id', 'content_type', 'content_type_name', 'publisher', 'publication',
                    'author', 'grant_number',
                    'co_authors', 'funder', 'funder_name',
                    'submission_date', 'acceptance_date', 'pub_date', 'vol_number', 'issue_number', 'page_no_from',
                    'page_no_to', 'licence', 'impact_factor', 'hindex', 'indexed', 'sherpa_romeo_info', 'pub_fee_gbp',
                    'pub_fee_eur', 'pub_fee_usd', 'submission_fee_gbp', 'submission_fee_eur', 'submission_fee_usd',
                    'colour_charge_gbp', 'colour_charge_eur', 'colour_charge_usd', 'page_charge_gbp', 'page_charge_eur',
                    'page_charge_usd', 'other_charge_gbp', 'other_charge_eur', 'other_charge_usd', 'is_active')
                read_only_fields = ('id',)

            def get_content_type_name(self, obj):
                return obj.content_type.name

        return ArticleFullSerializer

    @classmethod
    def data_filters(cls, user, *args, **kwargs):

        if user.is_publisher_user():
            org_ids = [user.organisation.pk]
            child_orgs = Organisation.objects.all().all_active().filter(parent_id=user.organisation.pk).values_list(
                'pk',
                flat=True)
            org_ids += child_orgs
            filters = [
                Q(**{'publisher_id__in': org_ids})
            ]

        elif user.role.name == 'User':
            filters = [
                Q(**{'author__email__iexact': user.email}) | Q(**{'co_authors__email__iexact': user.email})
            ]

        else:
            filters = [
                Q(**{'author__affiliation__iexact': user.organisation.name})
            ]
        return filters

    @classmethod
    def remove_duplicates(cls, queryset, *args, **kwargs):
        return queryset.distinct()

    @classmethod
    def filter_queryset(cls, request, queryset, search_request_params, *args, **kwargs):
        if request.user.organisation:
            institution_id = None
            for search_request_param in search_request_params:
                keys = search_request_param.keys()
                for key in keys:
                    if key == 'institution_id':
                        institution_id = search_request_param['institution_id']

            if institution_id is not None:
                org = Organisation.objects.filter(id=institution_id).first()
                if org is None:
                    return queryset.none()

                if request.user.is_publisher_user():
                    # shei shokol article jader publisher ami nije and oametrix shate er author er match ase
                    queryset = queryset.filter(publisher_id=request.user.organisation.pk).filter(
                        author__affiliation__iexact=org.name)
                else:
                    queryset = queryset.filter(publisher_id=org.id)

        return queryset

    @classmethod
    def has_custom_object_actions(cls):
        return True

    def show_edit_action(self):
        return False

    def show_delete_action(self):
        return False

    def generate_custom_actions(self, request):

        user = request.user

        custom_actions = []

        if self.status == ApprovalStatusEnum.PENDING.value:

            if user.is_hub_superadmin():
                custom_actions += [
                    self.__class__.action_template(action_label="Approve",
                                         instance_action=ViewAction.APPROVE.value,
                                         request_method="PUT")
                ]
                custom_actions += [
                    self.__class__.action_template(action_label="Reject",
                                         instance_action=ViewAction.REJECT.value,
                                         request_method="PUT")
                ]
                apc_fund_request_label = AuthorAPCFundRequest.apc_request_button_label(self, request.user)
                custom_actions += [
                    self.__class__.action_template(action_label=apc_fund_request_label,
                                         instance_action=ViewAction.NO_ACTION.value,
                                         request_method="GET")
                ]
            else:
                if user.is_publisher_user():
                    custom_actions += [
                        self.__class__.action_template(action_label="Pending",
                                             instance_action=ViewAction.NO_ACTION.value,
                                             request_method="GET")
                    ]
                elif user.is_author_user():
                    apc_fund_request_label = AuthorAPCFundRequest.apc_request_button_label(self, request.user)
                    custom_actions += [
                        self.__class__.action_template(action_label=apc_fund_request_label,
                                             instance_action=ViewAction.AUTHOR_APC_FUND_REQUEST.value,
                                             request_method="PUT")
                    ]
                else:
                    custom_actions += [
                        self.__class__.action_template(action_label="Approve",
                                             instance_action=ViewAction.APPROVE.value,
                                             request_method="PUT")
                    ]
                    custom_actions += [
                        self.__class__.action_template(action_label="Reject",
                                             instance_action=ViewAction.REJECT.value,
                                             request_method="PUT")
                    ]
        elif self.is_approved():
            allowed_correction_days = PublisherCorrectionRequestConfig.get_allowed_correction_days(self.publisher)
            pending_correction_request_exists = CorrectionRequest.pending_correction_request_exists(publisher_instance=self.publisher,
                                                                                                    inistitution_instance=user.organisation,
                                                                                                    article_instance=self)
            now_dt = Clock.utc_now().date()
            allowed_date = self.approved_date + timedelta(days=allowed_correction_days)

            if user.is_hub_superadmin():
                custom_actions += [
                    self.__class__.action_template(action_label="Approved",
                                         instance_action=ViewAction.NO_ACTION.value,
                                         request_method="GET")
                ]
                if now_dt <= allowed_date:
                    if pending_correction_request_exists:
                        custom_actions += [
                            self.__class__.action_template(action_label="Correction Requested",
                                                 instance_action=ViewAction.CORRECTION_REQUESTED.value,
                                                 request_method="PUT")
                        ]
                    else:
                        custom_actions += [
                            self.__class__.action_template(action_label="Correction Request",
                                                 instance_action=ViewAction.CORRECTION_REQUEST.value,
                                                 request_method="PUT")
                        ]
            else:
                if user.is_publisher_user():
                    self.__class__.action_template(action_label="Approved",
                                         instance_action=ViewAction.NO_ACTION.value,
                                         request_method="GET")
                elif user.is_author_user():
                    self.__class__.action_template(action_label="Approved",
                                         instance_action=ViewAction.NO_ACTION.value,
                                         request_method="GET")
                else:
                    custom_actions += [
                        self.__class__.action_template(action_label="Approved",
                                             instance_action=ViewAction.NO_ACTION.value,
                                             request_method="GET")
                    ]
                    if now_dt <= allowed_date:
                        if pending_correction_request_exists:
                            custom_actions += [
                                self.__class__.action_template(action_label="Correction Requested",
                                                     instance_action=ViewAction.CORRECTION_REQUESTED.value,
                                                     request_method="PUT")
                            ]
                        else:
                            custom_actions += [
                                self.__class__.action_template(action_label="Correction Request",
                                                     instance_action=ViewAction.CORRECTION_REQUEST.value,
                                                     request_method="PUT")
                            ]

        elif self.is_rejected():
            self.action_template(action_label="Rejected",
                                 instance_action=ViewAction.NO_ACTION.value,
                                 request_method="GET")

        return custom_actions

