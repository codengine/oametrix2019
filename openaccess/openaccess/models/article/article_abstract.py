from django.db import models
from core.models.base_entity import BaseEntity


class ArticleAbstract(BaseEntity):
    title = models.CharField(max_length=2000)
    doi = models.URLField(max_length=2000, null=True, blank=True)
    article_id = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        abstract = True

