from openaccess.models.article.articlefull_abstract import ArticleFullAbstract
from django.db import models
from openaccess.models.article.articlefull_abstract import ArticleFullAbstract
from openaccess.models.article.author import ArticleAuthor
from core.models.organisation.organisation import Organisation
from openaccess.models.publication.publication import Publication
from openaccess.models.policy.licence import Licence
from rest_framework import serializers


class ArticleManual(ArticleFullAbstract):
    publisher_name = models.CharField(max_length=1000, null=True, blank=True)
    journal_name = models.CharField(max_length=200, null=True, blank=True)
    journal_acronym = models.CharField(max_length=100, null=True, blank=True)
    status = models.CharField(max_length=20, null=True, blank=True)
    pissn = models.CharField(max_length=100, null=True, blank=True)
    eissn = models.CharField(max_length=100, null=True, blank=True)
    fund_acknowledgement = models.TextField(null=True, blank=True)
    sub_system_acronym = models.CharField(max_length=100, null=True, blank=True)
    grant_number = models.CharField(max_length=100, null=True, blank=True)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(ArticleManual, cls).get_serializer()

        class ArticleManualSerializer(BaseSerializer):
            author = ArticleAuthor.get_serializer()(required=False)
            co_authors = ArticleAuthor.get_serializer()(many=True, required=False)
            publisher = Organisation.get_serializer()(required=False)
            publication = Publication.get_serializer()(required=False)
            funder = Organisation.get_serializer()(required=False)
            licence = Licence.get_serializer()(required=False)
            content_type_name = serializers.SerializerMethodField()

            is_pissn_match = serializers.SerializerMethodField()
            is_eissn_match = serializers.SerializerMethodField()
            is_publisher_name_match = serializers.SerializerMethodField()
            is_funder_name_match = serializers.SerializerMethodField()
            is_author_affiliation_match = serializers.SerializerMethodField()

            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'title', 'doi', 'article_id', 'content_type', 'content_type_name', 'publisher', 'publication',
                    'author',
                    'co_authors', 'funder',
                    'submission_date', 'acceptance_date', 'pub_date', 'vol_number', 'issue_number', 'page_no_from',
                    'page_no_to', 'licence', 'impact_factor', 'hindex', 'indexed', 'sherpa_romeo_info', 'pub_fee_gbp',
                    'pub_fee_eur', 'pub_fee_usd', 'submission_fee_gbp', 'submission_fee_eur', 'submission_fee_usd',
                    'colour_charge_gbp', 'colour_charge_eur', 'colour_charge_usd', 'page_charge_gbp', 'page_charge_eur',
                    'page_charge_usd', 'other_charge_gbp', 'other_charge_eur', 'other_charge_usd',
                    'publisher_name', 'journal_name', 'journal_acronym', 'funder_name', 'status', 'pissn', 'eissn',
                    'fund_acknowledgement', 'sub_system_acronym', 'grant_number', 'is_pissn_match', 'is_eissn_match',
                    'is_publisher_name_match', 'is_funder_name_match', 'is_author_affiliation_match', 'is_active'
                )
                read_only_fields = ('id',)

            def get_is_pissn_match(self, obj):
                all_publications = Publication.objects.all()
                for single_publication in all_publications:
                    if obj.pissn == single_publication.pissn:
                        return True
                return False

            def get_is_eissn_match(self, obj):
                all_publications = Publication.objects.all()
                for single_publication in all_publications:
                    if obj.eissn == single_publication.eissn:
                        return True
                return False

            def get_is_publisher_name_match(self, obj):
                all_org_names = set(Organisation.objects.values_list('name', flat=True))
                for name in all_org_names:
                    if obj.publisher_name and name:
                        if obj.publisher_name.lower() == name.lower():
                            return True
                return False

            def get_is_funder_name_match(self, obj):
                all_org_names = set(Organisation.objects.values_list('name', flat=True))
                for name in all_org_names:
                    if obj.funder_name and name:
                        if obj.funder_name.lower() == name.lower():
                            return True
                return False

            def get_is_author_affiliation_match(self, obj):
                if obj.author is None:
                    return False

                if obj.author.affiliation is None:
                    return False

                all_org_names = set(Organisation.objects.values_list('name', flat=True))
                for org_name in all_org_names:
                    if obj.author.affiliation.lower() == org_name.lower():
                        return True
                return False

            def get_content_type_name(self, obj):
                return obj.content_type.name

        return ArticleManualSerializer
