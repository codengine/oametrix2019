from django.db import models, transaction
from core.constants.approval_status import ApprovalStatusEnum
from core.models.base_entity import BaseEntity
from openaccess.models.article.article_full import ArticleFull


class ArticleReject(BaseEntity):
    article = models.ForeignKey(ArticleFull, on_delete=models.DO_NOTHING)
    note = models.TextField(null=True, blank=True)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(ArticleReject, cls).get_serializer()

        class ArticleRejectSerializer(BaseSerializer):
            def create(self, validated_data):
                with transaction.atomic():
                    instance = super(ArticleRejectSerializer, self).create(validated_data=validated_data)
                    instance.article.status = ApprovalStatusEnum.REJECTED.value
                    instance.article.save()
                    return instance

            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'article', 'note', 'is_active')
                read_only_fields = ('id',)

        return ArticleRejectSerializer

