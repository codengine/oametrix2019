from django.db import models
from core.models.base_entity import BaseEntity
from core.models.organisation.organisation import Organisation
from openaccess.models.publication.journal_type import JournalType
from django.db.models.query_utils import Q


class Journal(BaseEntity):
    name = models.CharField(max_length=2000)
    publisher = models.ForeignKey(Organisation, null=True, blank=True, on_delete=models.DO_NOTHING)
    acronym = models.CharField(max_length=20, null=True, blank=True)
    pissn = models.CharField(max_length=20, null=True, blank=True)
    eissn = models.CharField(max_length=20, null=True, blank=True)
    journal_type = models.ForeignKey(JournalType, on_delete=models.DO_NOTHING)
    apc_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    apc_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    apc_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    submission_fee_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    submission_fee_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    submission_fee_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    colour_charge_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    colour_charge_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    colour_charge_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    page_other_charge_gbp = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    page_other_charge_eur = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    page_other_charge_usd = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    impact_factor = models.CharField(max_length=20, null=True, blank=True)
    hindex = models.CharField(max_length=20, null=True, blank=True)
    indexed = models.CharField(max_length=20, null=True, blank=True)
    sherpa_romeo_info = models.CharField(max_length=300, null=True, blank=True)
    note = models.CharField(max_length=300, null=True, blank=True)
    status = models.CharField(max_length=100, null=True, blank=True)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(Journal, cls).get_serializer()

        class JournalSerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                publisher = Organisation.get_serializer()(required=False)
                fields = (
                    'id', 'name', 'publisher', 'acronym', 'pissn', 'eissn', 'journal_type',
                    'apc_gbp', 'apc_eur', 'apc_usd', 'submission_fee_gbp', 'submission_fee_eur',
                    'submission_fee_usd', 'colour_charge_gbp', 'colour_charge_eur', 'colour_charge_usd',
                    'page_other_charge_gbp', 'page_other_charge_eur', 'page_other_charge_usd',
                    'impact_factor', 'hindex', 'indexed', 'sherpa_romeo_info', 'note', 'status', 'is_active')
                read_only_fields = ('id',)

        return JournalSerializer

    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        org_ids = [user.organisation.pk]
        child_orgs = Organisation.objects.all().all_active().filter(parent_id=user.organisation.pk).values_list('pk',
                                                                                                                flat=True)
        org_ids += child_orgs
        filters = [
            Q(**{'publisher_id__in': org_ids})
        ]
        return filters
