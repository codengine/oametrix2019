from django.db import models
from core.models.base_entity import BaseEntity


# [Book or Journal]
class PublicationType(BaseEntity):
    name = models.CharField(max_length=500)
    parent = models.ForeignKey('self', null=True, blank=True, on_delete=models.DO_NOTHING)

    @classmethod
    def create_of_update(cls, name, parent=None, is_active=True):
        instances = cls.objects.filter(name=name)
        if instances.exists():
            instance = instances.first()
        else:
            instance = cls()

        instance.name = name
        instance.parent = parent
        instance.is_active = is_active
        instance.save()
        return instance

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(PublicationType, cls).get_serializer()

        class PublicationTypeSerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'name', 'parent', 'is_active')
                read_only_fields = ('id',)

        return PublicationTypeSerializer
