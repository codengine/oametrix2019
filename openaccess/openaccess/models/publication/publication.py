from django.db import models
from core.models.base_entity import BaseEntity
from core.models.organisation.organisation import Organisation
from openaccess.models.price.publication_price import PublicationPrice
from openaccess.models.publication.journal_type import JournalType
from openaccess.models.publication.publication_type import PublicationType
from django.db.models.query_utils import Q
from django.db import transaction
from rest_framework import serializers


# [Journal and Publication are same]
class Publication(BaseEntity):
    name = models.CharField(max_length=2000)
    publication_type = models.ForeignKey(PublicationType, on_delete=models.DO_NOTHING)
    publisher = models.ForeignKey(Organisation, null=True, blank=True, on_delete=models.DO_NOTHING)
    journal_type = models.ForeignKey(JournalType, null=True, blank=True, on_delete=models.DO_NOTHING)
    pissn = models.CharField(max_length=20, null=True, blank=True)
    eissn = models.CharField(max_length=20, null=True, blank=True)

    impact_factor = models.CharField(max_length=20, null=True, blank=True)
    hindex = models.CharField(max_length=20, null=True, blank=True)
    indexed = models.CharField(max_length=20, null=True, blank=True)
    sherpa_romeo_info = models.CharField(max_length=300, null=True, blank=True)
    note = models.CharField(max_length=300, null=True, blank=True)
    status = models.CharField(max_length=100, null=True, blank=True)

    pub_acronym = models.CharField(max_length=20, null=True, blank=True)
    sub_sys_acronym = models.CharField(max_length=20, null=True, blank=True)
    owner = models.ForeignKey(Organisation, related_name='+', null=True, blank=True, on_delete=models.DO_NOTHING)

    prices = models.ManyToManyField(PublicationPrice)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(Publication, cls).get_serializer()

        class PublicationSerializer(BaseSerializer):
            prices = PublicationPrice.get_serializer()(many=True, required=False)
            publication_type_name = serializers.SerializerMethodField()
            publisher_name = serializers.SerializerMethodField()
            journal_type_name = serializers.SerializerMethodField()
            owner_name = serializers.SerializerMethodField()

            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'name', 'publication_type','publication_type_name', 'publisher', 'publisher_name', 'journal_type','journal_type_name', 'pissn', 'eissn',
                    'impact_factor', 'hindex', 'indexed', 'sherpa_romeo_info', 'note', 'status', 'pub_acronym',
                    'sub_sys_acronym', 'owner', 'owner_name', 'prices','is_active')
                read_only_fields = ('id',)

            def get_publication_type_name(self, obj):
                return obj.publication_type.name

            def get_publisher_name(self, obj):
                if obj.publisher:
                    return obj.publisher.name
                else:
                    return None

            def get_journal_type_name(self, obj):
                if obj.journal_type:
                    return obj.journal_type.name
                else:
                    return None

            def get_owner_name(self, obj):
                if obj.owner:
                    return obj.owner.name
                else:
                    return None

        return PublicationSerializer

    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        org_ids = [user.organisation.pk]
        child_orgs = Organisation.objects.all().all_active().filter(parent_id=user.organisation.pk).values_list('pk',
                                                                                                                flat=True)
        org_ids += child_orgs
        filters = [
            Q(**{'publisher_id__in': org_ids})
        ]
        return filters
