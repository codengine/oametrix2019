from django.db import models, transaction
from rest_framework import status
from django.db.models.query_utils import Q
from rest_framework import serializers
from core.constants.approval_status import ApprovalStatusEnum
from core.models.base_entity import BaseEntity
# from openaccess.models.article.article_approve import ArticleApprove
from openaccess.models.finance.organisation_wallet import OrganisationWallet
from openaccess.models.transactions.deposit_credit_transaction import DepositCreditTransaction
from openaccess.models.transactions.offset_fund_transaction import OffsetFundTransaction
from openaccess.models.transactions.oa_token_transaction import OATokenTransaction
from core.models.contracts.approvable_abstract_model import ApprovableAbstractModel
from core.models.organisation.organisation import Organisation
from django.apps import apps


class CorrectionRequest(BaseEntity, ApprovableAbstractModel):
    article = models.ForeignKey('ArticleFull', on_delete=models.DO_NOTHING)
    note = models.TextField()

    @classmethod
    def pending_correction_request_exists(cls, publisher_instance, inistitution_instance, article_instance):
        return CorrectionRequest.objects.filter(organisation_id=inistitution_instance.pk,
                                                counter_organisation_id=publisher_instance.pk,
                                                article_id=article_instance.pk,
                                                approval_status=ApprovalStatusEnum.PENDING.value).exists()

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(CorrectionRequest, cls).get_serializer()

        class CorrectionRequestSerializer(BaseSerializer):
            organisation_name = serializers.SerializerMethodField()
            counter_organisation_name = serializers.SerializerMethodField()
            created_by_name = serializers.SerializerMethodField()

            def get_oa_deal_type(self, instance):
                return instance.oa_deal.deal_type.name

            def get_created_by_name(self, instance):
                return instance.created_by.get_full_name()

            def get_organisation_name(self, obj):
                return obj.organisation.name

            def get_counter_organisation_name(self, obj):
                return obj.counter_organisation.name

            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'article', 'organisation', 'organisation_name', 'counter_organisation',
                    'counter_organisation_name', 'approval_status',
                    'note', 'created_by', 'created_by_name')
                read_only_fields = (
                    'id', 'organisation_name', 'counter_organisation_name', 'created_by', 'created_by_name')

            def create(self, validated_data):
                instance = super(CorrectionRequestSerializer, self).create(validated_data)
                instance.approval_status = ApprovalStatusEnum.PENDING.value
                instance.is_active = False
                instance.save()
                return instance

            def to_internal_value(self, data):
                ArticleFull = apps.get_model('openaccess', 'ArticleFull')
                user = self.context.get('request').user
                data['organisation'] = user.organisation.id

                article_id = self.initial_data.get('article')

                article = ArticleFull.objects.filter(id=article_id).first()
                if article is None:
                    raise serializers.ValidationError({"error": "No article found"},
                                                      status.HTTP_400_BAD_REQUEST)
                data['counter_organisation'] = article.publisher.id
                return super(CorrectionRequestSerializer, self).to_internal_value(data)

        return CorrectionRequestSerializer

    def approve(self, request, **kwargs):
        ArticleApprove = apps.get_model('openaccess', 'ArticleApprove')

        with transaction.atomic():
            if not self.article.is_approved():
                raise serializers.ValidationError({"error": "Article is not yet approved"},
                                                 status.HTTP_400_BAD_REQUEST)

            instance = super(CorrectionRequest, self).approve(request, **kwargs)
            publisher_organisation = request.user.organisation
            org_wallet_instances = OrganisationWallet.objects.filter(organisation_id=self.counter_organisation.pk)
            if not org_wallet_instances.exists():
                raise serializers.ValidationError({"error": "No wallet found for publisher"},
                                                  status.HTTP_400_BAD_REQUEST)
            institution_wallet_instances = OrganisationWallet.objects.filter(organisation_id=self.organisation.pk)
            if not institution_wallet_instances.exists():
                raise serializers.ValidationError({"error": "No wallet found for Institution"},
                                                  status.HTTP_400_BAD_REQUEST)

            org_wallet_instance = org_wallet_instances.first().wallet

            # Get the last ArticleApprove entry for this article
            article_approve_instances = ArticleApprove.objects.filter(article_id=instance.article.pk,
                                                                      article__status=ApprovalStatusEnum.APPROVED.value).order_by(
                '-id')
            article_approve_instance = article_approve_instances.first()
            if not article_approve_instance:
                raise serializers.ValidationError({"error": "No approval entry found for this article"},
                                                  status.HTTP_400_BAD_REQUEST)

            self.article.status = ApprovalStatusEnum.PENDING.value
            self.article.approved_date = None
            self.article.save()

            apc_option_instance = article_approve_instance.apc_option
            if apc_option_instance.is_deposit_fund():
                latest_debit_transaction = DepositCreditTransaction.find_latest_debit_transaction(
                    article_instance=self.article, institution_instance=self.article.author.get_organisation(),
                    publisher_instance=self.article.publisher)
                if not latest_debit_transaction:
                    raise serializers.ValidationError({"error": "No previous transaction found to reverse"},
                                                      status.HTTP_400_BAD_REQUEST)
                org_wallet_instance.add_deposit_credit(organisation_instance=self.organisation,
                                                       currency_instance=latest_debit_transaction.currency,
                                                       balance=latest_debit_transaction.amount)
                dc_transaction = DepositCreditTransaction()
                dc_transaction.add_reverse_debit_transaction(article_instance=self.article,
                                                             currency_instance=latest_debit_transaction.currency,
                                                             institution_instance=self.article.author.get_organisation(),
                                                             publisher_instance=self.article.publisher,
                                                             amount=latest_debit_transaction.amount,
                                                             reference_model='openaccess.CorrectionRequest',
                                                             reference_id=self.pk,
                                                             note='Reverse Transaction ref Correction request Approved # %s' % self.pk)


            elif apc_option_instance.is_offset_fund():
                latest_debit_transaction = OffsetFundTransaction.find_latest_debit_transaction(
                    article_instance=self.article, institution_instance=self.article.author.get_organisation(),
                    publisher_instance=self.article.publisher)
                if not latest_debit_transaction:
                    raise serializers.ValidationError({"error": "No previous transaction found to reverse"},
                                                      status.HTTP_400_BAD_REQUEST)
                institution_wallet_instance = institution_wallet_instances.first()
                institution_wallet_instance.wallet.add_offset_fund_credit(organisation_instance=self.counter_organisation,
                                                                   currency_instance=latest_debit_transaction.currency,
                                                                   balance=latest_debit_transaction.amount)
                of_transaction = OffsetFundTransaction()
                of_transaction.add_reverse_debit_transaction(article_instance=self.article,
                                                             currency_instance=latest_debit_transaction.currency,
                                                             institution_instance=self.article.author.get_organisation(),
                                                             publisher_instance=self.article.publisher,
                                                             amount=latest_debit_transaction.amount,
                                                             reference_model='openaccess.CorrectionRequest',
                                                             reference_id=self.pk,
                                                             note='Reverse Transaction ref Correction request Approved # %s' % self.pk)

            elif apc_option_instance.is_oa_token():
                latest_debit_transaction = OATokenTransaction.find_latest_debit_transaction(
                    article_instance=self.article, institution_instance=self.article.author.get_organisation(),
                    publisher_instance=self.article.publisher)
                if not latest_debit_transaction:
                    raise serializers.ValidationError({"error": "No previous transaction found to reverse"},
                                                      status.HTTP_400_BAD_REQUEST)
                institution_wallet_instance = institution_wallet_instances.first()
                latest_debit_transaction.token.is_used = False
                latest_debit_transaction.token.save()
                institution_wallet_instance.wallet.add_token(latest_debit_transaction.token)
                oa_token_transaction = OATokenTransaction()
                oa_token_transaction.add_reverse_debit_transaction(article_instance=self.article,
                                                                   token_instance=latest_debit_transaction.token,
                                                                   institution_instance=self.article.author.get_organisation(),
                                                                   publisher_instance=self.article.publisher,
                                                                   reference_model='openaccess.CorrectionRequest',
                                                                   reference_id=self.pk,
                                                                   note='Reverse Transaction ref Correction request Approved # %s' % self.pk)

            return instance

    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        org_ids = [user.organisation.pk]
        child_orgs = Organisation.objects.all().all_active().filter(parent_id=user.organisation.pk).values_list('pk',
                                                                                                                flat=True)
        org_ids += child_orgs
        filters = [
            Q(**{'organisation_id__in': org_ids}) | Q(**{'counter_organisation_id__in': org_ids})
        ]
        return filters
