from django.db import models
from core.constants.approval_status import ApprovalStatusEnum
from core.constants.deal_type_enum import DealTypeEnum
from core.models.base_entity import BaseEntity
from core.models.contacts.country import Country
from core.models.contracts.approvable_abstract_model import ApprovableAbstractModel
from core.models.finance.currency import Currency
from core.models.organisation.organisation import Organisation
from django.db.models.query_utils import Q
from rest_framework import serializers
from django.db import transaction
from rest_framework import status
from openaccess.models.contract.deal_type import DealType


class OADeal(BaseEntity, ApprovableAbstractModel):
    deal_type = models.ForeignKey(DealType, on_delete=models.DO_NOTHING)
    currency = models.ForeignKey(Currency, on_delete=models.DO_NOTHING)
    discount = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    vat_country = models.ForeignKey(Country, null=True, blank=True, on_delete=models.DO_NOTHING)
    vat_percentage = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    vat_number = models.CharField(max_length=200, null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    expire_date = models.DateField(null=True, blank=True)

    @classmethod
    def get_prepayment_deal(cls, publisher_instance, institution_instance, **kwargs):
        oa_deal_instances = OADeal.objects.filter(organisation_id=publisher_instance.pk,
                                                  counter_organisation_id=institution_instance.pk,
                                                  deal_type__name=DealTypeEnum.PRE_PAYMENT.value)
        if oa_deal_instances.exists():
            return oa_deal_instances.first()

    def has_expiry_date(self):
        return self.expire_date is not None

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(OADeal, cls).get_serializer()

        class OADealSerializer(BaseSerializer):
            deal_name = serializers.SerializerMethodField(required=False)
            created_by_name = serializers.SerializerMethodField(required=False)
            organisation_name = serializers.SerializerMethodField()
            counter_organisation_name = serializers.SerializerMethodField()
            currency_name = serializers.SerializerMethodField(required=False)
            vat_country_name = serializers.SerializerMethodField(required=False)

            def get_currency_name(self, instance):
                return instance.currency.short_name

            def get_vat_country_name(self, instance):
                return instance.vat_country.name

            def get_organisation_name(self, obj):
                return obj.organisation.name

            def get_counter_organisation_name(self, obj):
                return obj.counter_organisation.name

            def get_deal_name(self, instance):
                return instance.deal_type.name

            def get_created_by_name(self, instance):
                return instance.created_by.get_full_name()

            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'organisation', 'counter_organisation', 'organisation_name', 'deal_type', 'deal_name', 'counter_organisation_name',
                    'currency', 'discount', 'vat_country', 'approval_status', 'vat_percentage', 'approve_date', 'vat_number', 'currency_name',
                    'vat_country_name', 'note', 'expire_date','is_active', 'approved_by', 'approved_by_group', 'approved_by_role',
                    'date_created', 'created_by', 'created_by_name')
                read_only_fields = ('id', 'approval_status', 'approve_date', 'approved_by',
                                    'approved_by_group', 'approved_by_role', 'date_created')

            def create(self, validated_data):
                # Check if there any existing OADeal between these two
                # organisations and same type deal. Then return it simply.

                organisation = validated_data['organisation']
                counter_organisation = validated_data['counter_organisation']
                deal_type = validated_data['deal_type']
                oa_deal_instances = OADeal.objects.filter(organisation_id=organisation.pk,
                                                          counter_organisation_id=counter_organisation.pk,
                                                          deal_type_id=deal_type.pk)
                if oa_deal_instances.exists():
                    raise serializers.ValidationError({"error": "OA-Deal already exists"},
                                                status.HTTP_400_BAD_REQUEST)

                instance = super(OADealSerializer, self).create(validated_data)
                instance.approval_status = ApprovalStatusEnum.PENDING.value
                instance.is_active = False
                instance.save()
                return instance

            def to_internal_value(self, data):
                user = self.context.get('request').user
                data['organisation'] = user.organisation.id
                return super(OADealSerializer, self).to_internal_value(data)


            def validate(self, data):
                req_user = self.context.get('request').user
                if req_user.group.name != 'Publisher':
                    raise serializers.ValidationError({"error": "Only Publisher can create oa-deal"},
                                                      status.HTTP_400_BAD_REQUEST)

                data = super(OADealSerializer, self).validate(data)
                return data


        return OADealSerializer

    @classmethod
    def map_view_action_to_label(cls):
        return {
            "create": "New",
            "update": "Edit"
        }

    @classmethod
    def on_post_create(cls, request, user, model, view_ref, **kwargs):
        print("Inside the on post create with instance ref")
        print(view_ref.instance)

    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        org_ids = [user.organisation.pk]
        child_orgs = Organisation.objects.all().all_active().filter(parent_id=user.organisation.pk).values_list('pk',
                                                                                                                flat=True)
        org_ids += child_orgs
        filters = [
            Q(**{'organisation_id__in': org_ids}) | Q(**{'counter_organisation_id__in': org_ids})
        ]
        return filters

