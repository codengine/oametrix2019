from django.db import models, transaction
from engine.library.clock import Clock
from rest_framework import status
from core.models.base_entity import BaseEntity
from core.models.contracts.approvable_abstract_model import ApprovableAbstractModel
from core.models.finance.currency import Currency
from openaccess.models.contract.oa_deal import OADeal
from core.models.organisation.organisation import Organisation
from django.db.models.query_utils import Q
from rest_framework import serializers
from core.constants.approval_status import ApprovalStatusEnum
from openaccess.models.finance.organisation_wallet import OrganisationWallet
from openaccess.models.transactions.deposit_credit_transaction import DepositCreditTransaction


class DepositRequest(BaseEntity, ApprovableAbstractModel):
    oa_deal = models.ForeignKey(OADeal, on_delete=models.CASCADE)
    currency = models.ForeignKey(Currency, on_delete=models.DO_NOTHING)
    amount = models.DecimalField(decimal_places=3, max_digits=20)
    publish_fee = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    read_fee = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(DepositRequest, cls).get_serializer()

        class DepositRequestSerializer(BaseSerializer):
            oa_deal_type = serializers.SerializerMethodField(required=False)
            organisation_name = serializers.SerializerMethodField()
            counter_organisation_name = serializers.SerializerMethodField()
            created_by_name = serializers.SerializerMethodField(required=False)
            currency_name = serializers.SerializerMethodField(required=False)

            def get_oa_deal_type(self, instance):
                return instance.oa_deal.deal_type.name

            def get_created_by_name(self, instance):
                return instance.created_by.get_full_name()

            def get_currency_name(self, instance):
                return instance.currency.name

            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'organisation', 'organisation_name', 'counter_organisation', 'counter_organisation_name',
                    'oa_deal', 'oa_deal_type', 'currency', 'currency_name', 'amount', 'publish_fee', 'read_fee',
                    'created_by', 'created_by_name')
                read_only_fields = ('id', 'organisation_name', 'counter_organisation_name')

            def create(self, validated_data):
                instance = super(DepositRequestSerializer, self).create(validated_data)
                instance.approval_status = ApprovalStatusEnum.PENDING.value
                instance.is_active = False
                instance.save()
                return instance

            def to_internal_value(self, data):
                user = self.context.get('request').user
                data['organisation'] = user.organisation.id
                return super(DepositRequestSerializer, self).to_internal_value(data)

            def is_valid(self, raise_exception=False):
                data = self.initial_data
                oa_deal = data.get('oa_deal')
                try:
                    oa_deal = int(oa_deal)
                except Exception as exp:
                    raise serializers.ValidationError({"error": "OADeal ID is not correct"},
                                                      status.HTTP_400_BAD_REQUEST)
                oa_deal_instances = OADeal.objects.filter(pk=oa_deal)
                if oa_deal_instances.exists():
                    oa_deal_instance = oa_deal_instances.first()
                else:
                    raise serializers.ValidationError({"error": "OADeal does not exist"},
                                                      status.HTTP_400_BAD_REQUEST)

                currency = data.get('currency')

                try:
                    currency = int(currency)
                except Exception as exp:
                    raise serializers.ValidationError({"error": "Currency ID is not correct"},
                                                      status.HTTP_400_BAD_REQUEST)

                currency_instances = Currency.objects.filter(pk=currency)
                if currency_instances.exists():
                    currency_instance = currency_instances.first()
                else:
                    raise serializers.ValidationError({"error": "Currency does not exist"},
                                                      status.HTTP_400_BAD_REQUEST)

                if oa_deal_instance.currency != currency_instance:
                    raise serializers.ValidationError({"error": "Currency does not match with oa-deal"},
                                                      status.HTTP_400_BAD_REQUEST)

                if oa_deal_instance.has_expiry_date() and oa_deal_instance.expire_date < Clock.utc_now().date():
                    raise serializers.ValidationError({"error": "OA deal has expired."},
                                                      status.HTTP_400_BAD_REQUEST)

                if oa_deal_instance.approval_status != ApprovalStatusEnum.APPROVED.value:
                    raise serializers.ValidationError({"error": "OA deal is not approved yet."},
                                                      status.HTTP_400_BAD_REQUEST)
                user = self.context.get('request').user
                if not user.organisation:
                    raise serializers.ValidationError({"error": "User organization not found."},
                                                      status.HTTP_400_BAD_REQUEST)
                valid = super(DepositRequestSerializer, self).is_valid(raise_exception=raise_exception)
                return valid

            def get_organisation_name(self, obj):
                return obj.organisation.name

            def get_counter_organisation_name(self, obj):
                return obj.counter_organisation.name

        return DepositRequestSerializer

    def approve(self, request, **kwargs):
        with transaction.atomic():
            if not request.user.is_hub_superadmin() and not request.user.is_publisher_user():
                pass
            instance = super(DepositRequest, self).approve(request, **kwargs)
            publisher_organisation = request.user.organisation
            org_wallet_instances = OrganisationWallet.objects.filter(organisation_id=publisher_organisation.pk)
            if not org_wallet_instances.exists():
                raise serializers.ValidationError({"error": "No wallet found for publisher"},
                                                  status.HTTP_400_BAD_REQUEST)
            org_wallet_instance = org_wallet_instances.first().wallet
            # Check if wallet has deposit credit for this institution and currency
            org_wallet_instance.add_deposit_credit(organisation_instance=instance.organisation,
                                                   currency_instance=instance.currency,
                                                   balance=instance.amount)
            dc_transaction_instance = DepositCreditTransaction()
            dc_transaction_instance.add_credit_transaction(deposit_request_instance=instance,
                                                           note='Deposit Request Approved. '
                                                                'Publisher wallet has updated for institution '
                                                                'balance and currency')
            return instance

    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        org_ids = [user.organisation.pk]
        child_orgs = Organisation.objects.all().all_active().filter(parent_id=user.organisation.pk).values_list('pk',
                                                                                                                flat=True)
        org_ids += child_orgs
        filters = [
            Q(**{'organisation_id__in': org_ids}) | Q(**{'counter_organisation_id__in': org_ids})
        ]
        return filters

