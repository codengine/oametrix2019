from django.db import models
from core.models.base_entity import BaseEntity


# [pre-payment,  read & publish]
class DealType(BaseEntity):
    name = models.CharField(max_length=500)
    description = models.TextField(null=True, blank=True)

