from django.db import models
from django.db.models.query_utils import Q
from core.loader.loader import load_model
from core.models.base_entity import BaseEntity
from core.models.contracts.approvable_abstract_model import ApprovableAbstractModel
from openaccess.models.policy.licence import Licence


class AuthorAPCFundRequest(BaseEntity, ApprovableAbstractModel):
    article = models.ForeignKey('ArticleFull', on_delete=models.DO_NOTHING)
    licence = models.ForeignKey(Licence, on_delete=models.DO_NOTHING)
    note = models.TextField(null=True, blank=True)

    @classmethod
    def apc_request_for_article(cls, article_instance, user_instance):
        apc_fund_request_instances = AuthorAPCFundRequest.objects.filter(article_id=article_instance.pk,
                                                                         created_by_id=user_instance.pk)
        if apc_fund_request_instances.exists():
            return apc_fund_request_instances.first()

    @classmethod
    def apc_fund_request_exists(cls, article_instance, user_instance):
        apc_fund_request_instances = AuthorAPCFundRequest.objects.filter(article_id=article_instance.pk,
                                                                         created_by_id=user_instance.pk)
        return apc_fund_request_instances.exists()

    @classmethod
    def apc_request_button_label(cls, article_instance, user_instance):
        apc_fund_request_instances = AuthorAPCFundRequest.objects.filter(article_id=article_instance.pk,
                                                                         created_by_id=user_instance.pk)

        if apc_fund_request_instances.exists():
            apc_fund_request_instance = apc_fund_request_instances.first()
            if apc_fund_request_instance.is_approved():
                return "APC Request Approved"
            else:
                return "APC Fund Requested"
        else:
            return "Request APC Fund"

    @classmethod
    def get_serializer(cls):
        ArticleFull = load_model('openaccess', 'ArticleFull')

        BaseSerializer = super(AuthorAPCFundRequest, cls).get_serializer()

        class AuthorAPCFundRequestSerializer(BaseSerializer):
            article = ArticleFull.get_serializer()(required=False)
            licence = Licence.get_serializer()(required=False)

            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'organisation', 'counter_organisation', 'article', 'licence', 'note','is_active')
                read_only_fields = ('id',)

            def get_content_type_name(self, obj):
                return obj.content_type.name

        return AuthorAPCFundRequestSerializer

    @classmethod
    def data_filters(cls, user, *args, **kwargs):

        if user.is_author_user():
            filters = [
                Q(**{'create_by_id': user.pk})
            ]
        else:
            filters = [
                Q(**{'id': -1})
            ]
        return filters



