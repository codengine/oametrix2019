from django.db import models

from core.constants.apc_option_enum import APCOptionEnum
from core.models.base_entity import BaseEntity


class APCOption(BaseEntity):
    name = models.CharField(max_length=1000)

    def is_deposit_fund(self):
        return self.name == APCOptionEnum.DEPOSIT_CREDIT.value

    def is_offset_fund(self):
        return self.name == APCOptionEnum.OFFSET_FUND.value

    def is_oa_token(self):
        return self.name == APCOptionEnum.OATOKEN.value

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(APCOption, cls).get_serializer()

        class APCOptionSerializer(BaseSerializer):

            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'name', 'is_active')
                read_only_fields = ('id',)

        return APCOptionSerializer

    @classmethod
    def create_of_update(cls, name, is_active=True):
        instances = cls.objects.filter(name=name)
        if instances.exists():
            instance = instances.first()
        else:
            instance = cls()

        instance.name = name
        instance.is_active = is_active
        instance.save()
        return instance

