from django.db import models
from core.models.base_entity import BaseEntity
from core.models.organisation.organisation import Organisation
from django.db.models.query_utils import Q


class OAPolicy(BaseEntity):
    publisher = models.ForeignKey(Organisation, on_delete=models.DO_NOTHING)
    hybrid = models.TextField(null=True, blank=True)
    gold_oa = models.TextField(null=True, blank=True)
    st = models.TextField(null=True, blank=True)
    hss = models.TextField(null=True, blank=True)
    med = models.TextField(null=True, blank=True)
    green_oa = models.TextField(null=True, blank=True)
    min_discount_percentage = models.DecimalField(decimal_places=2, max_digits=20, null=True, blank=True)
    min_discount_amount = models.DecimalField(decimal_places=2, max_digits=20, null=True, blank=True)
    min_deposit_percentage = models.DecimalField(decimal_places=2, max_digits=20, null=True, blank=True)
    min_deposit_amount = models.DecimalField(decimal_places=2, max_digits=20, null=True, blank=True)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(OAPolicy, cls).get_serializer()

        class OAPolicySerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'publisher', 'hybrid', 'gold_oa', 'st', 'hss', 'med', 'green_oa', 'min_discount_percentage',
                    'min_discount_amount', 'min_deposit_percentage', 'min_deposit_amount', 'is_active')
                read_only_fields = ('id',)

        return OAPolicySerializer

    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        org_ids = [user.organisation.pk]
        child_orgs = Organisation.objects.all().all_active().filter(parent_id=user.organisation.pk).values_list('pk',
                                                                                                                flat=True)
        org_ids += child_orgs
        filters = [
            Q(**{'publisher_id__in': org_ids})
        ]
        return filters
