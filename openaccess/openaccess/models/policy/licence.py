from django.db import models
from core.models.base_entity import BaseEntity


class Licence(BaseEntity):
    name = models.CharField(max_length=500)
    description = models.TextField(null=True, blank=True)

    @classmethod
    def create_of_update(cls, name, description,is_active=True):
        instances = cls.objects.filter(name=name)
        if instances.exists():
            instance = instances.first()
        else:
            instance = cls()

        instance.name = name
        instance.description = description
        instance.is_active = is_active
        instance.save()
        return instance
