from django.db import models
from core.models.base_entity import BaseEntity


class APCFundSource(BaseEntity):
    name = models.CharField(max_length=1000)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(APCFundSource, cls).get_serializer()

        class APCFundSourceSerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'name', 'is_active')
                read_only_fields = ('id',)

        return APCFundSourceSerializer


    @classmethod
    def create_of_update(cls, name, is_active=True):
        instances = cls.objects.filter(name=name)

        if instances.exists():
            instance = instances.first()
        else:
            instance = cls()

        instance.name = name
        instance.is_active = is_active
        instance.save()
        return instance
