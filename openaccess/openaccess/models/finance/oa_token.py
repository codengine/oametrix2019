from django.db import models
from core.models.base_entity import BaseEntity
from core.models.organisation.organisation import Organisation
from django.db.models.query_utils import Q
from rest_framework import serializers
from engine.library.clock import Clock
from engine.library.generator import Generator


class OAToken(BaseEntity):
    name = models.CharField(max_length=500)
    publisher = models.ForeignKey(Organisation, on_delete=models.DO_NOTHING)
    token = models.CharField(max_length=100, default='')
    valid_from = models.DateField(null=True, blank=True)
    valid_to = models.DateField(null=True, blank=True)
    is_assigned = models.BooleanField(default=False)
    is_used = models.BooleanField(default=False)

    def is_valid(self):
        now_dt = Clock.utc_now().date()
        if self.valid_from and self.valid_from > now_dt:
            return False
        if self.valid_to and self.valid_to < now_dt:
            return False
        if self.is_used:
            return False
        if not self.is_assigned:
            return False
        return True

    def get_token_dict(self):
        return {
            "id": self.pk,
            "name": self.name,
            "publisher_name": self.publisher.name,
            "token": self.token,
            "id_valid": self.is_valid(),
            "valid_from": self.valid_from,
            "valid_to": self.valid_to
        }

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(OAToken, cls).get_serializer()

        class OATokenSerializer(BaseSerializer):
            publisher_name = serializers.SerializerMethodField()
            lable = serializers.SerializerMethodField()

            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'name', 'publisher', 'publisher_name', 'token', 'valid_from', 'valid_to', 'lable',
                    'is_active')
                read_only_fields = ('id', 'publisher_name')

            def get_publisher_name(self, obj):
                return obj.publisher.name

            def get_lable(self, obj):
                return obj.name + " (" + str(obj.token) + ")"

            def create(self, validated_data):
                validated_data['token'] = Generator.generate_unique_token()
                oa_token = OAToken(**validated_data)
                oa_token.save()
                return oa_token

            def to_internal_value(self, data):
                data['is_active'] = True
                user = self.context.get('request').user
                data['publisher'] = user.organisation.id
                return super(OATokenSerializer, self).to_internal_value(data)

        return OATokenSerializer

    @classmethod
    def filter_queryset(cls, request, queryset, search_request_params, *args, **kwargs):
        for search_request_param in search_request_params:
            if 'fresh_token_only' in search_request_param and search_request_param['search_request_param']:
                now_time = Clock.utc_now()
                filters = [
                    Q(**{'valid_from__lte': now_time, 'valid_to__gte': now_time, 'is_assigned': False, 'is_used': False})
                ]
                queryset = queryset.filter(*filters)
        return queryset

    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        org_ids = [user.organisation.pk]
        child_orgs = Organisation.objects.all().all_active().filter(parent_id=user.organisation.pk).values_list('pk',
                                                                                                                flat=True)
        org_ids += child_orgs
        filters = [
            Q(**{'publisher_id__in': org_ids})
        ]
        return filters
