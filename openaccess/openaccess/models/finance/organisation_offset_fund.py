from django.db import models
from core.models.base_entity import BaseEntity
from core.models.organisation.organisation import Organisation
from openaccess.models.finance.offset_fund import OffsetFund
from django.db.models.query_utils import Q
from rest_framework import serializers, status
from openaccess.models.contract.oa_deal import OADeal
from core.models.finance.currency import Currency
from openaccess.models.contract.deal_type import DealType
from django.db import transaction
from openaccess.models.finance.organisation_wallet import OrganisationWallet
from openaccess.models.finance.wallet import Wallet
from openaccess.models.transactions.offset_fund_transaction import OffsetFundTransaction


class OrganisationOffsetFund(BaseEntity):
    organisation = models.ForeignKey(Organisation, on_delete=models.DO_NOTHING)
    offset_fund = models.ForeignKey(OffsetFund, on_delete=models.DO_NOTHING)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(OrganisationOffsetFund, cls).get_serializer()

        class OrganisationOffsetFundSerializer(BaseSerializer):
            offset_fund = OffsetFund.get_serializer()(required=True)
            organisation_name = serializers.SerializerMethodField()

            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'organisation', 'organisation_name', 'offset_fund', 'is_active')
                read_only_fields = ('id',)

            def get_organisation_name(self, obj):
                return obj.organisation.name

            def is_valid(self, raise_exception=False):
                data = self.initial_data
                organisation_id = data.get('organisation')

                user = self.context.get('request').user

                deal_type_instance = DealType.objects.get(name='pre-payment')

                oa_deal_instances = OADeal.objects.filter(organisation_id=user.organisation.pk,
                                                          counter_organisation_id=organisation_id,
                                                          deal_type_id=deal_type_instance.pk)

                if oa_deal_instances.exists():
                    oa_deal_instance = oa_deal_instances.first()
                else:
                    raise serializers.ValidationError(
                        {"error": "No related OA-Deal found on system. please make sure you have existing oa-deal."},
                        status.HTTP_400_BAD_REQUEST)

                offset_fund = data.get('offset_fund')
                currency = offset_fund['currency']
                try:
                    currency = int(currency)
                except Exception as exp:
                    raise serializers.ValidationError({"error": "Currency ID is not correct"},
                                                      status.HTTP_400_BAD_REQUEST)

                currency_instances = Currency.objects.filter(pk=currency)
                if currency_instances.exists():
                    currency_instance = currency_instances.first()
                else:
                    raise serializers.ValidationError({"error": "Currency does not exist"},
                                                      status.HTTP_400_BAD_REQUEST)

                if oa_deal_instance.currency != currency_instance:
                    raise serializers.ValidationError({"error": "Currency does not match with oa-deal"},
                                                      status.HTTP_400_BAD_REQUEST)

                valid = super(OrganisationOffsetFundSerializer, self).is_valid(raise_exception=raise_exception)
                return valid

            def create(self, validated_data):
                with transaction.atomic():

                    request = self.context.get('request')

                    instance = super(OrganisationOffsetFundSerializer, self).create(validated_data)

                    org_wallet_instances = OrganisationWallet.objects.filter(organisation_id=instance.organisation.pk)
                    if not org_wallet_instances.exists():
                        raise serializers.ValidationError({"error": "No wallet found for institution"},
                                                          status.HTTP_400_BAD_REQUEST)
                    org_wallet_instance = org_wallet_instances.first().wallet
                    org_wallet_instance.add_offset_fund_credit(organisation_instance=request.user.organisation,
                                                               currency_instance=instance.offset_fund.currency,
                                                               balance=instance.offset_fund.amount)

                    of_transaction_instance = OffsetFundTransaction()
                    of_transaction_instance.add_credit_transaction(offset_fund_instance=instance.offset_fund,
                                                                   institution_instance=instance.organisation,
                                                                   publisher_instance=instance.offset_fund.publisher,
                                                                   note='Offset Fund allocated by Publisher')

                    return instance

        return OrganisationOffsetFundSerializer

    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        org_ids = [user.organisation.pk]
        child_orgs = Organisation.objects.all().all_active().filter(parent_id=user.organisation.pk).values_list('pk',
                                                                                                                flat=True)
        org_ids += child_orgs

        filters = [
            Q(**{'organisation_id__in': org_ids}) |  Q(**{'offset_fund__publisher_id': user.organisation.pk})
        ]
        return filters

