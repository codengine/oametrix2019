from django.db import models
from core.models.base_entity import BaseEntity
from core.models.organisation.organisation import Organisation
from openaccess.models.finance.wallet import Wallet


class OrganisationWallet(BaseEntity):
    organisation = models.ForeignKey(Organisation, on_delete=models.DO_NOTHING)
    wallet = models.ForeignKey(Wallet, on_delete=models.DO_NOTHING)

    @classmethod
    def add_new_wallet_to_organisation(cls, organisation_instance):
        organisation_wallet_instances = OrganisationWallet.objects.filter(organisation_id=organisation_instance.pk)
        if organisation_wallet_instances.exists():
            organisation_wallet_instance = organisation_wallet_instances.first()
            return organisation_wallet_instance
        else:
            new_wallet_instance = Wallet.new_wallet()
            organisation_wallet_instance = OrganisationWallet(organisation_id=organisation_instance.pk,
                                                              wallet_id=new_wallet_instance.pk)
            organisation_wallet_instance.save()
            return organisation_wallet_instance