from django.db import models
from core.models.base_entity import BaseEntity
from core.models.finance.currency import Currency
from core.models.organisation.organisation import Organisation
from django.db.models.query_utils import Q
from rest_framework import serializers, status
from engine.library.clock import Clock


class OffsetFund(BaseEntity):
    publisher = models.ForeignKey(Organisation, on_delete=models.DO_NOTHING)
    currency = models.ForeignKey(Currency, on_delete=models.DO_NOTHING)
    amount = models.DecimalField(decimal_places=3, max_digits=20)
    valid_from = models.DateField(null=True, blank=True)
    valid_to = models.DateField(null=True, blank=True)

    def is_valid(self):
        now_dt = Clock.utc_now().date()
        if self.valid_from and self.valid_from > now_dt:
            return False
        if self.valid_to and self.valid_to < now_dt:
            return False
        return True

    def get_offset_fund_dict(self):
        return {
            "publisher_name": self.publisher.name,
            "publisher_id": self.publisher.pk,
            "currency_name": self.currency.name,
            "currency_id": self.currency.pk,
            "amount": self.amount,
            "is_valid": self.is_valid(),
            "valid_from": self.valid_from,
            "valid_to": self.valid_to
        }

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(OffsetFund, cls).get_serializer()

        class OffsetFundSerializer(BaseSerializer):
            publisher_name = serializers.SerializerMethodField()
            currency_name = serializers.SerializerMethodField()

            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'publisher', 'publisher_name', 'currency', 'currency_name', 'amount', 'valid_from',
                    'valid_to',
                    'is_active')
                read_only_fields = ('id', 'publisher_name')

            def get_publisher_name(self, obj):
                return obj.publisher.name

            def get_currency_name(self, obj):
                return obj.currency.name

            def to_internal_value(self, data):
                data['is_active'] = True
                user = self.context.get('request').user
                data['publisher'] = user.organisation.id
                return super(OffsetFundSerializer, self).to_internal_value(data)

        return OffsetFundSerializer

    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        org_ids = [user.organisation.pk]
        child_orgs = Organisation.objects.all().all_active().filter(parent_id=user.organisation.pk).values_list('pk',
                                                                                                                flat=True)
        org_ids += child_orgs
        filters = [
            Q(**{'publisher_id__in': org_ids})
        ]
        return filters
