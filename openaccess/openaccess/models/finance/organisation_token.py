from django.db import models
from core.models.base_entity import BaseEntity
from openaccess.models.finance.oa_token import OAToken
from core.models.organisation.organisation import Organisation
from django.db.models.query_utils import Q
from rest_framework import serializers
from rest_framework import status
from django.utils import timezone
from django.db import transaction
from openaccess.models.transactions.oa_token_transaction import OATokenTransaction
from openaccess.models.finance.organisation_wallet import OrganisationWallet


class OrganisationToken(BaseEntity):
    organisation = models.ForeignKey(Organisation, on_delete=models.DO_NOTHING)
    token = models.ForeignKey(OAToken, on_delete=models.DO_NOTHING)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(OrganisationToken, cls).get_serializer()

        class OrganisationTokenSerializer(BaseSerializer):

            organisation_name = serializers.SerializerMethodField()
            token_info = serializers.SerializerMethodField()

            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'organisation', 'organisation_name', 'token', 'token_info', 'is_active')
                read_only_fields = ('id',)

            def create(self, validated_data):
                token = validated_data.get('token')
                organisation = validated_data.get('organisation')
                if token.valid_to < timezone.now().date():
                    raise serializers.ValidationError({"error": "Token has been expired."},
                                                      status.HTTP_400_BAD_REQUEST)

                instance = OrganisationToken.objects.filter(token=token)
                if instance:
                    raise serializers.ValidationError({"error": "Token has already been used."},
                                                      status.HTTP_400_BAD_REQUEST)

                instance = super(OrganisationTokenSerializer, self).create(validated_data)
                token.is_assigned = True

                with transaction.atomic():
                    token.save()
                    
                    org_wallet_instances = OrganisationWallet.objects.filter(organisation_id=instance.organisation.pk)
                    if not org_wallet_instances.exists():
                        raise serializers.ValidationError({"error": "No wallet found for publisher"},
                                                          status.HTTP_400_BAD_REQUEST)
                    org_wallet_instance = org_wallet_instances.first().wallet
                    
                    org_wallet_instance.add_token(instance.token)

                    oa_token_transaction_instance = OATokenTransaction()
                    oa_token_transaction_instance.add_credit_transaction(token_instance=instance.token,
                                                                         institution_instance=instance.organisation,
                                                                         publisher_instance=instance.token.publisher,
                                                                         note='Publisher allocated token to institution')

                return instance

            def get_organisation_name(self, obj):
                return obj.organisation.name

            def get_token_info(self, obj):
                return obj.token.token

        return OrganisationTokenSerializer

    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        org_ids = [user.organisation.pk]
        child_orgs = Organisation.objects.all().all_active().filter(parent_id=user.organisation.pk).values_list('pk',
                                                                                                                flat=True)
        org_ids += child_orgs

        filters = [
            Q(**{'organisation_id__in': org_ids}) | Q(**{'token__publisher_id': user.organisation.pk})
        ]
        return filters
