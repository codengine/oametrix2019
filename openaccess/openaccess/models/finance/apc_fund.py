from django.db import models
from core.models.base_entity import BaseEntity
from core.models.finance.currency import Currency
from openaccess.models.finance.apc_funder import APCFunder
from rest_framework import serializers


class APCFund(BaseEntity):
    title = models.CharField(max_length=1000)
    source = models.ForeignKey(APCFunder, on_delete=models.DO_NOTHING)
    currency = models.ForeignKey(Currency, on_delete=models.DO_NOTHING)
    amount = models.DecimalField(decimal_places=3, max_digits=20)
    note = models.TextField(null=True, blank=True)


    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(APCFund, cls).get_serializer()

        class APCFundSerializer(BaseSerializer):
            source_name = serializers.SerializerMethodField()
            currency_name = serializers.SerializerMethodField()
            class Meta(BaseSerializer.Meta):
                fields = (
                'id', 'title', 'source', 'source_name', 'currency', 'currency_name', 'amount', 'note',
                'is_active')
                read_only_fields = ('id',)

            def get_source_name(self, obj):
                return  obj.source.name

            def get_currency_name(self, obj):
                return  obj.currency.name

        return APCFundSerializer

