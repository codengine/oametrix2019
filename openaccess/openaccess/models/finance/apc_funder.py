from django.db import models
from core.models.base_entity import BaseEntity
from core.models.contacts.address import Address


# [Like BD GOVT]
class APCFunder(BaseEntity):
    name = models.CharField(max_length=1000)
    address = models.ForeignKey(Address, null=True, blank=True, on_delete=models.CASCADE)
    website = models.CharField(max_length=2000, null=True, blank=True)
    email = models.EmailField(max_length=100, null=True, blank=True)
    alt_email = models.EmailField(max_length=100, null=True, blank=True)
    phone1 = models.CharField(max_length=100, null=True, blank=True)
    phone2 = models.CharField(max_length=100, null=True, blank=True)
    phone3 = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(APCFunder, cls).get_serializer()

        class APCFunderSerializer(BaseSerializer):
            address = Address.get_serializer()(required=False)

            class Meta(BaseSerializer.Meta):
                address = Address.get_serializer()(required=False)
                fields = ('id', 'name', 'address', 'website', 'email', 'alt_email',
                          'phone1', 'phone2', 'phone3', 'description', 'is_active')
                read_only_fields = ('id',)

        return APCFunderSerializer


