from django.db import models
from rest_framework import serializers, status
from core.constants.transaction_type import TransactionType
from core.models.base_entity import BaseEntity
from core.models.finance.currency import Currency
from core.models.finance.wallet_abstract import WalletAbstract, WalletBalance
from core.models.organisation.organisation import Organisation
from engine.library.clock import Clock
from openaccess.models.finance.oa_token import OAToken
from openaccess.models.finance.offset_fund import OffsetFund
from decimal import Decimal


class DepositCredit(BaseEntity):
    currency = models.ForeignKey(Currency, on_delete=models.DO_NOTHING)
    credit = models.DecimalField(decimal_places=3, max_digits=20, default=0.0)
    organisation = models.ForeignKey(Organisation, on_delete=models.DO_NOTHING)

    def add_balance(self, balance):
        cur_bal = Decimal(self.credit)
        new_bal = cur_bal + balance
        self.credit = new_bal
        self.save()
        return self

    def debit_balance(self, balance):
        cur_bal = Decimal(self.credit)
        new_bal = cur_bal - balance
        if new_bal < 0:
            raise serializers.ValidationError({"error": "Insufficient fund to debit balance"},
                                              status.HTTP_400_BAD_REQUEST)
        self.credit = new_bal
        self.save()
        return self

    def get_deposit_credit_dict(self):
        return {
            "organisation_name": self.organisation.name,
            "organisation_id": self.organisation.pk,
            "currency_id": self.currency.pk,
            "currency_name": self.currency.short_name,
            "credit": self.credit.get_balance()
        }


class OffsetFundCredit(BaseEntity):
    currency = models.ForeignKey(Currency, on_delete=models.DO_NOTHING)
    credit = models.DecimalField(decimal_places=3, max_digits=20, default=0.0)
    organisation = models.ForeignKey(Organisation, on_delete=models.DO_NOTHING)
    valid_from = models.DateTimeField(null=True, blank=True)
    valid_to = models.DateTimeField(null=True, blank=True)

    def add_balance(self, balance):
        cur_bal = Decimal(self.credit)
        new_bal = cur_bal + balance
        self.credit = new_bal
        self.save()
        return self

    def debit_balance(self, balance):
        cur_bal = Decimal(self.credit)
        new_bal = cur_bal - balance

        if new_bal < 0:
            raise serializers.ValidationError({"error": "Insufficient fund to debit balance"},
                                              status.HTTP_400_BAD_REQUEST)
        self.credit = new_bal
        self.save()
        return self

    def get_deposit_credit_dict(self):
        return {
            "organisation_name": self.organisation.name,
            "organisation_id": self.organisation.pk,
            "currency_id": self.currency.pk,
            "currency_name": self.currency.short_name,
            "credit": self.credit.get_balance()
        }


class Wallet(WalletAbstract):
    tokens = models.ManyToManyField(OAToken) # Tokens that owned by the Wallet owner
    offset_funds = models.ManyToManyField(OffsetFundCredit) # Offset Fund that is owned by the Wallet Owner
    deposit_credits = models.ManyToManyField(DepositCredit) # Credits usable by some other organisation
    
    def add_token(self, token_instance):
        self.tokens.add(token_instance)
        return token_instance
    
    def remove_token(self, token_instance):
        self.tokens.remove(token_instance)
        return token_instance

    def add_offset_fund_credit(self, organisation_instance, currency_instance, balance):
        now_dt = Clock.utc_now().date()
        offset_fund_instances = self.offset_funds.filter(organisation_id=organisation_instance.pk,
                                                         currency_id=currency_instance.pk,
                                                         valid_from__lte=now_dt, valid_to__gte=now_dt)
        if offset_fund_instances.exists():
            offset_fund_credit_instance = offset_fund_instances.first()
        else:
            start_time = Clock.start_of_year()
            end_time = Clock.end_of_year()
            offset_fund_credit_instance = OffsetFundCredit(organisation_id=organisation_instance.pk,
                                                    currency_id=currency_instance.pk, valid_from= start_time, valid_to=end_time)
            offset_fund_credit_instance.save()
            self.offset_funds.add(offset_fund_credit_instance)
        offset_fund_credit_instance.add_balance(balance)
        return offset_fund_credit_instance

    def debit_offset_fund_credit(self, organisation_instance, currency_instance, balance):

        offset_fund_instances = self.offset_funds.filter(organisation_id=organisation_instance.pk,
                                                         currency_id=currency_instance.pk)
        if offset_fund_instances.exists():
            offset_fund_instance = offset_fund_instances.first()
            offset_fund_instance.debit_balance(balance)
            return offset_fund_instance
        else:
            raise serializers.ValidationError({"error": "No balance found to perform a debit transaction"},
                                              status.HTTP_400_BAD_REQUEST)

    def add_deposit_credit(self, organisation_instance, currency_instance, balance):
        deposit_credit_instances = self.deposit_credits.filter(organisation_id=organisation_instance.pk,
                                                               currency_id=currency_instance.pk)
        if deposit_credit_instances.exists():
            deposit_credit_instance = deposit_credit_instances.first()
        else:
            deposit_credit_instance = DepositCredit(organisation_id=organisation_instance.pk,
                                                    currency_id=currency_instance.pk)
            deposit_credit_instance.save()
            self.deposit_credits.add(deposit_credit_instance)

        deposit_credit_instance.add_balance(balance)
        return deposit_credit_instance

    def debit_deposit_credit(self, organisation_instance, currency_instance, balance):

        deposit_credit_instances = self.deposit_credits.filter(organisation_id=organisation_instance.pk,
                                                               currency_id=currency_instance.pk)
        if deposit_credit_instances.exists():
            deposit_credit_instance = deposit_credit_instances.first()
            deposit_credit_instance.debit_balance(balance=balance)
            return deposit_credit_instance
        else:
            raise serializers.ValidationError({"error": "No balance found to perform a debit transaction"},
                                              status.HTTP_400_BAD_REQUEST)

    def get_wallet_balance(self):
        wallet_balance_instances = self.balance.all()
        wallet_balance_list = []
        for wallet_balance_instance in wallet_balance_instances:
            bal_dict = wallet_balance_instance.get_balance()
            wallet_balance_list += [bal_dict]
        return wallet_balance_list

    def get_tokens(self, only_valid=False):
        token_instances = self.tokens.all()
        token_list = []
        for token_instance in token_instances:
            token_dict = token_instance.get_token_dict()
            token_list += [token_dict]
        return token_list

    def get_offset_funds(self, only_valid=False):
        offset_fund_instances = self.offset_funds.all()
        offset_fund_list = []
        for offset_fund_instance in offset_fund_instances:
            offset_fund_dict = offset_fund_instance.get_offset_fund_dict()
            offset_fund_list += [offset_fund_dict]
        return offset_fund_list

    def get_deposit_credits(self):
        dc_instances = self.deposit_credits.all()
        dc_list = []
        for dc_instance in dc_instances:
            dc_dict = dc_instance.get_deposit_credit_dict()
            dc_list += [dc_dict]
        return dc_list

    def get_wallet_snapshot(self):
        return {
            "balance": self.get_wallet_balance(),
            "tokens": self.get_tokens(),
            "offset_fund": self.get_offset_funds(),
            "deposit_credits": self.get_deposit_credits()
        }

    def credit_balance(self, currency_instance, amount):
        # Check if Wallet Balance already exists for the same currency
        wallet_balance_instances = self.balance.filter(currency_id=currency_instance.pk)

        same_currency_exists = wallet_balance_instances.exists()

        if same_currency_exists:
            wallet_balance_instance = wallet_balance_instances.first()
        else:
            wallet_balance_instance = WalletBalance(currency_id=currency_instance.pk)
            wallet_balance_instance.save()

            self.balance.add(wallet_balance_instance)

        wallet_balance_instance.update_balance(amount, transaction_type=TransactionType.CREDIT.value)

    def debit_balance(self, currency_instance, amount):
        # Check if Wallet Balance already exists for the same currency
        wallet_balance_instances = self.balance.filter(currency_id=currency_instance.pk)

        same_currency_exists = wallet_balance_instances.exists()

        if same_currency_exists:
            wallet_balance_instance = wallet_balance_instances.first()
        else:
            raise Exception("No balance record found. Insufficient fund to debit amount")

        wallet_balance_instance.update_balance(amount, transaction_type=TransactionType.DEBIT.value)

    @classmethod
    def new_wallet(cls):
        wallet_instance = Wallet()
        wallet_instance.save()
        return wallet_instance



