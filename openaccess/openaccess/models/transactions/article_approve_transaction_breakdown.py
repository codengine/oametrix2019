from decimal import Decimal
from django.db import models
from rest_framework import serializers
from rest_framework import status
from core.models.base_entity import BaseEntity
from openaccess.models.article.article_full import ArticleFull
from openaccess.models.contract.oa_deal import OADeal
from openaccess.models.finance.oa_token import OAToken
from openaccess.models.policy.apc_option import APCOption
from openaccess.models.policy.fund_source import APCFundSource
from core.models.finance.currency import Currency


class ArticleApproveTransactionBreakdown(BaseEntity):
    article = models.ForeignKey(ArticleFull, on_delete=models.DO_NOTHING)
    apc_option = models.ForeignKey(APCOption, on_delete=models.DO_NOTHING)  # DepositCredit, OffsetFund, OAToken
    token = models.ForeignKey(OAToken, null=True, blank=True, on_delete=models.DO_NOTHING)
    charge_description = models.TextField(null=True, blank=True)
    fund_source = models.ForeignKey(APCFundSource, null=True, blank=True,
                                    on_delete=models.DO_NOTHING)  # Department, Funder, Author, Publisher, Other
    funder_name = models.CharField(max_length=1000, null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    currency = models.ForeignKey(Currency, null=True, blank=True, on_delete=models.DO_NOTHING)
    amount = models.DecimalField(null=True, blank=True, decimal_places=3, max_digits=20)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(ArticleApproveTransactionBreakdown, cls).get_serializer()

        class ArticleApproveTransactionBreakdownSerializer(BaseSerializer):

            def create(self, validated_data):
                article_instance = validated_data.get('article')
                publisher_instance = article_instance.publisher
                article_author = article_instance.author
                author_institution = article_author.get_organisation()

                pre_payment_deal_instance = OADeal.get_prepayment_deal(publisher_instance=publisher_instance,
                                                                       institution_instance=author_institution)

                if not pre_payment_deal_instance:
                    raise serializers.ValidationError({"error": "OADeal does not exist between "
                                                                "publisher and institution"},
                                                      status.HTTP_400_BAD_REQUEST)
                validated_data['currency'] = pre_payment_deal_instance.currency
                instance = super(ArticleApproveTransactionBreakdownSerializer, self).create(validated_data=validated_data)
                instance.is_active = True
                instance.save()
                return instance

            def is_valid(self, raise_exception=False):
                initial_data = self.initial_data
                apc_option_id = initial_data['apc_option']
                try:
                    apc_option_id = int(apc_option_id)
                    apc_option_instance = APCOption.objects.get(pk=apc_option_id)
                except Exception as exp:
                    raise serializers.ValidationError({"error": "APC Option not correct"},
                                                      status.HTTP_400_BAD_REQUEST)

                if apc_option_instance.is_deposit_fund():
                    fund_source = initial_data.get('fund_source')
                    try:
                        fund_source = int(fund_source)
                        fund_source_instance = APCFundSource.objects.get(pk=fund_source)
                    except Exception as exp:
                        raise serializers.ValidationError({"error": "Fund Source invalid"},
                                                          status.HTTP_400_BAD_REQUEST)
                    funder_name = initial_data.get('funder_name')
                    charge_description = initial_data.get('charge_description')
                    if not funder_name:
                        raise serializers.ValidationError({"error": "Funder Name is required"},
                                                          status.HTTP_400_BAD_REQUEST)
                    if not charge_description:
                        raise serializers.ValidationError({"error": "Charge description is required"},
                                                          status.HTTP_400_BAD_REQUEST)
                    amount = initial_data.get('amount')
                    try:
                        amount = Decimal(amount)
                    except Exception as exp:
                        raise serializers.ValidationError({"error": "Amount must be decimal"},
                                                          status.HTTP_400_BAD_REQUEST)
                elif apc_option_instance.is_offset_fund():
                    fund_source = initial_data.get('fund_source')
                    try:
                        fund_source = int(fund_source)
                        fund_source_instance = APCFundSource.objects.get(pk=fund_source)
                    except Exception as exp:
                        raise serializers.ValidationError({"error": "Fund Source invalid"},
                                                          status.HTTP_400_BAD_REQUEST)
                    funder_name = initial_data.get('funder_name')
                    charge_description = initial_data.get('charge_description')
                    if not funder_name:
                        raise serializers.ValidationError({"error": "Funder Name is required"},
                                                          status.HTTP_400_BAD_REQUEST)
                    if not charge_description:
                        raise serializers.ValidationError({"error": "Charge description is required"},
                                                          status.HTTP_400_BAD_REQUEST)
                    amount = initial_data.get('amount')
                    try:
                        amount = Decimal(amount)
                    except Exception as exp:
                        raise serializers.ValidationError({"error": "Amount must be decimal"},
                                                          status.HTTP_400_BAD_REQUEST)
                elif apc_option_instance.is_oa_token():
                    token_id = initial_data.get('token')
                    try:
                        token_id = int(token_id)
                        token_instance = OAToken.objects.get(pk=token_id)
                    except Exception as exp:
                        raise serializers.ValidationError({"error": "Token ID not correct"},
                                                          status.HTTP_400_BAD_REQUEST)
                    if not token_instance.is_valid():
                        raise serializers.ValidationError({"error": "Token is not available to use"},
                                                          status.HTTP_400_BAD_REQUEST)
                    charge_description = initial_data.get('charge_description')
                    if not charge_description:
                        raise serializers.ValidationError({"error": "Charge description missing"},
                                                          status.HTTP_400_BAD_REQUEST)

                else:
                    raise serializers.ValidationError({"error": "Invalid APC Option"},
                                                      status.HTTP_400_BAD_REQUEST)

                valid = super(ArticleApproveTransactionBreakdownSerializer, self).is_valid(raise_exception=raise_exception)
                return valid

            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'article', 'apc_option', 'token', 'charge_description', 'fund_source', 'funder_name', 'note', 'currency','amount',
                    'is_active')
                read_only_fields = ('id',)

        return ArticleApproveTransactionBreakdownSerializer
