from django.db import models
from core.models.transactions.transaction_abstract import TransactionAbstract
from openaccess.constants.transaction_source import TransactionSource
from openaccess.models.finance.oa_token import OAToken
from openaccess.models.finance.offset_fund import OffsetFund
from openaccess.models.finance.wallet import Wallet


class WalletTransaction(TransactionAbstract):
    source = models.CharField(max_length=20, null=True, blank=True) #TransactionSource.FUNDER.value
    token = models.ForeignKey(OAToken, null=True, blank=True, on_delete=models.DO_NOTHING)
    offset_fund = models.ForeignKey(OffsetFund, null=True, blank=True, on_delete=models.DO_NOTHING)
    from_wallet = models.ForeignKey(Wallet, related_name='+', on_delete=models.DO_NOTHING)
    to_wallet = models.ForeignKey(Wallet, related_name='+', on_delete=models.DO_NOTHING)
