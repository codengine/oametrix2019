from django.db import models
from core.constants.transaction_type import TransactionType
from core.models.base_entity import BaseEntity


class OATokenTransaction(BaseEntity):
    transaction_type = models.CharField(max_length=20)  # TransactionType.DEBIT.value or TransactionType.CREDIT.value
    token = models.ForeignKey('OAToken', on_delete=models.DO_NOTHING)
    article = models.ForeignKey('ArticleFull', null=True, blank=True, on_delete=models.DO_NOTHING)
    institution = models.ForeignKey('core.Organisation', related_name='+', on_delete=models.DO_NOTHING)
    publisher = models.ForeignKey('core.Organisation', related_name='+', on_delete=models.DO_NOTHING)
    note = models.TextField(null=True, blank=True)
    
    @classmethod
    def find_latest_debit_transaction(cls, article_instance, institution_instance, publisher_instance, **kwargs):
        oa_token_transactions = OATokenTransaction.objects.filter(article_id=article_instance.pk, 
                                                                  institution_id=institution_instance.pk, 
                                                                  publisher_id=publisher_instance.pk, 
                                                                  transaction_type=TransactionType.DEBIT.value).order_by('-id')
        return oa_token_transactions.first()

    @classmethod
    def find_latest_credit_transaction(cls, institution_instance, publisher_instance, **kwargs):
        oa_token_transactions = OATokenTransaction.objects.filter(institution_id=institution_instance.pk, 
                                                                  publisher_id=publisher_instance.pk, 
                                                                  transaction_type=TransactionType.CREDIT.value).order_by('-id')
        return oa_token_transactions.first()

    def add_credit_transaction(self, token_instance, institution_instance,
                               publisher_instance, **kwargs):
        self.transaction_type = TransactionType.CREDIT.value
        self.token_id = token_instance.pk
        self.institution_id = institution_instance.pk
        self.publisher_id = publisher_instance.pk
        if 'note' in kwargs:
            self.note = kwargs.get('note', 'CREDIT Transaction')
        self.save()
        return self

    def add_debit_transaction(self, article_instance, token_instance,
                              institution_instance, publisher_instance, **kwargs):
        self.transaction_type = TransactionType.DEBIT.value
        self.token_id = token_instance.pk
        self.article_id = article_instance.pk
        self.institution_id = institution_instance.pk
        self.publisher_id = publisher_instance.pk
        if 'note' in kwargs:
            self.note = kwargs.get('note', 'DEBIT Transaction')
        self.save()
        return self
    
    def add_reverse_debit_transaction(self, article_instance, token_instance,
                              institution_instance, publisher_instance, **kwargs):
        self.transaction_type = TransactionType.DEBIT.value
        self.token_id = token_instance.pk
        self.article_id = article_instance.pk
        self.institution_id = institution_instance.pk
        self.publisher_id = publisher_instance.pk
        if 'note' in kwargs:
            self.note = kwargs.get('note', 'DEBIT Transaction')
        if kwargs.get('reference_model'):
            self.reference_model = kwargs.get('reference_model')
        if kwargs.get('reference_id'):
            self.reference_id = kwargs.get('reference_id')
        self.save()
        return self


