from django.db import models
from core.constants.transaction_type import TransactionType
from core.models.transactions.transaction_abstract import TransactionAbstract


class OffsetFundTransaction(TransactionAbstract):
    offset_fund = models.ForeignKey('OffsetFund', null=True, blank=True, on_delete=models.DO_NOTHING)
    article = models.ForeignKey('ArticleFull', null=True, blank=True, on_delete=models.DO_NOTHING)
    institution = models.ForeignKey('core.Organisation', related_name='+', on_delete=models.DO_NOTHING)
    publisher = models.ForeignKey('core.Organisation', related_name='+', on_delete=models.DO_NOTHING)

    @classmethod
    def find_latest_debit_transaction(cls, article_instance, institution_instance, publisher_instance, **kwargs):
        of_transactions = OffsetFundTransaction.objects.filter(article_id=article_instance.pk,
                                                               institution_id=institution_instance.pk,
                                                               publisher_id=publisher_instance.pk,
                                                               transaction_type=TransactionType.DEBIT.value).order_by(
            '-id')
        return of_transactions.last()

    @classmethod
    def find_latest_credit_transaction(cls, institution_instance, publisher_instance, **kwargs):
        of_transactions = OffsetFundTransaction.objects.filter(institution_id=institution_instance.pk,
                                                               publisher_id=publisher_instance.pk,
                                                               transaction_type=TransactionType.CREDIT.value).order_by(
            '-id')
        return of_transactions.first()

    def add_credit_transaction(self, offset_fund_instance, institution_instance,
                               publisher_instance, **kwargs):

        self.institution_id = institution_instance.pk
        self.publisher_id = publisher_instance.pk
        self.transaction_type = TransactionType.CREDIT.value
        self.currency_id = offset_fund_instance.currency_id
        self.amount = offset_fund_instance.amount
        self.offset_fund_id = offset_fund_instance.pk
        if 'note' in kwargs:
            self.note = kwargs.get('note', 'CREDIT Transaction')
        self.save()
        return self

    def add_debit_transaction(self, article_instance, currency_instance,
                              institution_instance, publisher_instance, amount, **kwargs):
        self.institution_id = institution_instance.pk
        self.publisher_id = publisher_instance.pk
        self.transaction_type = TransactionType.DEBIT.value
        self.currency_id = currency_instance.pk
        self.amount = amount
        self.article_id = article_instance.pk
        if 'note' in kwargs:
            self.note = kwargs.get('note', 'DEBIT Transaction')
        self.save()
        return self

    def add_reverse_debit_transaction(self, article_instance, currency_instance,
                                      institution_instance, publisher_instance, amount, **kwargs):
        self.institution_id = institution_instance.pk
        self.publisher_id = publisher_instance.pk
        self.transaction_type = TransactionType.REVERSE_DEBIT.value
        self.currency_id = currency_instance.pk
        self.amount = amount
        self.article_id = article_instance.pk
        if 'note' in kwargs:
            self.note = kwargs.get('note', 'DEBIT Transaction')
        if kwargs.get('reference_model'):
            self.reference_model = kwargs.get('reference_model')
        if kwargs.get('reference_id'):
            self.reference_id = kwargs.get('reference_id')
        self.save()
        return self
