from django.db import models
from core.models.auth.user import User
from core.models.base_entity import BaseEntity
from engine.library.clock import Clock
from openaccess.constants.notification_read_status import NotificationReadStatus

__author__ = 'Sohel'


class Notification(BaseEntity):
    read = models.IntegerField(NotificationReadStatus.UNREAD.value)
    notification_type = models.CharField(max_length=200,null=True,blank=True)
    object_class = models.CharField(max_length=20)
    object_id = models.IntegerField()
    text = models.TextField(null=True,blank=True)
    title = models.TextField(null=True,blank=True)
    url = models.CharField(max_length=500,null=True,blank=True)
    target_user = models.ForeignKey(User,related_name='notification_for', on_delete=models.DO_NOTHING)
    notification_date = models.DateTimeField(null=True, blank=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None, update_date=True):
        if update_date:
            self.notification_date = Clock.utc_now()
        super(Notification, self).save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

