from django.db import models
from core.models.base_entity import BaseEntity
from core.models.finance.currency import Currency


class OATarget(BaseEntity):
    year = models.IntegerField(null=True, blank=True)
    currency = models.ForeignKey(Currency, on_delete=models.DO_NOTHING)
    target = models.DecimalField(decimal_places=3, max_digits=20)
    note = models.TextField(null=True, blank=True)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(OATarget, cls).get_serializer()

        class OATargetSerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'year', 'currency', 'target', 'note', 'is_active')
                read_only_fields = ('id',)

        return OATargetSerializer
