import logging
from django.dispatch import receiver
from django.db.models.signals import post_save
from core.models.organisation.organisation import Organisation
from openaccess.models.finance.organisation_wallet import OrganisationWallet

logger = logging.getLogger('openaccess')


@receiver(post_save, sender=Organisation)
def add_wallet_to_organisation(sender, instance, **kwargs):
    if sender.__name__ == Organisation.__name__:

        logger.info("Adding wallet to Organisation")

        OrganisationWallet.add_new_wallet_to_organisation(instance)

        logger.info("Wallet added to the organisation")
