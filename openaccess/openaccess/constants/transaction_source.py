from enum import Enum


class TransactionSource(Enum):
    FUNDER = "Funder"
    DEPOSIT = "Deposit"
    TOKEN = "Token"
    OFFSET_FUND = "OffsetFund"
    APPROVAL = "Approval"
    OTHER = "Other"

