from enum import Enum


class NotificationReadStatus(Enum):
    READ = "read"
    UNREAD = "unread"
