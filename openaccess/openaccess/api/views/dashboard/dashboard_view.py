from rest_framework import status
from rest_framework.response import Response
from core.api.authorization.is_authorized_dashboard import IsAuthorizedDashboard
from core.api.constants.permission_code import PermCode
from core.api.decorators.permission_decorators import require_permissions
from core.api.mixins.viewmixins.protected_view_mixin import ProtectedViewMixin
from core.api.views.base_api_view import BaseAPIView
from openaccess.api.views.dashboard.components.renderers.dashboard_renderer import DashboardRenderer


class DashboardAPIView(ProtectedViewMixin, BaseAPIView):
    permission_classes = (IsAuthorizedDashboard,)
    http_method_names = ('get', )

    def get(self, request, *args, **kwargs):
        try:
            dashboard_content = DashboardRenderer.render(request, **kwargs)
            return Response(data={'data': dashboard_content}, status=status.HTTP_200_OK)
        except Exception as exp:
            return Response(data={'message': 'Internal Server Error'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
