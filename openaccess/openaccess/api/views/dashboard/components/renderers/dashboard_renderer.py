import inspect
import sys

from engine.library.app_util import get_all_models
from openaccess.api.views.dashboard.components.dashboard_component import DashboardComponent
from openaccess.api.views.dashboard.components.institution.article_summary_component import ArticleSummaryComponent
from openaccess.api.views.dashboard.components.institution.available_fund_component import AvailableFundComponent
from openaccess.api.views.dashboard.components.institution.average_apc_component import AverageAPCComponent
from openaccess.api.views.dashboard.components.institution.new_deals_component import NewDealsComponent
from openaccess.api.views.dashboard.components.institution.oa_journal_component import OAJournalComponent
from openaccess.api.views.dashboard.components.institution.oa_spent_component import OASpentComponent
from openaccess.api.views.dashboard.components.institution.offset_summary_component import OffsetSummaryComponent
from openaccess.api.views.dashboard.components.institution.read_and_publish_deal_component import \
    ReadAndPublishDealComponent
from openaccess.api.views.dashboard.components.institution.token_summary_component import TokenSummaryComponent as InstitutionTokenSummaryComponent
from openaccess.api.views.dashboard.components.institution.total_deposit_component import TotalDepositComponent
from openaccess.api.views.dashboard.components.publisher.allocated_offset_component import AllocatedOffsetComponent
from openaccess.api.views.dashboard.components.publisher.available_fund_component import AvailableFundComponent as PubAvailableFundComponent
from openaccess.api.views.dashboard.components.publisher.available_offset_component import AvailableOffsetComponent
from openaccess.api.views.dashboard.components.publisher.deposit_component import DepositComponent as PubDepositComponent
from openaccess.api.views.dashboard.components.publisher.oa_income_component import OAIncomeComponent
from openaccess.api.views.dashboard.components.publisher.oadeal_list_component import OADealListComponent
from openaccess.api.views.dashboard.components.publisher.spent_offset_component import SpentOffsetComponent
from openaccess.api.views.dashboard.components.publisher.token_summary_component import TokenSummaryComponent


class DashboardRenderer(object):
    @classmethod
    def get_all_components(cls, request, **kwargs):

        if request.user.is_publisher_user():
            return [
                AllocatedOffsetComponent(),
                ArticleSummaryComponent(),
                PubAvailableFundComponent(),
                AvailableOffsetComponent(),
                PubDepositComponent(),
                OAIncomeComponent(),
                OADealListComponent(),
                SpentOffsetComponent(),
                TokenSummaryComponent()
            ]
        else:
            return [
                ArticleSummaryComponent(),
                AvailableFundComponent(),
                AverageAPCComponent(),
                NewDealsComponent(),
                OAJournalComponent(),
                OASpentComponent(),
                OffsetSummaryComponent(),
                ReadAndPublishDealComponent(),
                InstitutionTokenSummaryComponent(),
                TotalDepositComponent()
            ]
        
    @classmethod
    def render(cls, request, **kwargs):
        filter_component_keys = request.GET.get('components')
        if filter_component_keys:
            filter_component_keys = filter_component_keys.split(',')
            filter_component_keys = [fc.strip() for fc in filter_component_keys]

        filter_component_keys = filter_component_keys or 'all'
        
        _rendered = {}
        components = cls.get_all_components(request, **kwargs)
        if filter_component_keys == 'all':
            filter_component_keys = [c.get_component_key() for c in components]
        for component in components:
            component_key = component.get_component_key()
            if component_key in filter_component_keys:
                if component.is_permitted(request, **kwargs):
                    component_rendered_body = component.render(request, **kwargs)
                    _rendered[component_key] = component_rendered_body
        return _rendered
