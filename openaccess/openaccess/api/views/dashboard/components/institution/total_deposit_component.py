from openaccess.api.views.dashboard.components.dashboard_component import DashboardComponent


class TotalDepositComponent(DashboardComponent):
    
    def is_permitted(self, request, **kwargs):
        return True
    
    def prepare_response(self, request, **kwargs):
        td_usd = 0.0
        td_eur = 0.0
        td_gbp = 0.0
        return {
            "usd": td_usd,
            "eur": td_eur,
            "gbp": td_gbp
        }
