from openaccess.api.views.dashboard.components.dashboard_component import DashboardComponent


class OffsetSummaryComponent(DashboardComponent):
    
    def is_permitted(self, request, **kwargs):
        return True
    
    def prepare_response(self, request, **kwargs):
        os_usd = 0.0
        os_eur = 0.0
        os_gbp = 0.0
        return {
            "usd": os_usd,
            "eur": os_eur,
            "gbp": os_gbp
        }
