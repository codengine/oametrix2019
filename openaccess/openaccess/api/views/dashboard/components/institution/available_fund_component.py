from openaccess.api.views.dashboard.components.dashboard_component import DashboardComponent


class AvailableFundComponent(DashboardComponent):
    
    def is_permitted(self, request, **kwargs):
        return True
    
    def prepare_response(self, request, **kwargs):
        af_usd = 0.0
        af_eur = 0.0
        af_gbp = 0.0
        return {
            "usd": af_usd,
            "eur": af_eur,
            "gbp": af_gbp
        }
