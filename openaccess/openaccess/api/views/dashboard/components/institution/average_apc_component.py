from openaccess.api.views.dashboard.components.dashboard_component import DashboardComponent


class AverageAPCComponent(DashboardComponent):
    
    def is_permitted(self, request, **kwargs):
        return True
    
    def prepare_response(self, request, **kwargs):
        avg_apc_usd = 0
        avg_apc_eur = 0
        avg_apc_gbp = 0
        return {
            "usd": avg_apc_usd,
            "eur": avg_apc_eur,
            "gbp": avg_apc_gbp
        }
