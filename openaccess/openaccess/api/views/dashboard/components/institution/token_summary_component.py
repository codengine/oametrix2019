from openaccess.api.views.dashboard.components.dashboard_component import DashboardComponent


class TokenSummaryComponent(DashboardComponent):
    
    def is_permitted(self, request, **kwargs):
        return True
    
    def prepare_response(self, request, **kwargs):
        available = 0
        allocated = 0
        spent = 0
        return {
            "available": available,
            "allocated": allocated,
            "spent": spent
        }
