from openaccess.api.views.dashboard.components.dashboard_component import DashboardComponent


class NewDealsComponent(DashboardComponent):
    
    def is_permitted(self, request, **kwargs):
        return True
    
    def prepare_response(self, request, **kwargs):
        new_deals = [] # [{'publisher': publisher_name, 'country': publisher_country}]
        return {
            'new_deals': new_deals
        }
  
