from openaccess.api.views.dashboard.components.dashboard_component import DashboardComponent


class OAJournalComponent(DashboardComponent):
    
    def is_permitted(self, request, **kwargs):
        return True
    
    def prepare_response(self, request, **kwargs):
        gold = 0
        hybrid = 0
        return {
            "gold": gold,
            "hybrid": hybrid
        }
