from openaccess.api.views.dashboard.components.dashboard_component import DashboardComponent


class ReadAndPublishDealComponent(DashboardComponent):
    
    def is_permitted(self, request, **kwargs):
        return True
    
    def prepare_response(self, request, **kwargs):
        read_fee = 0.0
        publish_fee = 0.0
        total_fee = 0.0
        total_articles = 0
        average_apc_cost = 0.0
        return {
            "read_fee": read_fee,
            "publish_fee": publish_fee,
            "total_fee": total_fee,
            "total_articles": total_articles,
            "average_apc_cost": average_apc_cost
        }
