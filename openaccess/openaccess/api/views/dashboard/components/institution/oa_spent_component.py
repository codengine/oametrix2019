from openaccess.api.views.dashboard.components.dashboard_component import DashboardComponent


class OASpentComponent(DashboardComponent):
    
    def is_permitted(self, request, **kwargs):
        return True
    
    def prepare_response(self, request, **kwargs):
        oa_spent_usd = 0.0
        oa_spent_eur = 0.0
        oa_spent_gbp = 0.0
        return {
            "usd": oa_spent_usd,
            "eur": oa_spent_eur,
            "gbp": oa_spent_gbp
        }
