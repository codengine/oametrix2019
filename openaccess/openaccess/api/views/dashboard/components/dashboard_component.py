import re
from rest_framework import status


class DashboardComponent(object):
    
    @classmethod
    def convert_to_snake_case(cls, name, placeholder="-"):
        if placeholder == "-":
            string = re.sub('(.)([A-Z][a-z]+)', r'\1-\2', name)
            return re.sub('([a-z0-9])([A-Z])', r'\1-\2', string).lower()
        elif placeholder == "_":
            string = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
            return re.sub('([a-z0-9])([A-Z])', r'\1_\2', string).lower()
    
    def get_component_key(self):
        return self.__class__.convert_to_snake_case(name=self.__class__.__name__)
    
    def is_public(self):
        return False
    
    def prepare_json_content(self, status_code, data, **kwargs):
        json_content = {
            "status_code": status_code,
            "data": data
        }
        if kwargs.get('message'):
            json_content['message'] = kwargs['message']
        return json_content
    
    def prepare_response(self, request, **kwargs):
        return {}
    
    def is_permitted(self, request, **kwargs):
        return False
    
    def render(self, request, **kwargs):
        if self.is_permitted(request, **kwargs):
            json_response = self.prepare_response(request, **kwargs)
            if type(json_response) is not dict:
                return self.prepare_json_content(status_code=500, data={ 'message': 'Response should return dict only' })
            return self.prepare_json_content(status_code=200, data=json_response)
        else:
            return self.prepare_json_content(status_code=403, data={ 'message': 'Permissioned denied' }) # Permission denied
