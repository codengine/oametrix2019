from openaccess.api.views.dashboard.components.dashboard_component import DashboardComponent


class OAIncomeComponent(DashboardComponent):
    
    def is_permitted(self, request, **kwargs):
        return True
    
    def prepare_response(self, request, **kwargs):
        # DepositCreditTransaction.objects.filter(transaction_type='DEBIT', 
        #   article__publisher_id=org_id, currency__short_name='usd').values('currency__short_name').annotate(total=Sum('amount'))
        oa_income_usd = 0.0
        oa_income_eur = 0.0
        oa_income_gbp = 0.0
        return {
            "usd": oa_income_usd,
            "eur": oa_income_eur,
            "gbp": oa_income_gbp
        }
