from openaccess.api.views.dashboard.components.dashboard_component import DashboardComponent


class ArticleSummaryComponent(DashboardComponent):
    
    def is_permitted(self, request, **kwargs):
        return True
    
    def prepare_response(self, request, **kwargs):
        accepted = 0
        approved = 0
        rejected = 0
        return {
            "accepted": accepted,
            "approved": approved,
            "rejected": rejected
        }
