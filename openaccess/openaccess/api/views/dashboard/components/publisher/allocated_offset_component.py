from openaccess.api.views.dashboard.components.dashboard_component import DashboardComponent


class AllocatedOffsetComponent(DashboardComponent):
    
    def is_permitted(self, request, **kwargs):
        return True
    
    def prepare_response(self, request, **kwargs):
        ao_usd = 0.0
        ao_eur = 0.0
        ao_gbp = 0.0
        return {
            "usd": ao_usd,
            "eur": ao_eur,
            "gbp": ao_gbp
        }
