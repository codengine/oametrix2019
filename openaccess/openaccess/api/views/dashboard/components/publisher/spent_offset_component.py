from openaccess.api.views.dashboard.components.dashboard_component import DashboardComponent


class SpentOffsetComponent(DashboardComponent):
    
    def is_permitted(self, request, **kwargs):
        return True
    
    def prepare_response(self, request, **kwargs):
        so_usd = 0.0
        so_eur = 0.0
        so_gbp = 0.0
        return {
            "usd": so_usd,
            "eur": so_eur,
            "gbp": so_gbp
        }
