from openaccess.api.views.dashboard.components.dashboard_component import DashboardComponent


class OADealListComponent(DashboardComponent):
    
    def is_permitted(self, request, **kwargs):
        return True
    
    def prepare_response(self, request, **kwargs):
        items = []
        return {
            "items": items
        }
