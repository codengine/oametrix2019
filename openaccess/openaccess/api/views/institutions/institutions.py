from openaccess.models.article.article_full import ArticleFull
from openaccess.models.article.author import ArticleAuthor
import logging
from rest_framework.permissions import AllowAny, IsAuthenticated
from core.models.organisation.organisation import Organisation
from rest_framework import viewsets
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from django.db.models import Avg, Count, Min, Sum
from django.http import Http404
from core.api.views.base_api_view import BaseAPIView
from rest_framework import status
from openaccess.models.contract.oa_deal import OADeal
import random
from openaccess.models.contract.deposit_request import DepositRequest
from openaccess.models.contract.deal_type import DealType

__author__ = 'expo_ashiq'

logger = logging.getLogger('openaccess')


class InstitutionsView(viewsets.ModelViewSet):
    serializer_class = Organisation.get_serializer()

    def get_queryset(self):
        request = self.request
        all_organisations = []

        if request.user.organisation:
            if request.user.is_publisher_user():
                author_ids = ArticleFull.objects.filter(publisher_id=request.user.organisation.pk).values_list('author',
                                                                                                               flat=True).distinct()

                affiliation_list = ArticleAuthor.objects.filter(id__in=author_ids).values_list('affiliation', flat=True)
                unique_affiliation_list = set(affiliation_list)
                all_org_names = set(Organisation.objects.values_list('name', flat=True))
                org_ids = []
                for affiliation in unique_affiliation_list:
                    for org_name in all_org_names:
                        if org_name.lower() == affiliation.lower():
                            org_id = Organisation.objects.get(name__iexact=affiliation).id
                            org_ids.append(org_id)
                all_organisations = Organisation.objects.filter(id__in=org_ids)
            else:
                publisher_ids = ArticleFull.objects.filter(
                    author__affiliation__iexact=request.user.organisation.name).all().values_list('publisher',
                                                                                                  flat=True)
                all_organisations = Organisation.objects.filter(id__in=publisher_ids)
        return all_organisations


class OrganisationFinanceView(BaseAPIView):
    http_method_names = ['get']
    permission_classes = (IsAuthenticated,)

    def get_object(self, pk):
        try:
            return Organisation.objects.get(pk=pk)
        except Organisation.DoesNotExist:
            raise Http404

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    @csrf_exempt
    def get(self, request, id):
        org = self.get_object(id)
        response = {
            "data": org.finance_report_json()
        }
        return Response(response, status=status.HTTP_200_OK)


class ReadAndPublishDashboardInfoView(BaseAPIView):
    http_method_names = ['get']
    permission_classes = (IsAuthenticated,)

    def get_deal_type(self, name):
        try:
            return DealType.objects.get(name=name)
        except DealType.DoesNotExist:
            raise Http404

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    @csrf_exempt
    def get(self, request):
        user = request.user

        deal_type = self.get_deal_type(name='read & publish')
        deposit_requests = DepositRequest.objects.filter(oa_deal__deal_type=deal_type,
                                                         oa_deal__counter_organisation=user.organisation).all()

        total_read_fee_result = deposit_requests.aggregate(total_read_fee=Sum('read_fee'))
        total_publish_fee_result = deposit_requests.aggregate(total_publish_fee=Sum('publish_fee'))

        temp = {}
        temp['total_read_fee'] = total_read_fee_result['total_read_fee']
        temp['total_publish_fee'] = total_publish_fee_result['total_publish_fee']
        temp['total'] = total_publish_fee_result['total_publish_fee'] + total_read_fee_result['total_read_fee']
        temp['total_articles'] = random.randint(100, 1000)
        temp['avg_apc_cost'] = (temp['total_publish_fee'] / temp['total_articles'])

        response = {
            "results": temp
        }
        return Response(response, status=status.HTTP_200_OK)



class DashboardTotalInfoView(BaseAPIView):
    http_method_names = ['get']
    permission_classes = (IsAuthenticated,)

    def get_deal_type(self, name):
        try:
            return DealType.objects.get(name=name)
        except DealType.DoesNotExist:
            raise Http404

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    @csrf_exempt
    def get(self, request):
        user = request.user

        # deal_type = self.get_deal_type(name='pre-payment')
        # deposit_requests = DepositRequest.objects.filter(oa_deal__deal_type__name=deal_type.name,
        #                                                  oa_deal__counter_organisation=user.organisation).all()
        #
        # results = deposit_requests.aggregate(total_amount=Sum('amount'))
        temp = {}
        temp['total_deposit'] = random.randint(100, 1000)
        temp['oa_spend'] = random.randint(100, 1000)
        temp['available_fund'] = random.randint(100, 1000)

        response = {
            "results": temp
        }
        return Response(response, status=status.HTTP_200_OK)
