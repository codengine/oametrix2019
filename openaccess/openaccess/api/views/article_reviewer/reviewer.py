from rest_framework.permissions import AllowAny, IsAuthenticated
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from django.db import transaction
from core.models.auth.user import User, AccountInfo
from celery.schedules import crontab
from celery.task import periodic_task
from core.api.views.base_api_view import BaseAPIView
from rest_framework import status
from openaccess.models.article.article_basic import ArticleBasic
from openaccess.models.article.article_full import ArticleFull
from openaccess.models.article.article_manual import ArticleManual
from openaccess.models.publication.publication import Publication
from core.models.organisation.organisation import Organisation
import logging

__author__ = 'expo_ashiq'

logger = logging.getLogger('openaccess')


@periodic_task(
    run_every=(crontab(hour='*/1')),
    name="article_reviewer",
    ignore_result=True
)
def task_article_reviewer():
    reviewer()
    manual_reviewer()
    logger.info("Article reviewer activated")


def reviewer():
    logger.info("Article reviewer initializing")
    all_publications = Publication.objects.all()
    all_org_names = set(Organisation.objects.values_list('name', flat=True))

    moving_article_full(all_publications, all_org_names)
    moving_article_manual(all_publications, all_org_names)


class ArtilcleReviewerView(BaseAPIView):
    http_method_names = ['get']
    permission_classes = (AllowAny,)

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    @csrf_exempt
    @transaction.atomic
    def get(self, request):
        all_publications = Publication.objects.all()
        all_org_names = set(Organisation.objects.values_list('name', flat=True))

        moving_article_full(all_publications, all_org_names)
        moving_article_manual(all_publications, all_org_names)

        return Response(status=status.HTTP_200_OK)


def moving_article_full(all_publications, all_org_names):
    logger.info("Initializing articlefull query")

    all_basics = ArticleBasic.objects.all()

    for basic in all_basics:
        for single_publication in all_publications:
            if basic.pissn == single_publication.pissn or basic.eissn == single_publication.eissn:
                publisher_obj = None

                for name in all_org_names:
                    if basic.publisher_name:
                        if basic.publisher_name.lower() == name.lower():
                            publisher_obj = Organisation.objects.filter(name__iexact=basic.publisher_name).first()

                    if publisher_obj is not None:
                        publication = single_publication

                        if basic.id is not None and checking_author_affiliation(all_org_names, basic.author):
                            res = add_to_full(basic, publisher_obj, publication)

                            if res:
                                basic.delete()


def checking_author_affiliation(all_org_names, author):
    if author is None:
        return False

    for org_name in all_org_names:
        if author.affiliation.lower() == org_name.lower():
            return True
    return False


def add_to_full(basic, publisher_obj, publication):
    try:

        article_full = ArticleFull()
        article_full.title = basic.title
        article_full.doi = basic.doi
        article_full.article_id = basic.article_id
        article_full.content_type = basic.content_type
        article_full.author = basic.author

        if publication:
            article_full.publication_id = publication.id
        if publisher_obj:
            article_full.publisher_id = publisher_obj.id

        funder = Organisation.objects.filter(name=basic.funder_name).first()

        if funder:
            article_full.funder = funder
        else:
            article_full.funder_name = basic.funder_name

        article_full.grant_number = basic.grant_number
        article_full.submission_date = basic.submission_date
        article_full.acceptance_date = basic.acceptance_date
        article_full.license = None  # TODO
        article_full.impact_factor = publication.impact_factor
        article_full.hindex = publication.hindex
        article_full.indexed = publication.indexed
        article_full.sherpa_romeo_info = publication.sherpa_romeo_info

        price_info = publication.prices.all().filter(content_type=basic.content_type).first()

        if price_info is not None:
            article_full.pub_fee_gbp = price_info.pub_fee_gbp
            article_full.pub_fee_eur = price_info.pub_fee_eur
            article_full.pub_fee_usd = price_info.pub_fee_usd

            article_full.submission_fee_gbp = price_info.submission_fee_gbp
            article_full.submission_fee_eur = price_info.submission_fee_eur
            article_full.submission_fee_usd = price_info.submission_fee_usd

            article_full.colour_charge_gbp = price_info.colour_charge_gbp
            article_full.colour_charge_eur = price_info.colour_charge_eur
            article_full.colour_charge_usd = price_info.colour_charge_usd

            # TODO
            article_full.page_charge_gbp = price_info.page_charge_gbp
            article_full.page_charge_eur = price_info.page_charge_eur
            article_full.page_charge_usd = price_info.page_charge_usd

        article_full.save()

        for co_author in basic.co_authors.all():
            article_full.co_authors.add(co_author)

        logger.info("Articlefull query match found")

        return True
    except:
        logger.warning("Articlefull query interepted")
        return False


def moving_article_manual(all_publications, all_org_names):
    logger.info("Initializing article_reviewer manual query")

    all_basics = ArticleBasic.objects.all()

    for basic in all_basics:
        article_manual = ArticleManual()
        article_manual.title = basic.title
        article_manual.content_type_id = basic.content_type.id
        if basic.author:
            article_manual.author_id = basic.author.id
        article_manual.journal_name = basic.journal_name
        article_manual.journal_acronym = basic.journal_acronym
        article_manual.publisher_name = basic.publisher_name
        article_manual.funder_name = basic.funder_name
        article_manual.status = basic.status
        article_manual.pissn = basic.pissn
        article_manual.eissn = basic.eissn
        article_manual.fund_acknowledgement = basic.fund_acknowledgement
        article_manual.sub_system_acronym = basic.sub_system_acronym
        article_manual.grant_number = basic.grant_number
        article_manual.submission_date = basic.submission_date
        article_manual.acceptance_date = basic.acceptance_date

        # Match refferenced fields
        for single_publication in all_publications:
            if basic.pissn == single_publication.pissn or basic.eissn == single_publication.eissn:
                article_manual.publication_id = single_publication.id

        for name in all_org_names:
            if basic.publisher_name:
                if basic.publisher_name.lower() == name.lower():
                    publisher_obj = Organisation.objects.filter(name__iexact=name).first()
                    if publisher_obj is not None:
                        article_manual.publisher_id = publisher_obj.id

            if basic.funder_name:
                if basic.funder_name.lower() == name.lower():
                    funder = Organisation.objects.filter(name__iexact=basic.funder_name).first()
                    if funder is not None:
                        article_manual.funder_id = funder.id

        article_manual.save()
        logger.info("Article manual saved successfully")

        for co_author in basic.co_authors.all():
            article_manual.co_authors.add(co_author)

        # removing basic
        basic.delete()


def manual_reviewer():
    logger.info("Manual reviewer initializing")
    all_publications = Publication.objects.all()
    all_org_names = set(Organisation.objects.values_list('name', flat=True))

    checking_article_manual(all_publications, all_org_names)


def checking_article_manual(all_publications, all_org_names):
    logger.info("Initializing checking_article_manual ")

    all_manuals = ArticleManual.objects.all()

    for manual in all_manuals:
        for single_publication in all_publications:
            if manual.pissn == single_publication.pissn or manual.eissn == single_publication.eissn:
                publisher_obj = None

                for name in all_org_names:
                    if manual.publisher_name:
                        if manual.publisher_name.lower() == name.lower():
                            publisher_obj = Organisation.objects.filter(name__iexact=manual.publisher_name).first()

                    if publisher_obj is not None:
                        publication = single_publication

                        if manual.id is not None and checking_author_affiliation(all_org_names, manual.author):
                            res = add_to_full(manual, publisher_obj, publication)

                            if res:
                                manual.delete()
