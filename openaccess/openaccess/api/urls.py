from django.conf.urls import url
from django.urls import path
from django.conf.urls import include
from openaccess.api.views.article_reviewer.reviewer import ArtilcleReviewerView
from openaccess.api.views.dashboard.dashboard_view import DashboardAPIView
from openaccess.api.views.institutions.institutions import InstitutionsView, OrganisationFinanceView, ReadAndPublishDashboardInfoView, DashboardTotalInfoView



urlpatterns = [
    url(r'', include("core.api.urls")),
    url(r'^dashboard/$', DashboardAPIView.as_view(), name="oa_dashboard_api"),
    path('article-reviewer/', ArtilcleReviewerView.as_view(), name='article_reviewer'),
    path('institutions/', InstitutionsView.as_view({'get': 'list'}), name='institutions'),
    path('organization-finance-report/<int:id>', OrganisationFinanceView.as_view(), name='org_report'),
    path('read-and-publish-dashboard-info/', ReadAndPublishDashboardInfoView.as_view(), name='read_and_publish_dashboard_info'),
    path('dashboard-total-info/', DashboardTotalInfoView.as_view(), name='university_dashboard_total_info'),
]