from django.contrib.auth.backends import ModelBackend
from core.models.auth.user import User
from django.core.exceptions import ObjectDoesNotExist


class OpenAccessAuthBackend(ModelBackend):
    def authenticate(self, request, username=None, password=None):
        try:
            user = User.objects.get(email=username)
            if user.check_password(password):
                return user
        except ObjectDoesNotExist:
           return None

