class WorkflowModelMixin(object):
    
    @classmethod
    def workflow_activated(cls):
        return True

    @classmethod
    def handle_workflow_activity(cls, workflow_instance, user, state_from, state_to, action, *args, **kwargs):
        raise NotImplemented("handle workflow activity method not implemented")
