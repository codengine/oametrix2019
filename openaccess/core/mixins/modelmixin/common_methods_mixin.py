from core.api.mixins.modelmixins.routable_model_mixin import RoutableModelMixin


class CommonMethodsMixin(object):
    @classmethod
    def filter_model(cls, queryset):
        return queryset

    @classmethod
    def filter_queryset(cls, request, queryset, search_request_params, *args, **kwargs):
        return queryset

    @classmethod
    def require_auth(cls):
        return True

    @classmethod
    def map_view_action_to_label(cls):
        return {

        }

    @classmethod
    def map_object_action_to_label(cls):
        return {

        }

    @classmethod
    def permission_model(cls):
        return cls


