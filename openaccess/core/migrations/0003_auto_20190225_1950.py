# Generated by Django 2.1.2 on 2019-02-25 13:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20190215_0124'),
    ]

    operations = [
        migrations.AddField(
            model_name='emailbase',
            name='email_body_text',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='emailbase',
            name='subject',
            field=models.CharField(blank=True, max_length=1000, null=True),
        ),
    ]
