# Generated by Django 2.1.2 on 2019-02-28 12:10

import core.api.mixins.modelmixins.custom_object_action_model_mixin
import core.api.mixins.modelmixins.filter_model_mixin
import core.api.mixins.modelmixins.routable_model_mixin
import core.api.mixins.modelmixins.searchable_model_mixin
import core.api.mixins.modelmixins.serializer_model_mixin
import core.api.mixins.viewmixins.viewsets_post_callback_methods_mixin
import core.mixins.modelmixin.common_methods_mixin
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20190225_2224'),
    ]

    operations = [
        migrations.CreateModel(
            name='WalletBalance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField()),
                ('date_updated', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(default=False)),
                ('amount', models.DecimalField(decimal_places=3, default=0.0, max_digits=20)),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('created_by_group', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='core.Group')),
                ('created_by_role', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='core.Role')),
                ('currency', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='core.Currency')),
                ('updated_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('updated_by_group', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='core.Group')),
                ('updated_by_role', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='core.Role')),
            ],
            options={
                'ordering': ['id'],
                'abstract': False,
            },
            bases=(models.Model, core.mixins.modelmixin.common_methods_mixin.CommonMethodsMixin, core.api.mixins.modelmixins.custom_object_action_model_mixin.CustomObjectActionModelMixin, core.api.mixins.modelmixins.routable_model_mixin.RoutableModelMixin, core.api.mixins.viewmixins.viewsets_post_callback_methods_mixin.ViewSetsPostCallBackMethodsMixin, core.api.mixins.modelmixins.serializer_model_mixin.SerializerModelMixin, core.api.mixins.modelmixins.searchable_model_mixin.SearchableModelMixin, core.api.mixins.modelmixins.filter_model_mixin.FilterDataFilterMixin),
        ),
    ]
