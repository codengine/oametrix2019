from django.db import models
from core.models.base_entity import BaseEntity
from core.models.contacts.address import Address
from core.models.auth.group import Group


class DomainEntity(BaseEntity):
    name = models.CharField(max_length=1000)
    domain = models.ForeignKey(Group, null=True, blank=True, on_delete=models.CASCADE)
    address = models.ForeignKey(Address, null=True, blank=True, on_delete=models.CASCADE)
    note = models.TextField(null=True, blank=True)
    parent = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name



