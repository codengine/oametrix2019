from django.db import models
from core.models.base_entity import BaseEntity
from core.models.domain.division import Division


# This is supposed to be the department
class Segment(BaseEntity):
    name = models.CharField(max_length=1000)
    division = models.ForeignKey(Division, null=True, blank=True, on_delete=models.CASCADE)
    description = models.TextField(null=True, blank=True)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(Segment, cls).get_serializer()

        class SegmentSerializer(BaseSerializer):
            division = Division.get_serializer()(required = False)
            class Meta(BaseSerializer.Meta):
                fields = ('id', 'name', 'description', 'division', 'is_active')
                read_only_fields = ('id',)
        return SegmentSerializer