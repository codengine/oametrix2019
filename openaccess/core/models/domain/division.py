from django.db import models
from core.models.base_entity import BaseEntity


# This is supposed to be the faculty
class Division(BaseEntity):
    name = models.CharField(max_length=1000)
    description = models.TextField(null=True, blank=True)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(Division, cls).get_serializer()

        class DivisionSerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = ('id', 'name', 'description','is_active')
                read_only_fields = ('id', )

        return DivisionSerializer