from django.db import models
from core.models.base_entity import BaseEntity


class OrganisationMeta(BaseEntity):
    name_1_other_lang = models.CharField(max_length=1000, null=True, blank=True)
    name_2_other_lang = models.CharField(max_length=1000, null=True, blank=True)
    grid_number = models.URLField(max_length=255, null=True, blank=True)
    ring_gold_number = models.CharField(max_length=255, null=True, blank=True)
    isni_number = models.CharField(max_length=255, null=True, blank=True)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(OrganisationMeta, cls).get_serializer()

        class OrganisationMetaSerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = (
                'id', 'name_1_other_lang', 'name_2_other_lang', 'grid_number', 'ring_gold_number', 'isni_number',
                'is_active')
                read_only_fields = ('id',)

        return OrganisationMetaSerializer

    @classmethod
    def generate_auto_urls(cls):
        return None
