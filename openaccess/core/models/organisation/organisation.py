from django.db import models
from django.db.models.query_utils import Q
from core.models.organisation.organisation_abstract import OrganisationAbstract
from core.models.organisation.organisation_meta import OrganisationMeta
from core.models.contacts.address import Address
from rest_framework import serializers
from rest_framework import status
from django.apps import apps
import random


class Organisation(OrganisationAbstract):

    def finance_report_json(self):
        return {
            "id": self.id,
            "name": self.name,
            "oa_income": random.randint(5000, 100000),
            "deposit": random.randint(50000, 100000),
            "available_fund": random.randint(5000, 1000000)
        }

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(Organisation, cls).get_serializer()

        class OrganisationSerializer(BaseSerializer):
            meta = OrganisationMeta.get_serializer()(required=False)
            address = Address.get_serializer()(required=False)
            dommain_name = serializers.SerializerMethodField(read_only=True)
            parent_name = serializers.SerializerMethodField(read_only=True)
            total_accepted = serializers.SerializerMethodField(read_only=True)
            total_acceptance = serializers.SerializerMethodField(read_only=True)
            declined = serializers.SerializerMethodField(read_only=True)


            class Meta(BaseSerializer.Meta):
                fields = ('id', 'name', 'domain',
                          'address', 'note', 'parent', 'parent_name',
                          'website', 'vat_number', 'email_domain', 'meta', 'dommain_name', 'total_accepted', 'declined',
                          'total_acceptance', 'is_active')
                read_only_fields = ('id',)


            def get_dommain_name(self, obj):
                if obj.domain:
                    return obj.domain.name
                else:
                    return None

            def get_parent_name(self, obj):
                if obj.parent:
                    return obj.parent.name
                else:
                    return None

            def get_total_accepted(self, obj):
                return random.randint(1, 10)

            def get_total_acceptance(self, obj):
                return random.randint(1, 10)

            def get_declined(self, obj):
                return random.randint(1, 10)

            def to_internal_value(self, data):
                data['is_active'] = True
                return super(OrganisationSerializer, self).to_internal_value(data)

            def validate(self, data):
                email_domain = data.get('email_domain', None)

                if email_domain is not None:
                    if email_domain.find('.') == -1:
                        raise serializers.ValidationError({"email_domain": "This field is not valid."},
                                                          status.HTTP_400_BAD_REQUEST)

                data = super(OrganisationSerializer, self).validate(data)
                return data
        return OrganisationSerializer

    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        org_ids = [user.organisation.pk]
        child_orgs = Organisation.objects.all().filter(parent_id=user.organisation.pk).values_list('pk', flat=True)
        org_ids += child_orgs

        filters = [
            Q(**{'id__in': org_ids})
        ]
        return filters  # [<Q: (AND: ('id__in', [1, 2, 4]))>]
