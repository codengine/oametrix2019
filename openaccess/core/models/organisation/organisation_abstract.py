from django.db import models
from core.models.domain.domain_entity import DomainEntity
from core.models.organisation.organisation_meta import OrganisationMeta


class OrganisationAbstract(DomainEntity):
    website = models.URLField(max_length=255, null=True, blank=True)
    vat_number = models.CharField(max_length=255, null=True, blank=True)
    email_domain = models.CharField(max_length=255, unique=True)
    meta = models.ForeignKey(OrganisationMeta, null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        abstract = True
