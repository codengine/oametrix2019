from django.db import models
from django.conf import settings


class BaseEntityRelAbstract(models.Model):
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="+", null=True, on_delete=models.CASCADE)
    updated_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="+", null=True, on_delete=models.CASCADE)
    created_by_group = models.ForeignKey('core.Group', related_name="+", null=True, on_delete=models.CASCADE)
    updated_by_group = models.ForeignKey('core.Group', related_name="+", null=True, on_delete=models.CASCADE)
    created_by_role = models.ForeignKey('core.Role', related_name="+", null=True, on_delete=models.CASCADE)
    updated_by_role = models.ForeignKey('core.Role', related_name="+", null=True, on_delete=models.CASCADE)

    class Meta:
        abstract = True
