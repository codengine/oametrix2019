from django.db import models, transaction
from core.constants.transaction_type import TransactionType
from core.models.base_entity import BaseEntity
from core.models.finance.currency import Currency


class WalletBalance(BaseEntity):
    currency = models.ForeignKey(Currency, on_delete=models.DO_NOTHING)
    amount = models.DecimalField(decimal_places=3, max_digits=20, default=0.0)

    def get_balance(self):
        return {
            "currency": self.currency.short_name,
            "currency_id": self.currency.pk,
            "amount": self.amount
        }

    def update_balance(self, amount, transaction_type=TransactionType.CREDIT.value):
        with transaction.atomic():
            cur_bal = self.amount

            if transaction_type == TransactionType.CREDIT.value:
                new_bal = cur_bal + amount
            else:
                new_bal = cur_bal - amount

                if new_bal < 0:
                    raise Exception("Insufficient funds to debit")

            self.amount = new_bal
            self.save()
        return self


class WalletAbstract(BaseEntity):
    balance = models.ManyToManyField(WalletBalance) # Balance could be in multiple currencies. This amount is owned by Wallet owner

    class Meta:
        abstract = True


