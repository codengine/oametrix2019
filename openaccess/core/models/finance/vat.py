from django.db import models
from core.models.base_entity import BaseEntity
from core.models.contacts.country import Country
from core.models.finance.currency import Currency


class Vat(BaseEntity):
    country = models.ForeignKey(Country, on_delete=models.DO_NOTHING)
    currency = models.ForeignKey(Currency, on_delete=models.DO_NOTHING)
    vat_percentage = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    vat_amount = models.DecimalField(decimal_places=3, max_digits=20, null=True, blank=True)
    note = models.TextField(null=True, blank=True)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(Vat, cls).get_serializer()

        class VatSerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = ('id', 'country', 'currency', 'vat_percentage', 'vat_amount', 'note', 'is_active')
                read_only_fields = ('id',)

        return VatSerializer
