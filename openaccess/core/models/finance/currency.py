from django.db import models
from core.models.base_entity import BaseEntity


class Currency(BaseEntity):
    name = models.CharField(max_length=100)
    short_name = models.CharField(max_length=20)
    symbol = models.CharField(max_length=10, null=True, blank=True)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(Currency, cls).get_serializer()

        class CurrencySerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = ('id', 'name','short_name', 'symbol', 'is_active')
                read_only_fields = ('id', )

        return CurrencySerializer