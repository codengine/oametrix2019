from django.db import models
from core.constants.email_send_status import EmailSendStatus
from core.models.base_entity import BaseEntity
from core.models.mail.email_base import EmailBase


class EmailLog(BaseEntity):
    email_template = models.ForeignKey(EmailBase, on_delete=models.DO_NOTHING)
    sender_name = models.CharField(max_length=1000, null=True, blank=True)
    sender = models.EmailField(null=True, blank=True)
    recipients = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=200, default=EmailSendStatus.PENDING.value)
    stacktrace = models.TextField(null=True, blank=True)

