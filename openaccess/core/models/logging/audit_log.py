from django.db import models
from core.models.base_entity import BaseEntity


class AuditLog(BaseEntity):
    url = models.CharField(max_length=2000, null=True, blank=True)
    request_method = models.CharField(max_length=10, null=True, blank=True)
    request_data = models.TextField(null=True, blank=True)
    message = models.TextField()
    stacktrace = models.TextField(null=True, blank=True)
