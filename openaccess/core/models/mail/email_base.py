from django.db import models
from core.models.base_entity import BaseEntity


class EmailBase(BaseEntity):
    type = models.CharField(max_length=200, null=True, blank=True)
    subject = models.CharField(max_length=1000, null=True, blank=True)
    email_body_text = models.TextField(null=True, blank=True)
    email_body = models.TextField(null=True, blank=True)