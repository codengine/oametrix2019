from core.models.contacts.base_contact import BaseContact


class State(BaseContact):


    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(State, cls).get_serializer()

        class StateSerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = ('id', 'name', 'short_name', 'parent', 'is_active')
                read_only_fields = ('id',)

        return StateSerializer

