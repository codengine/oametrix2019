from django.db import models
from core.models.base_entity import BaseEntity
from core.models.contacts.city import City
from core.models.contacts.country import Country
from core.models.contacts.state import State
from rest_framework import serializers


class Address(BaseEntity):
    title = models.CharField(max_length=1000, null=True, blank=True) # Home Address, Office Address etc
    address1 = models.TextField(null=True, blank=True)
    address2 = models.TextField(null=True, blank=True)
    city = models.ForeignKey(City, null=True, blank=True, on_delete=models.CASCADE)
    state = models.ForeignKey(State, null=True, blank=True, on_delete=models.CASCADE)
    country = models.ForeignKey(Country, null=True, blank=True, on_delete=models.CASCADE)
    postal_code = models.CharField(max_length=100, null=True, blank=True)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(Address, cls).get_serializer()

        class AddressSerializer(BaseSerializer):

            country_name = serializers.SerializerMethodField(read_only=True)
            class Meta(BaseSerializer.Meta):
                fields = ('id','title', 'address1', 'address2', 'city', 'state',
                          'country', 'postal_code', 'country_name', 'is_active')

            def get_country_name(self, obj):
                if obj.country is not None:
                    return obj.country.name
                else :
                    return None

        return AddressSerializer
