import logging
from django.db import models
from core.models.contacts.base_contact import BaseContact
from core.openaccessio.file_manager import FileManager
from django.db.models.query_utils import Q


logger = logging.getLogger('core')


class Country(BaseContact):
    phone_code = models.CharField(max_length=10, null=True, blank=True)

    @classmethod
    def load_from_json(cls, json_file_path):
        country_json_data = FileManager.read_json(json_file_path)
        countries = country_json_data.get("countries", [])
        total_countries = len(countries)
        logger.info('Total %s countries found' % total_countries)
        for i, country in enumerate(countries):
            logger.info("Processing %s/%s and country name: %s" % (i + 1, total_countries, country['name']))
            short_name = country['short_name']
            name = country['name']
            phone_code = country['phone_code']

            country_instances = cls.objects.filter(name=name, short_name=short_name)
            if country_instances.exists():
                country_instance = country_instances.first()
            else:
                country_instance = cls()

            country_instance.name = name
            country_instance.short_name = short_name
            country_instance.phone_code = phone_code
            country_instance.is_active = True
            country_instance.save()

        logger.info("Countries uploaded successfully")


    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(Country, cls).get_serializer()

        class CountrySerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = ('id', 'name', 'short_name', 'parent',  'phone_code', 'is_active')
                read_only_fields = ('id',)

        return CountrySerializer

    @classmethod
    def apply_order_by(cls, request, queryset, *args, **kwargs):
        return queryset.order_by('id')








