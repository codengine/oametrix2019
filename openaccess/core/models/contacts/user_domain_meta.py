from django.db import models
from core.models.base_entity import BaseEntity
from core.models.domain.segment import Segment
from django.db import transaction
from rest_framework import serializers

class UserDomainMeta(BaseEntity):
    segment = models.ForeignKey(Segment, null=True, blank=True, on_delete=models.DO_NOTHING)
    title = models.CharField(max_length=1000, null=True, blank=True)
    position = models.CharField(max_length=1000, null=True, blank=True)
    home_tel = models.CharField(max_length=100, null=True, blank=True)
    office_tel = models.CharField(max_length=100, null=True, blank=True)
    alt_tel = models.CharField(max_length=100, null=True, blank=True)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(UserDomainMeta, cls).get_serializer()

        class UserDomainMetaSerializer(BaseSerializer):
            department = serializers.CharField(allow_null=True, allow_blank=True)
            segment = serializers.SerializerMethodField()

            class Meta(BaseSerializer.Meta):
                fields = ('id','title', 'position', 'home_tel', 'office_tel',
                          'alt_tel','department','segment','is_active')
                read_only_fields = ('id',)
                write_only_fields = ('department',)

            @transaction.atomic
            def create(self, validated_data):
                department_name = validated_data.pop('department')

                instance = super(UserDomainMetaSerializer, self).create(validated_data)

                if department_name:
                    segment_instances = Segment.objects.filter(name = department_name)

                    if segment_instances.exists():
                        segment_instance = segment_instances.first()
                    else:
                        segment_instance = Segment()

                    segment_instance.name = department_name
                    segment_instance.save()

                    instance.segment_id = segment_instance.id
                    instance.save()
                return instance

            def get_segment(self, obj):
                return obj.segment_id

        return UserDomainMetaSerializer
