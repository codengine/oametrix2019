from django.db import models
from core.models.base_entity import BaseEntity


class BaseContact(BaseEntity):
    name = models.CharField(max_length=1000)
    short_name = models.CharField(max_length=20)
    parent = models.ForeignKey("self", null=True, blank=True, related_name="self", on_delete=models.CASCADE)

    class Meta:
        abstract = True
