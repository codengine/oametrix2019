from core.models.contacts.base_contact import BaseContact


class City(BaseContact):

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(City, cls).get_serializer()

        class CitySerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = ('id', 'name', 'short_name', 'parent', 'is_active')
                read_only_fields = ('id',)

        return CitySerializer
