from django.db import models
from core.constants.approval_status import ApprovalStatusEnum
from core.models.auth.group import Group
from core.models.auth.role import Role
from core.models.auth.user import User
from core.models.base_abstract_model import BaseAbstractModel
from core.models.contracts.approval_activity import ApprovalActivity
from core.models.organisation.organisation import Organisation
from engine.library.clock import Clock
from rest_framework.response import Response
from rest_framework import status


class ApprovableAbstractModel(BaseAbstractModel):
    organisation = models.ForeignKey(Organisation, related_name='+', on_delete=models.DO_NOTHING)
    counter_organisation = models.ForeignKey(Organisation, related_name='+', on_delete=models.DO_NOTHING)
    approval_status = models.CharField(max_length=200, default=ApprovalStatusEnum.PENDING.value)
    approve_date = models.DateTimeField(null=True, blank=True)
    approved_by = models.ForeignKey(User, null=True, blank=True, related_name='+', on_delete=models.DO_NOTHING)
    approved_by_group = models.ForeignKey(Group, null=True, blank=True, related_name='+', on_delete=models.DO_NOTHING)
    approved_by_role = models.ForeignKey(Role, null=True, blank=True, related_name='+', on_delete=models.DO_NOTHING)

    activities = models.ManyToManyField(ApprovalActivity)

    def is_approved(self):
        return self.approval_status == "approved"

    @classmethod
    def render_workflow_actions(cls):
        return []

    @classmethod
    def approval_enabled(cls):
        return True

    def approve(self, request, **kwargs):

        self.approval_status = ApprovalStatusEnum.APPROVED.value
        self.approve_date = Clock.utc_now()
        self.approved_by_id = request.user.id
        self.approved_by_group_id = request.user.group_id
        self.approved_by_role_id = request.user.role_id
        self.is_active = True
        self.save()
        return self

    def reject(self, request, **kwargs):
        self.approval_status = ApprovalStatusEnum.REJECTED.value
        self.approve_date = Clock.utc_now()
        self.approved_by_id = request.user.id
        self.approved_by_group_id = request.user.group_id
        self.approved_by_role_id = request.user.role_id
        self.is_active = True
        self.save()
        return self

    class Meta:
        abstract = True




