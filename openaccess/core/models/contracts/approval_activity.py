from django.db import models
from core.models.auth.group import Group
from core.models.auth.role import Role
from core.models.auth.user import User
from core.models.base_entity import BaseEntity


class ApprovalActivity(BaseEntity):
    approval_status = models.CharField(max_length=200)
    approve_date = models.DateTimeField()
    approved_by = models.ForeignKey(User, related_name='+', on_delete=models.DO_NOTHING)
    approved_by_group = models.ForeignKey(Group, related_name='+', on_delete=models.DO_NOTHING)
    approved_by_role = models.ForeignKey(Role, related_name='+', on_delete=models.DO_NOTHING)

