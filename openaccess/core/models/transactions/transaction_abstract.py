from django.db import models
from core.constants.transaction_type import TransactionType
from core.models.base_entity import BaseEntity
from core.models.finance.currency import Currency


class TransactionAbstract(BaseEntity):
    transaction_type = models.CharField(max_length=20) #TransactionType.DEBIT.value or TransactionType.CREDIT.value
    currency = models.ForeignKey(Currency, on_delete=models.DO_NOTHING)
    amount = models.DecimalField(decimal_places=3, max_digits=20, default=0.0)
    note = models.TextField(null=True, blank=True)
    reference_model = models.CharField(max_length=200, null=True, blank=True) # core.ModelName
    reference_id = models.BigIntegerField(null=True, blank=True) # 1

    class Meta:
        abstract = True




