from django.db import models
from rest_framework import serializers
from core.models.base_entity import BaseEntity


class CustomField(BaseEntity):
    field_name = models.CharField(max_length=200)
    field_description = models.TextField(null=True, blank=True)
    field_type = models.CharField(max_length=10,
                                  choices=(
                                    ('t', 'Text'),
                                    ('a', 'Large Text Field'),
                                    ('i', 'Integer'),
                                    ('f', 'Floating point decimal'),
                                    ('b', 'Boolean (Yes/No)'),
                                    ('m', 'Dropdown Choices'),
                                    ('d', 'Date'),
                                    ('h', 'Date Time'),
                                    ),
                                  default='text')
    default_value = models.CharField(max_length=5000, blank=True,
                                     help_text="You may leave blank. Use True/False for Boolean")
    is_required = models.BooleanField(default=False)
    field_choices = models.CharField(max_length=2000, blank=True, help_text="Custom choices")
    placeholder = models.CharField(max_length=2000, blank=True, help_text="Placeholder")

    def get_serializer_field(self):
        universal_kwargs = {
            'initial': self.default_value if self.default_value else '',
            'required': self.is_required if self.is_required else False,
        }
        if self.field_type == "b":
            return serializers.BooleanField(**universal_kwargs)
        elif self.field_type == "i":
            return serializers.IntegerField(**universal_kwargs)
        elif self.field_type == "f":
            return serializers.FloatField(**universal_kwargs)
        elif self.field_type == "t":
            return serializers.CharField(max_length=255, **universal_kwargs)
        elif self.field_type == "a":
            return serializers.CharField(max_length=5000, **universal_kwargs)
        elif self.field_type == "enum":
            choices = self.field_choices.split(',')
            if self.is_required is True:
                select_choices = ()
            else:
                select_choices = (('', '---------'),)
            for choice in choices:
                select_choices = select_choices + ((choice, choice),)
            return serializers.ChoiceField(
                choices=select_choices, **universal_kwargs)
        elif self.field_type == "date":
            return serializers.DateField(**universal_kwargs)
        elif self.field_type == "datetime":
            return serializers.DateTimeField(**universal_kwargs)
        return serializers.CharField(**universal_kwargs)

    def __str__(self):
        return self.field_name + "(%s)" % self.field_type
