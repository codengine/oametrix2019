from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import fields
from django.db import models
from core.models.base_entity import BaseEntity
from core.models.custom_field.custom_field import CustomField


class CustomFieldValue(BaseEntity):
    object_id = models.PositiveIntegerField()
    content_type = models.ForeignKey(ContentType, blank=True, null=True, on_delete=models.DO_NOTHING)
    content_object = fields.GenericForeignKey('content_type', 'object_id')
    row_id = models.IntegerField(default=0)
    field = models.ForeignKey(CustomField, on_delete=models.CASCADE)
    value = models.CharField(max_length=5000, blank=True, null=True)