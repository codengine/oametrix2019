import pytz
from datetime import datetime
from django.db import models

from core.api.mixins.modelmixins.custom_object_action_model_mixin import CustomObjectActionModelMixin
from core.api.mixins.modelmixins.filter_model_mixin import FilterDataFilterMixin
from core.api.mixins.modelmixins.routable_model_mixin import RoutableModelMixin
from core.api.mixins.modelmixins.searchable_model_mixin import SearchableModelMixin
from core.api.mixins.modelmixins.serializer_model_mixin import SerializerModelMixin
from core.api.mixins.viewmixins.viewsets_post_callback_methods_mixin import ViewSetsPostCallBackMethodsMixin
from core.mixins.modelmixin.common_methods_mixin import CommonMethodsMixin
from core.models.base_abstract_model import BaseAbstractModel
from core.models.base_entity_basic_abstract import BaseEntityBasicAbstract
from core.models.base_entity_rel_abstract import BaseEntityRelAbstract
from core.models.managers.basic_query_manager import BasicQueryManager
from core.models.managers.generic_query_manager import GenericQueryManager


class BaseEntity(BaseAbstractModel, BaseEntityBasicAbstract, BaseEntityRelAbstract,
                 CommonMethodsMixin,
                 CustomObjectActionModelMixin,
                 RoutableModelMixin,
                 ViewSetsPostCallBackMethodsMixin,
                 SerializerModelMixin,
                 SearchableModelMixin,
                 FilterDataFilterMixin):

    objects = GenericQueryManager()
    objects2 = BasicQueryManager()

    class Meta:
        abstract = True
        ordering = ["id"]

    @classmethod
    def handle_object_retrieve(cls, **kwargs):
        pk = kwargs.get('pk')
        return cls.objects.filter(pk=pk).first()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        if not self.pk:
            dt = datetime.utcnow()
            dt = dt.replace(tzinfo=pytz.utc)
            self.date_created = dt

        self.last_updated = datetime.utcnow()
        super(BaseEntity, self).save(force_insert, force_update, using, update_fields)