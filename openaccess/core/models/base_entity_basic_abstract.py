from django.db import models


class BaseEntityBasicAbstract(models.Model):
    date_created = models.DateTimeField()
    date_updated = models.DateTimeField(null=True, blank=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        abstract = True
