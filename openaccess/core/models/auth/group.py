from django.db import models
from django.db.models.query_utils import Q

from core.models.base_entity import BaseEntity


class Group(BaseEntity):
    name = models.CharField(max_length=1000)
    parent = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE)
    enitity_class = models.CharField(max_length=100, null=True, blank=True)

    @classmethod
    def create_of_update(cls, name, parent=None, is_active=True):
        group_instances = cls.objects.filter(name=name)
        if group_instances.exists():
            group_instance = group_instances.first()
        else:
            group_instance = cls()

        group_instance.name = name
        group_instance.parent = parent
        group_instance.is_active = is_active
        group_instance.save()

        return group_instance

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(Group, cls).get_serializer()
        class GroupSerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = ('id', 'name','parent','is_active')
                read_only_fields = ('id', )
        return GroupSerializer

    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        group_ids = [user.group.pk]
        child_groups = Group.objects.filter(parent_id=user.group.id).values_list('pk', flat=True)
        group_ids += child_groups

        filters = [
            Q(**{'id__in': group_ids}),
        ]
        return filters


