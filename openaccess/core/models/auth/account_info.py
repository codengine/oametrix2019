from django.db import models
from core.models.base_entity import BaseEntity
from datetime import datetime, timedelta
import uuid
import pytz
from django.apps import apps


# User = apps.get_model('core', 'User')

class AccountInfo(BaseEntity):
    user = models.ForeignKey('core.User', on_delete=models.CASCADE)
    security_code = models.UUIDField()
    expire_date = models.DateTimeField()

    class Meta:
        abstract = False
        ordering = ["-expire_date"]

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.expire_date:
            dt = datetime.utcnow()
            dt = dt.replace(tzinfo=pytz.utc)
            self.expire_date = dt + timedelta(days=30)
        if not self.security_code:
            self.security_code = uuid.uuid4()
        super(AccountInfo, self).save(force_insert, force_update, using, update_fields)
