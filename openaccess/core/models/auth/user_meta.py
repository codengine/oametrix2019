from django.db import models
from core.models.base_entity import BaseEntity


class UserMeta(BaseEntity):
    orcid_id = models.CharField(max_length=1000, null=True, blank=True)
    pmc_id = models.CharField(max_length=1000, null=True, blank=True)
    phone = models.CharField(max_length=100, null=True, blank=True)
    alt_phone = models.CharField(max_length=100, null=True, blank=True)


    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(UserMeta, cls).get_serializer()

        class UserMetaSerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'orcid_id', 'pmc_id','phone', 'alt_phone')

                read_only_fields = ('id',)

        return UserMetaSerializer



