from django.db import models
from core.models.base_entity import BaseEntity
from core.models.auth.group import Group
from core.models.auth.role import Role


class GroupRole(BaseEntity):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)

    @classmethod
    def create_of_update(cls, group_instance, role_instance):
        instances = cls.objects.filter(group_id=group_instance.pk, role_id=role_instance.pk)
        if instances.exists():
            instance = instances.first()
        else:
            instance = cls()
        instance.group_id = group_instance.pk
        instance.role_id = role_instance.pk
        instance.save()
        return instance

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(GroupRole, cls).get_serializer()
        class GroupRoleSerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = ('id', 'group','role','is_active')
                read_only_fields = ('id', )
        return GroupRoleSerializer
