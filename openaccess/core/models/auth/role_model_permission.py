from django.db import models
from core.models.base_entity import BaseEntity
from core.models.auth.oapermission import OAPermission
from core.models.auth.role import Role


class RoleModelPermission(BaseEntity):
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    permission = models.ForeignKey(OAPermission, on_delete=models.CASCADE)
    model = models.CharField(max_length=100)

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(RoleModelPermission, cls).get_serializer()

        class RoleModelPermissionSerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = ('id', 'role', 'permission','model', 'is_active')
                read_only_fields = ('id', )

        return RoleModelPermissionSerializer

