from django.db import models
from core.models.base_entity import BaseEntity
from core.models.auth.role import Role
from core.models.auth.user import User


class UserRole(BaseEntity):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)


    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(UserRole, cls).get_serializer()

        class UserRoleSerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = ('id', 'user', 'role', 'is_active')
                read_only_fields = ('id',)

        return UserRoleSerializer

