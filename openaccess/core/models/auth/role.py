from django.db import models
from django.db.models.query_utils import Q

from core.models.base_entity import BaseEntity


class Role(BaseEntity):
    name = models.CharField(max_length=1000)

    @classmethod
    def create_of_update(cls, name, is_active=True):
        instances = cls.objects.filter(name=name)
        if instances.exists():
            instance = instances.first()
        else:
            instance = cls()

        instance.name = name
        instance.is_active = is_active
        instance.save()
        return instance

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(Role, cls).get_serializer()

        class RoleSerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = ('id', 'name', 'is_active')
                read_only_fields = ('id', )

        return RoleSerializer

    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        filters = []
        if user.permitted_roles() == 'own':
            role_ids = [user.role.pk]
            filters += [Q(**{'id__in': role_ids})]
        return filters




