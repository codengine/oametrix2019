from __future__ import unicode_literals
from django.db import models
from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.db.models.query_utils import Q
from django.utils.translation import ugettext_lazy as _
from core.models.base_entity import BaseEntity
from core.models.managers.user_manager import UserManager
from core.models.contacts.user_domain_meta import UserDomainMeta
from core.models.organisation.organisation import Organisation
from core.models.auth.user_meta import UserMeta
from core.models.auth.group import Group
from core.models.auth.role import Role
from core.models.contacts.address import Address
import uuid
from rest_framework import serializers
from django.db import transaction
from rest_framework import status
from django.conf import settings
from engine.library.app_util import Mailer
from core.models.mail.email_base import EmailBase
import uuid
from core.models.auth.account_info import AccountInfo


class User(AbstractBaseUser, BaseEntity, PermissionsMixin):
    username = models.EmailField(_('username'), unique=True)
    email = models.EmailField(_('email address'), unique=True)
    group = models.ForeignKey(Group, on_delete=models.DO_NOTHING)
    role = models.ForeignKey(Role, on_delete=models.DO_NOTHING)
    organisation = models.ForeignKey(Organisation, null=True, blank=True, on_delete=models.DO_NOTHING)
    salute = models.CharField(_('salute'), max_length=30, null=True, blank=True)
    first_name = models.CharField(_('first name'), max_length=100, blank=True)
    middle_name = models.CharField(_('middle_name'), max_length=100, null=True, blank=True)
    last_name = models.CharField(_('last name'), max_length=100, blank=True)
    request_source = models.CharField(_('request_source'), max_length=30, null=True, blank=True)
    avatar = models.ImageField(upload_to='avatars/', null=True, blank=True)
    multi_factor_enabled = models.BooleanField(default=False)
    domain_meta = models.ForeignKey(UserDomainMeta, null=True, blank=True, on_delete=models.CASCADE)
    user_meta = models.ForeignKey(UserMeta, null=True, blank=True, on_delete=models.CASCADE)
    addresses = models.ManyToManyField(Address, blank=True)
    unique_id = models.CharField(max_length=100, null=True, blank=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        return "Name: %s, Username: %s, Email: %s" % (self.get_full_name(), self.username, self.email)

    def as_json(self):
        return {
            "id": self.id,
            "username": self.username,
            "fullname": self.get_full_name(),
            "email": self.email,
            "group_id": self.group.id,
            "group_name": self.group.name,
            "role_id": self.role.id,
            "role_name": self.role.name,
            "organisation_id": self.organisation.id,
            "organisation_name": self.organisation.name
        }

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def is_hub_user(self):
        if self.group.name == 'Hub':
            if self.role.name == 'SuperAdmin' or self.role.name == 'Admin':
                return True
        return False

    def is_publisher_user(self):
        return self.group.name == 'Publisher'

    def is_author_user(self):
        if self.group.name != 'Hub':
            return self.role.name == 'User'

    def is_hub_superadmin(self):
        return self.is_hub_user() and (self.role.name == 'SuperAdmin' or self.role.name == 'Admin')

    def is_admin(self):
        return self.group.name == 'SuperAdmin' or self.group.name == 'Admin'

    def get_short_name(self):
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def permitted_roles(self):
        if self.is_hub_superadmin():
            return 'all'
        else:
            return 'own'

    @classmethod
    def create_default_hub_superuser(cls):
        pass

    @classmethod
    def require_auth(cls):
        return True

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(User, cls).get_serializer()

        class UserSerializer(BaseSerializer):
            user_meta = UserMeta.get_serializer()(required=False)
            domain_meta = UserDomainMeta.get_serializer()(required=False)
            addresses = Address.get_serializer()(required=False, many=True)
            fullname = serializers.SerializerMethodField()
            organisation_name = serializers.SerializerMethodField()
            group_name = serializers.SerializerMethodField()
            role_name = serializers.SerializerMethodField()

            class Meta(BaseSerializer.Meta):
                fields = (
                    'id', 'username', 'email', 'group', 'role', 'organisation', 'salute', 'first_name',
                    'middle_name', 'last_name', 'fullname', 'domain_meta', 'user_meta', 'addresses', 'request_source',
                    'unique_id', 'organisation_name', 'group_name', 'role_name')

            def get_fullname(self, obj):
                if obj.salute is not None:
                    full_name = '%s %s %s' % (obj.salute, obj.first_name, obj.last_name)
                else:
                    full_name = '%s %s' % (obj.first_name, obj.last_name)

                return full_name.strip()

            def get_organisation_name(self, obj):
                return obj.organisation.name

            def get_group_name(self, obj):
                return obj.group.name

            def get_role_name(self, obj):
                return obj.role.name

            @transaction.atomic
            def create(self, validated_data):
                initial_data = self.initial_data
                instance = super(UserSerializer, self).create(validated_data)

                if initial_data.get('request_source') == 'hub':
                    instance.set_password(uuid.uuid4())
                else:
                    instance.set_password(initial_data.get('password'))

                unique_id = str(uuid.uuid4().hex)[:10]
                instance.unique_id = unique_id
                instance.save()

                request = self.context.get("request")
                call_back_data = {
                    'request': request,
                    'user': request.user,
                    'model': User,
                    'view_ref': self.context['view'].__class__
                }

                instance.on_post_create(**call_back_data, instance=instance)

                return instance

            def validate(self, data):
                email = data.get('email', None)
                organisation = data.get('organisation', None)
                if organisation is None:
                    raise serializers.ValidationError({"organisation": "This field can't be empty"},
                                                      status.HTTP_400_BAD_REQUEST)

                splited_data = email.split('@')

                if splited_data[1].strip() != organisation.email_domain.strip():
                    raise serializers.ValidationError({"error": "Email domain doesn't match."},
                                                      status.HTTP_400_BAD_REQUEST)
                data = super(UserSerializer, self).validate(data)
                return data

        return UserSerializer

    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        org_ids = [user.organisation.pk]
        child_orgs = Organisation.objects.all().all_active().filter(parent_id=user.organisation.pk).values_list('pk',
                                                                                                                flat=True)
        org_ids += child_orgs
        filters = [
            Q(**{'group_id': user.group.id}),
            Q(**{'organisation_id__in': org_ids})
        ]
        if user.permitted_roles() == 'own':
            role_ids = [user.role.pk]
            filters += [Q(**{'role_id__in': role_ids})]
        return filters

    @classmethod
    def on_post_create(cls, request, user, model, view_ref, **kwargs):
        instance = kwargs['instance']
        with transaction.atomic():
            account_info = AccountInfo(user=instance)
            account_info.save()
        # Sending email
        email_body = EmailBase.objects.filter(type='registration').first().email_body
        email_body += settings.ACCOUNT_ACTIVATION_URL + str(account_info.security_code)

        mailer = Mailer()
        response = mailer.send_email(recipient=instance.email, message=email_body)

        if response is False:
            transaction.set_rollback(True)
            raise serializers.ValidationError({"error": "Email sending process failed."},
                                              status.HTTP_500_INTERNAL_SERVER_ERROR)
