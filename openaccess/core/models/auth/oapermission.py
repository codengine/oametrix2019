from django.db import models
from core.models.base_entity import BaseEntity


class OAPermission(BaseEntity):
    name = models.CharField(max_length=1000)
    perm_code = models.CharField(max_length=200)  # 1111 - can_view can_create can_edit can_delete

    def map_permission_to_http_request_method(self):
        # can_create, can_update, can_delete, can_view, can_approve
        pass

    @classmethod
    def create_of_update(cls, name, perm_code, is_active=True):
        instances = cls.objects.filter(perm_code=perm_code)
        if instances.exists():
            instance = instances.first()
        else:
            instance = cls()

        instance.name = name
        instance.perm_code = perm_code
        instance.is_active = is_active
        instance.save()
        return instance

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(OAPermission, cls).get_serializer()

        class OAPermissionSerializer(BaseSerializer):
            class Meta(BaseSerializer.Meta):
                fields = ('id', 'name', 'perm_code', 'is_active')
                read_only_fields = ('id',)

        return OAPermissionSerializer
