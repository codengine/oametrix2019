from django.db import models
from django.db.models.query_utils import Q

from core.api.constants.permission_code import PermCode
from core.models.base_entity import BaseEntity
from core.models.auth.group import Group
from core.models.auth.oapermission import OAPermission
from core.models.auth.role import Role


class GroupRoleModelPermission(BaseEntity):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    model = models.CharField(max_length=200)
    permissions = models.ManyToManyField(OAPermission)

    @classmethod
    def read_permission(cls, group_id, role_id, model_name):
        instances = cls.objects.filter(group_id=group_id, role_id=role_id, model=model_name)
        if not instances.exists():
            return None
        instance = instances.first()
        return instance.permissions.all()

    @classmethod
    def create_or_update(cls, group_id, role_id, model_name, permission, is_active=True):
        instances = cls.objects.filter(group_id=group_id, role_id=role_id, model=model_name)
        if instances.exists():
            instance = instances.first()
        else:
            instance = cls()

        instance.group_id = group_id
        instance.role_id = role_id
        instance.model = model_name
        instance.is_active = is_active
        instance.save()
        instance.permissions.add(permission)
        return instance

    @classmethod
    def read_permissions(cls, user, model_name):
        instances = cls.objects.filter(group_id=user.group.id, role_id=user.role.id, model=model_name)
        if instances.exists():
            instance = instances.first()
            permission_instances = instance.permissions.all().values_list('perm_code', flat=True)
            perm_codes = []
            for perm_code in permission_instances:
                perm_codes += perm_code.split(',')
            return list(set(perm_codes))
        return []

    @classmethod
    def on_post_list(cls, request, user, model, view_ref, **kwargs):
        print("Inside group role model permission!")

    @classmethod
    def get_serializer(cls):
        BaseSerializer = super(GroupRoleModelPermission, cls).get_serializer()

        class GroupRoleModelPermissionSerializer(BaseSerializer):
            permissions = OAPermission.get_serializer()(required=False, many=True)

            class Meta(BaseSerializer.Meta):
                fields = ('id', 'group', 'role', 'model', 'permissions', 'is_active')
                read_only_fields = ('id',)

        return GroupRoleModelPermissionSerializer

    @classmethod
    def has_view_permission(cls, request, model_name):
        user = request.user
        instances = cls.objects.filter(group_id=user.group.id, role_id=user.role.id, model=model_name)
        if instances.exists():
            instance = instances.first()
        else:
            return False
        return instance.permissions.filter(perm_code__icontains=PermCode.CAN_VIEW.value)

    @classmethod
    def has_group_role_model_permission(cls, request, model_name):

        return True

        user = request.user
        method = request.method
        # return True
        instances = cls.objects.filter(group_id=user.group.id, role_id=user.role.id, model=model_name)
        if instances.exists():
            instance = instances.first()
        else:
            return False
        oa_permission = instance.permission
        perm_code = oa_permission.perm_code

        perm_code_list = perm_code.split(',')

        method = method.upper()
        if method == "GET":
            return PermCode.CAN_VIEW.value in perm_code_list

        elif method == "POST":
            return PermCode.CAN_CREATE.value in perm_code_list

        elif method == "PUT":
            return PermCode.CAN_EDIT.value in perm_code_list

        elif method == "DELETE":
            return PermCode.CAN_DELETE.value in perm_code_list

        return False

    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        group_ids = [user.group.pk]
        child_groups = Group.objects.filter(parent_id=user.group.id).values_list('pk', flat=True)
        group_ids += child_groups

        filters = [
            Q(**{'group_id__in': group_ids}),
            Q(**{'role_id': user.role.pk}),
            Q(**{'permissions__perm_code__icontains': PermCode.CAN_VIEW.value})
        ]

        return filters
