from django.db import models
from core.models.auth.group import Group
from core.models.auth.user import User
from core.models.base_entity import BaseEntity


class UserGroup(BaseEntity):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
