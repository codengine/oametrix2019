import logging
from django.db import models
from django_global_request.middleware import get_request
from core.models.managers.generic_queryset import GenericQuerySet

logger = logging.getLogger('core')


class GenericQueryManager(models.Manager):

    def __init__(self, filters=[]):
        self._filters = filters
        super(GenericQueryManager, self).__init__()

    def get_queryset(self):
        request = get_request()
        queryset = GenericQuerySet(self.model, request=request)
        if self._filters:
            queryset = queryset.filter(*self._filters)
        return queryset
