import logging
from django.db.models.query import QuerySet
from django_global_request.middleware import get_request

logger = logging.getLogger('core')


class GenericQuerySet(QuerySet):
    def __init__(self, model=None, query=None, using=None, hints=None, request=None):
        self.request = request or get_request()
        super(GenericQuerySet, self).__init__(model=model, query=query, using=using, hints=hints)

    def all(self):
        queryset = super(GenericQuerySet, self).all()
        return queryset

    def all_active(self):
        return self.filter(is_active=True)

    def all_inactive(self):
        return self.filter(is_active=False)

    def filter_data(self):
        user = self.request.user
        if user.is_anonymous:
            if not self.model.require_auth():
                return self.filter()
            else:
                return self.none()
        if user and user.is_active and user.is_authenticated:
            if user.is_hub_user():
                logger.info('No data filter is needed for Hub Admin')
                return self.filter()
            else:
                filters = self.model.data_filters(user)
                if type(filters) is list:
                    try:
                        queryset = self.filter(*filters)
                        return queryset
                    except Exception as exp:
                        return self.none()
                else:
                    raise Exception("Invalid filters. A List of filters are expected.")
        return self.filter()

