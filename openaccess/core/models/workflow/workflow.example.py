"""
    {
        "content_type_id": 1,
        "states": ["pending", "FinalApproval"],
        "state_owners": [
            {
                "state": "FinalApproval",
                "role": "SuperAdmin"
            },
            {
                "state": "FinalApproval",
                "role": "Admin"
            }
        ],
        "allowed_actions": [
            {
                "state": "Pending",
                "action": None
            },
            {
                "state": "FinalApproval",
                "action": "Approve"
            },
            {
                "state": "FinalApproval",
                "action": "Reject"
            },
        ],
        "transitions": [
            {
                "state": "FinalApproval",
                "action": "Approve",
                "notify": False,
                "next_state": "Approved"
            },
            {
                "state": "FinalApproval",
                "action": "Reject",
                "notify": True,
                "next_state": "Pending"
            }
        ]
    }
"""