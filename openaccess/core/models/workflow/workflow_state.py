from django.db import models
from core.models.base_entity import BaseEntity


class WorflowState(BaseEntity):
    state = models.CharField(max_length=200)
