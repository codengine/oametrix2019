from django.db import models
from core.models.base_entity import BaseEntity


class WorkflowStateTransition(BaseEntity):
    state = models.CharField(max_length=200)
    action = models.CharField(max_length=200)
    next_state = models.CharField(max_length=200, null=True, blank=True)
    notify = models.BooleanField(default=False)
