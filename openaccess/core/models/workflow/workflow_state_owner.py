from django.db import models
from core.models.auth.role import Role
from core.models.base_entity import BaseEntity


class WorkflowStateOwner(BaseEntity):
    state = models.CharField(max_length=200)
    role = models.ForeignKey(Role, on_delete=models.DO_NOTHING)
