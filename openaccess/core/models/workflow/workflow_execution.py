from django.contrib.contenttypes.fields import GenericForeignKey
from django.db import models
from django.contrib.contenttypes.models import ContentType
from core.models.base_entity import BaseEntity


class WorkflowExecution(BaseEntity):
    # For generic foreign key relationship
    content_type = models.ForeignKey(ContentType, on_delete=models.DO_NOTHING)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    current_state = models.CharField(max_length=200)
    action_taken = models.CharField(max_length=200)
    notified = models.BooleanField(default=False)
    note = models.TextField(null=True, blank=True)

