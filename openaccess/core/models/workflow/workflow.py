from django.contrib.contenttypes.models import ContentType
from django.db import models
from core.models.base_entity import BaseEntity
from core.models.workflow.workflow_execution import WorkflowExecution
from core.models.workflow.workflow_state import WorflowState
from core.models.workflow.workflow_state_action import WorkflowStateAction
from core.models.workflow.workflow_state_owner import WorkflowStateOwner
from core.models.workflow.workflow_state_transition import WorkflowStateTransition


class Workflow(BaseEntity):
    content_type = models.ForeignKey(ContentType, on_delete=models.DO_NOTHING)
    states = models.ManyToManyField(WorflowState)
    transitions = models.ManyToManyField(WorkflowStateTransition)
    allowed_actions = models.ManyToManyField(WorkflowStateAction)
    state_owners = models.ManyToManyField(WorkflowStateOwner)

    def get_next_state(self, object_id):
        pass

    def get_current_state(self, object_id):
        last_executed_action = WorkflowExecution.objects.filter(content_type_id=self.content_type_id,
                                                                object_id=object_id).order_by('-id').first()
        if not last_executed_action:
            # No action had taken place.
            pass

    def get_previous_state(self, object_id):
        pass
            
    def execute_action(self, object_id, user, action, note=None):
        pass

