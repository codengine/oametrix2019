from django.db import models
from core.models.base_entity import BaseEntity


class WorkflowStateAction(BaseEntity):
    state = models.CharField(max_length=200)
    action = models.CharField(max_length=200)
