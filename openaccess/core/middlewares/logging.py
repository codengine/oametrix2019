from django.utils.deprecation import MiddlewareMixin
import logging
import json
from django.conf import settings
from django.http import QueryDict
from core.models.logging.audit_log import AuditLog
import traceback

logger = logging.getLogger(__name__)
REQUEST_LOG_ID = None


class LoggingMiddleware(MiddlewareMixin):

    def process_request(self, request):
        enabled = getattr(settings, 'LOG_ENABLED', False)
        if not enabled:
            return

        if request.method == "GET":
            REQUEST_DATA = request.GET
        else:
            REQUEST_DATA = QueryDict(request.body)

        log_obj = AuditLog(url=request.build_absolute_uri(),
                           request_method=request.method,
                           request_data=json.dumps(REQUEST_DATA) )

        log_obj.save()

        # saving REQUEST_LOG_ID for error log
        global REQUEST_LOG_ID
        REQUEST_LOG_ID = log_obj.id

    def process_exception(self, request, exception):

        enabled = getattr(settings, 'LOG_ENABLED', False)
        if not enabled:
            return

        log_obj = AuditLog.objects.get(id=REQUEST_LOG_ID)
        try:
            log_obj.message = exception.message
        except:
            log_obj.message = exception

        # log_obj.stacktrace = traceback.extract_stack()
        log_obj.stacktrace = traceback.format_exc()
        log_obj.save()

