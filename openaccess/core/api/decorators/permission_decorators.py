import logging
from functools import wraps
from rest_framework.exceptions import PermissionDenied
from core.models.auth.group_role_model_permission import GroupRoleModelPermission

logger = logging.getLogger(__file__)


def require_permissions(permission_list=[]):
    def outer_wrapper(func):
        @wraps(func)
        def wrapped(self, request, *args, **kwargs):
            if request.user.is_authenticated:
                if not request.user.is_hub_superadmin():
                    permission_model = self.model.permission_model()
                    permissions = GroupRoleModelPermission.read_permissions(request.user, model_name=permission_model.__name__)
                    permission_confirmed = list(set(permission_list) & set(permissions))
                    if len(permission_confirmed) != len(permission_list):
                        raise PermissionDenied("Permission required")
            response = func(self, request, *args, **kwargs)
            return response
        return wrapped
    return outer_wrapper
