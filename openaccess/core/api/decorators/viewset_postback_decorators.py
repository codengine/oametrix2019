import logging
from functools import wraps


logger = logging.getLogger(__file__)


def ensure_viewset_action_post_callback(method):
    def outer_wrapper(func):
        @wraps(func)
        def wrapped(self, request, *args, **kwargs):
            response = func(self, request, *args, **kwargs)
            call_back_data = {
                'request': request,
                'user': request.user,
                'model': self.model,
                'view_ref': self
            }
            if method == "list":
                try:
                    self.model.on_post_list(**call_back_data)
                except Exception as exp:
                    logger.info("Exception on post list call")
            elif method == "create":
                try:
                    # call_back_data["instance"] = self.instance
                    self.model.on_post_create(**call_back_data)
                except Exception as exp:
                    logger.info("Exception on post create call")
            elif method == "update":
                try:
                    self.model.on_post_update(**call_back_data)
                except Exception as exp:
                    logger.info("Exception on post update call")
            elif method == "delete":
                try:
                    self.model.on_post_delete(**call_back_data)
                except Exception as exp:
                    logger.info("Exception on post delete call")
            elif method == "retrieve":
                try:
                    self.model.on_post_retrieve(**call_back_data)
                except Exception as exp:
                    logger.info("Exception on post retrieve call")
            elif method == "upload":
                try:
                    self.model.on_post_upload(**call_back_data)
                except Exception as exp:
                    logger.info("Exception on post upload call")
            elif method == "download":
                try:
                    self.model.on_post_download(**call_back_data)
                except Exception as exp:
                    logger.info("Exception on post download call")
            elif method == "approve":
                try:
                    self.model.on_post_approve(**call_back_data)
                except Exception as exp:
                    logger.info("Exception on post approve call")
            elif method == "reject":
                try:
                    self.model.on_post_reject(**call_back_data)
                except Exception as exp:
                    logger.info("Exception on post reject call")
            return response
        return wrapped
    return outer_wrapper
