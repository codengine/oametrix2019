from collections import OrderedDict
from django.db import transaction
from django.db.models.base import Model
from django.db.models.fields.related import ForeignKey, OneToOneField
from rest_framework import serializers
from core.api.actions.view_action import ViewAction
from core.api.constants.permission_code import PermCode
from core.constants.approval_status import ApprovalStatusEnum
from core.loader.loader import load_model


class SerializerModelMixin(object):

    @classmethod
    def action_template(cls, action_label, instance_action, request_method):
        return {
                "label": action_label,
                "action": instance_action,
                "request_method": request_method
        }

    @classmethod
    def get_serializer(cls):
        class GenericModelSerializer(serializers.ModelSerializer):
            def __init__(self, *args, **kwargs):

                super(GenericModelSerializer, self).__init__(*args, **kwargs)

                if self.context.get('request', None) is not None:
                    fields = self.context['request'].query_params.get('fields')
                    if fields:
                        fields = fields.split(',')
                        # Drop any fields that are not specified in the `fields` argument.
                        allowed = set(fields)
                        existing = set(self.fields.keys())
                        for field_name in existing - allowed:
                            self.fields.pop(field_name)

                super(GenericModelSerializer, self).__init__(*args, **kwargs)

            def is_valid(self, raise_exception=False):
                valid = super(GenericModelSerializer, self).is_valid(raise_exception=raise_exception)
                return valid



            def to_representation(self, instance):
                response = super(GenericModelSerializer, self).to_representation(instance)
                # response['permissions'] = {}
                instance_actions = [
                    ViewAction.UPDATE.value,
                    ViewAction.DELETE.value,
                    ViewAction.APPROVE.value,
                    ViewAction.REJECT.value
                ]

                actions = []

                object_action_label_mapping = instance.__class__.map_object_action_to_label() or {}

                request = self.context['request']

                GroupRoleModelPermission = load_model('core', 'GroupRoleModelPermission')

                edit_actions = []
                approval_actions = []
                custom_actions = []

                for instance_action in instance_actions:
                    if instance_action == ViewAction.UPDATE.value:
                        action_label = object_action_label_mapping.get(instance_action, instance_action.capitalize())
                        if request.user and request.user.is_authenticated and request.user.is_hub_superadmin():
                            if instance.show_edit_action():
                                action_instance = instance.__class__.action_template(action_label=action_label,
                                                                       instance_action=instance_action,
                                                                       request_method="PUT")
                                edit_actions += [
                                    action_instance
                                ]
                        else:
                            if request.user and request.user.is_authenticated:
                                if instance.created_by_group_id == request.user.group_id and instance.created_by_role_id == request.user.role_id:
                                    if instance.show_delete_action():
                                        action_instance = instance.__class__.action_template(action_label=action_label,
                                                                               instance_action=instance_action,
                                                                               request_method="PUT")
                                        edit_actions += [
                                            action_instance
                                        ]

                    elif instance_action == ViewAction.DELETE.value:
                        action_label = object_action_label_mapping.get(instance_action, instance_action.capitalize())
                        if request.user and request.user.is_authenticated and request.user.is_hub_superadmin():
                            action_instance = instance.__class__.action_template(action_label=action_label,
                                                                   instance_action=instance_action,
                                                                   request_method="DELETE")
                            edit_actions += [
                                action_instance
                            ]
                        else:
                            if request.user and request.user.is_authenticated:
                                if instance.created_by_group_id == request.user.group_id and instance.created_by_role_id == request.user.role_id:
                                    action_instance = instance.__class__.action_template(action_label=action_label,
                                                                           instance_action=instance_action,
                                                                           request_method="DELETE")
                                    edit_actions += [
                                        action_instance
                                    ]
                    elif instance_action == ViewAction.APPROVE.value:
                        if hasattr(instance.__class__, 'approval_enabled'):
                            if getattr(instance.__class__, 'approval_enabled')():
                                if instance.approval_status == ApprovalStatusEnum.PENDING.value:
                                    action_label = object_action_label_mapping.get(instance_action, instance_action.capitalize())
                                    if request.user and request.user.is_authenticated and request.user.is_hub_superadmin():
                                        action_instance = instance.__class__.action_template(action_label=action_label,
                                                                               instance_action=instance_action,
                                                                               request_method="PUT")
                                        approval_actions += [
                                            action_instance
                                        ]
                                    else:
                                        if request.user and request.user.is_authenticated:
                                            if instance.counter_organisation.domain_id == request.user.organisation.domain_id:
                                                permissions = GroupRoleModelPermission.read_permissions(user=request.user,
                                                                                                        model_name=instance.__class__.__name__)
                                                if PermCode.CAN_APPROVE.value in permissions:
                                                    action_instance = instance.__class__.action_template(action_label=action_label,
                                                                                           instance_action=instance_action,
                                                                                           request_method="PUT")
                                                    approval_actions += [
                                                        action_instance
                                                    ]
                                else:
                                    edit_actions = []
                    elif instance_action == ViewAction.REJECT.value:
                        if hasattr(instance.__class__, 'approval_enabled'):
                            if getattr(instance.__class__, 'approval_enabled')():
                                if instance.approval_status == ApprovalStatusEnum.PENDING.value:
                                    action_label = object_action_label_mapping.get(instance_action, instance_action.capitalize())
                                    if request.user and request.user.is_authenticated and request.user.is_hub_superadmin():
                                        action_instance = instance.__class__.action_template(action_label=action_label,
                                                                               instance_action=instance_action,
                                                                               request_method="PUT")
                                        approval_actions += [
                                            action_instance
                                        ]
                                    else:
                                        if request.user and request.user.is_authenticated:
                                            if instance.counter_organisation.domain_id == request.user.organisation.domain_id:
                                                permissions = GroupRoleModelPermission.read_permissions(user=request.user,
                                                                                                        model_name=instance.__class__.__name__)
                                                if PermCode.CAN_REJECT.value in permissions:
                                                    action_instance = instance.__class__.action_template(action_label=action_label,
                                                                                           instance_action=instance_action,
                                                                                           request_method="PUT")
                                                    approval_actions += [
                                                        action_instance
                                                    ]

                if instance.__class__.has_custom_object_actions():
                    custom_actions += instance.generate_custom_actions(request)

                response['actions'] = edit_actions + approval_actions + custom_actions

                return response

            def create(self, validated_data):
                with transaction.atomic():
                    instance_id = validated_data.pop('id') if "id" in validated_data else None
                    if instance_id:
                        responseObj = self.Meta.model.objects.filter(pk=instance_id)
                        if responseObj.exists():
                            return responseObj.first()

                    if self.context['request'].user.is_authenticated:
                        validated_data.update({
                            "created_by": self.context['request'].user,
                            "created_by_group": self.context['request'].user.group,
                            "created_by_role": self.context['request'].user.role,
                        })
                    m2m_fields = [
                        (f, f.model if f.model != self.Meta.model else None)
                        for f in self.Meta.model._meta.get_fields()
                        if f.many_to_many and not f.auto_created
                        ]
                    m2m_dict = dict()
                    for m in m2m_fields:
                        m2m_dict[m[0].name] = validated_data.pop(m[0].name, [])

                    attributes = self.Meta.model._meta.fields

                    for attr in attributes:
                        if attr.name in validated_data and isinstance(validated_data[attr.name], dict):
                            _serializer = attr.related_model.get_serializer()(context=self.context)
                            validated_data[attr.name] = _serializer.create(validated_data[attr.name])

                    pk = validated_data.pop('id', None)
                    if pk:
                        obj = self.Meta.model.objects.get(pk=pk)
                    else:
                        obj = self.Meta.model.objects.create(**validated_data)

                    for attr in attributes:
                        if isinstance(attr, ForeignKey) or isinstance(attr, OneToOneField):
                            value = validated_data.pop(attr.name, None)
                            if isinstance(value, Model) and value.pk is None:
                                value.save()
                            setattr(obj, attr.name,
                                    attr.model.objects.get(pk=value) if isinstance(value, int) else value)
                        else:
                            pass
                    obj.save()

                    for m in m2m_fields:
                        _field = getattr(obj, m[0].name)
                        _values = m2m_dict[m[0].name]
                        for v in _values:
                            if isinstance(v, (dict, OrderedDict)):
                                _serializer = m[0].related_model.get_serializer()(context=self.context)
                                v = _serializer.create(v)

                            v.save()
                            _field.add(v)

                    return obj

            def update(self, instance, validated_data):
                with transaction.atomic():
                    if self.context['request'].user.is_authenticated:
                        instance.updated_by = self.context['request'].user
                        instance.updated_by_group = self.context['request'].user.group
                        instance.updated_by_role = self.context['request'].user.role

                    m2m_fields = [
                        (f, f.model if f.model != self.Meta.model else None)
                        for f in self.Meta.model._meta.get_fields()
                        if f.many_to_many and not f.auto_created
                        ]

                    m2m_dict = dict()
                    for m in m2m_fields:
                        m2m_dict[m[0].name] = validated_data.pop(m[0].name, [])

                    attributes = self.Meta.model._meta.fields

                    for attr in attributes:
                        if attr.name in validated_data and isinstance(validated_data[attr.name], dict):
                            _obj = getattr(instance, attr.name)
                            _serializer = attr.related_model.get_serializer()(context=self.context)

                            if _obj is None:
                                validated_data[attr.name] = _serializer.create(validated_data[attr.name])
                            else:
                                validated_data[attr.name] = _serializer.update(_obj, validated_data[attr.name])

                    for attr in attributes:
                        if attr.name in validated_data.keys():
                            if isinstance(attr, ForeignKey) or isinstance(attr, OneToOneField):
                                value = validated_data.pop(attr.name, None)
                                if isinstance(value, Model) and value.pk is None:
                                    value.save()
                                setattr(instance, attr.name,
                                        attr.model.objects.get(pk=value) if isinstance(value, int) else value)

                    for m in m2m_fields:
                        if m[0].name in m2m_dict.keys() and len(m2m_dict[m[0].name]) > 0:
                            _field = getattr(instance, m[0].name)
                            _values = m2m_dict[m[0].name]
                            _field.clear()
                            for v in _values:
                                if isinstance(v, (dict, OrderedDict)):
                                    _serializer = m[0].related_model.get_serializer()(context=self.context)
                                    v = _serializer.create(v)
                                    v.save()
                                    _field.add(v)
                                elif isinstance(v, int):
                                    v = m[0].related_model.objects.get(pk=v)
                                    v.save()
                                    _field.add(v)

                    return super(GenericModelSerializer, self).update(instance, validated_data)

            class Meta:
                model = cls
                fields = '__all__'
                read_only_fields = ('id', 'date_created', 'date_updated', 'created_by', 'updated_by')
                level = 2

        return GenericModelSerializer

