class FilterDataFilterMixin(object):
    @classmethod
    def filter_model_data(cls, user, queryset, *args, **kwargs):
        return queryset

    @classmethod
    def data_filters(cls, user, *args, **kwargs):
        return []

    @classmethod
    def remove_duplicates(cls, queryset, *args, **kwargs):
        return queryset

    @classmethod
    def apply_order_by(cls, request, queryset, *args, **kwargs):
        return queryset.order_by('-id')

