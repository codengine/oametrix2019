import re

from core.constants.view_action import ViewAction


class RoutableModelMixin(object):

    @classmethod
    def convert_to_snake_case(cls, name, placeholder="-"):
        if placeholder == "-":
            string = re.sub('(.)([A-Z][a-z]+)', r'\1-\2', name)
            return re.sub('([a-z0-9])([A-Z])', r'\1-\2', string).lower()
        elif placeholder == "_":
            string = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
            return re.sub('([a-z0-9])([A-Z])', r'\1_\2', string).lower()

    @classmethod
    def get_route_name(cls):
        return cls.convert_to_snake_case(cls.__name__)

    @classmethod
    def get_route_url_name(cls):
        return cls.convert_to_snake_case(cls.__name__, placeholder='_')

    @classmethod
    def allow_view_actions(cls):
        return ViewAction.all()

    @classmethod
    def generate_auto_urls(cls):

        requested_view_actions = cls.allow_view_actions()

        approval_enabled_method_exists = lambda x: True if hasattr(x, 'approval_enabled') else False
        approval_enabled_activated = getattr(cls, 'approval_enabled')() if approval_enabled_method_exists(cls) else False

        auto_urls = [
            {
                "path": cls.get_route_name(),
                "method": "GET",
                "action_handler": "list",
                "action": ViewAction.LIST.value
            },
            {
                "path": cls.get_route_name(),
                "method": "POST",
                "action_handler": "create",
                "action": ViewAction.CREATE.value
            },
            {
                "path": cls.get_route_name() + "/download",
                "method": "GET",
                "action_handler": "download",
                "action": ViewAction.DOWNLOAD.value
            },
            {
                "path": cls.get_route_name() + "/upload",
                "method": "POST",
                "action_handler": "upload",
                "action": ViewAction.UPLOAD.value
            },
            {
                "path": '%s/(?P<pk>[0-9]+)' % cls.get_route_name(),
                "method": "GET",
                "action_handler": "retrieve",
                "action": ViewAction.RETRIEVE.value
            },
            {
                "path": '%s/(?P<pk>[0-9]+)' % cls.get_route_name(),
                "method": "PUT",
                "action_handler": "update",
                "action": ViewAction.UPDATE.value
            },
            {
                "path": '%s/(?P<pk>[0-9]+)' % cls.get_route_name(),
                "method": "DELETE",
                "action_handler": "destroy",
                "action": ViewAction.DELETE.value
            }
        ]

        if approval_enabled_activated:
            auto_urls += [
                {
                    "path": cls.get_route_name() + "/(?P<ids>[\d\,]+)/approve",
                    "method": "PUT",
                    "action_handler": "approve_handler",
                    "action": ViewAction.APPROVE.value
                }
            ]
            auto_urls += [
                {
                    "path": cls.get_route_name() + "/(?P<ids>[\d\,]+)/reject",
                    "method": "PUT",
                    "action_handler": "reject_handler",
                    "action": ViewAction.REJECT.value
                }
            ]

        has_custom_object_actions = cls.has_custom_object_actions()
        if has_custom_object_actions:
            auto_urls += [
                {
                    "path": cls.get_route_name() + "/(?P<pk>[0-9]+)/mutate/<mutate_action>",
                    "method": "PUT",
                    "action_handler": "mutate",
                    "action": ViewAction.MUTATE.value
                }
            ]

        allowed_urls = []
        for url_config in auto_urls:
            if url_config["action"] in requested_view_actions:
                allowed_urls += [ url_config ]

        return allowed_urls


    @classmethod
    def override_viewset(cls):
        return None


