class CustomObjectActionModelMixin(object):

    @classmethod
    def has_custom_object_actions(cls):
        return False

    def show_edit_action(self):
        return True

    def show_delete_action(self):
        return True

    def generate_custom_actions(self, request):
        return []

