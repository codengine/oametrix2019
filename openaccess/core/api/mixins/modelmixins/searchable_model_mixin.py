import json
from django.core.exceptions import FieldDoesNotExist
from django.db import models
from django.db.models import Q


class SearchableModelMixin(object):

    @classmethod
    def disable_search_fields(cls):
        return []

    @classmethod
    def get_searchable_model_fields(cls, only_names=False):
        model = cls
        model_fields = model._meta.get_fields()
        # Get many to many fields
        m2m_fields = [
            (f, f.model if f.model != model else None) for f in model_fields if f.many_to_many and not f.auto_created
            ]
        one_to_many_fields = [
            (f, f.model if f.model != model else None) for f in model_fields if f.one_to_many and f.auto_created
            and f.related_model.__name__ != model.__name__
        ]
        m2m_field_names = [m[0].name for m in m2m_fields]
        one_to_many_field_names = [m[0].name for m in one_to_many_fields]
        searchable_model_fields = []
        disabled_search_fields = cls.disable_search_fields()
        for model_field in model_fields:
            if model_field.name in m2m_field_names or model_field.name in one_to_many_field_names:
                continue
            if model_field.name not in disabled_search_fields:
                if not only_names:
                    searchable_model_fields += [model_field]
                else:
                    searchable_model_fields += [model_field.name]
        return searchable_model_fields

    @classmethod
    def collect_search_params(cls, request):
        search_fields = []
        model = cls
        ignore = ['search', 'paginate_by', 'sort', 'page', 'depth', 'expand', 'format', 'authkey',
                  'disable_pagination', 'operator', 'csrfmiddlewaretoken']
        for key, value in request.GET.items():
            if key not in ignore:
                search_fields += [{key: value}]
        return search_fields

    @classmethod
    def apply_custom_search(cls, request, queryset, search_params, *args, **kwargs):
        return queryset

    @classmethod
    def extract_fields(cls, search_fields=[], searchable_model_fields=[]):
        model_fields, or_fields, extra_fields = [], [], []
        for search_param in search_fields:
            # {key: value}
            # key = field:property.action[optional]
            # For example: the following search key are all valid
            # /endpoint/?name=SomeName It will match exact by default
            # /endpoint/?name.start=SomeName It will match that start with SomeName

            # /endpoint/?parent_id=2
            # /endpoint/?parent:id=2
            # /endpoint/?parent:name.exact=ParentName
            # /endpoint/?parent:name=ParentName
            # /endpoint/?parent:name.start=ParentName This will match parent that start with Parentname
            # /endpoint/?parent:name.end=ParentName This will match parent that end with Parentname
            # /endpoint/?parent:name.in=ParentName This will match parent that contains the name Parentname

            # /endpoint/?date_created.bin=timestamp1, timestamp2 This will match date created between timestamp1 and timestamp2

            # /endpoint/?field_name.[operator] For example
            # /endpoint/?name.and=Name1,Name2 So it will match queryset which match with Name1 and Name2
            # /endpoint/?name.or=Name1,Name2 So it will match queryset which match with Name1 or Name2

            # The special or parameter should handle the or logic in the quer string.
            # For example,
            # /endpoint/?or=(nested querystring separated by pipe operator)
            # /endpoint/?or=(id=9|name=Name) It should return queryset matching either id=9 or name=Name

            search_key, search_value = next((k, v) for k, v in search_param.items())

            search_key_split = search_key.split(':')

            search_key_field_name = search_key_split[0]

            search_key_field_name = search_key_field_name.split(".")[0]

            if search_key_field_name in searchable_model_fields:
                model_fields += [search_param]
            elif search_key_field_name in ['or', 'OR']:
                or_fields += [search_param]
            else:
                extra_fields += [search_param]

        return model_fields, or_fields, extra_fields

    @classmethod
    def build_single_filter(cls, model, prop, query_strings, prefix='', **kwargs):

        if prop.find(':') == -1:
            try:
                # Lets try to find the expression
                if prop.find(".") == -1:
                    # No expression
                    field_name = prop
                    expression = None
                else:
                    prop_split = prop.split(".")
                    field_name = prop_split[0]
                    expression = prop_split[1]

                m_fields = model._meta.get_fields()
                field = None
                for m_field in m_fields:
                    if m_field.name == field_name:
                        field = m_field
                        break

                if isinstance(field, models.AutoField):
                    expression = expression.lower() if expression else None

                    if expression == 'in':
                        query_strings = query_strings.split('|')
                        entry_query = Q(**{"%s__in" % (prefix + field_name): tuple(query_strings)})
                        return entry_query
                    elif expression == 'eq':
                        entry_query = Q(**{"%s" % (prefix + field_name): query_strings})
                        return entry_query
                    elif expression == 'ne':
                        entry_query = ~Q(**{"%s" % (prefix + field_name): query_strings})
                        return entry_query
                    elif expression == 'ni':
                        query_strings = query_strings.split('|')
                        entry_query = ~Q(**{"%s__in" % (prefix + field_name): tuple(query_strings)})
                        return entry_query
                    else:
                        entry_query = Q(**{"%s" % (prefix + field_name): query_strings})
                        return entry_query
                    # elif expression == 'ge':
                    #     q = Q(**{"%s__ge" % (prefix + field_name): query_strings})
                    #     return q
                    # elif expression == 'gt':
                    #     pass
                    # elif expression == 'le':
                    #     pass
                    # elif expression == 'lt':
                    #     pass
                    # return
                elif isinstance(field, models.ForeignKey):
                    entry_query = cls.build_single_filter(field.related_model, 'id', query_strings,
                                                  prefix=field.name + '_')
                    return entry_query
                elif isinstance(field, models.ManyToManyField):
                    pass
                elif isinstance(field, models.DecimalField):
                    query_strings = float(query_strings)
                    expression = expression.lower() if expression else None
                    if expression == 'in':
                        query_strings = query_strings.split('|')
                        entry_query = Q(**{"%s__in" % (prefix + field_name): tuple(query_strings)})
                        return entry_query
                    elif expression == 'eq':
                        entry_query = Q(**{"%s" % (prefix + field_name): query_strings})
                        return entry_query
                    elif expression == 'ne':
                        entry_query = ~Q(**{"%s" % (prefix + field_name): query_strings})
                        return entry_query
                    elif expression == 'ni':
                        query_strings = query_strings.split('|')
                        entry_query = ~Q(**{"%s__in" % (prefix + field_name): tuple(query_strings)})
                        return entry_query
                    else:
                        entry_query = Q(**{"%s" % (prefix + field_name): query_strings})
                        return entry_query
                elif isinstance(field, (models.BigIntegerField, models.IntegerField)):
                    query_strings = int(float(query_strings))
                    expression = expression.lower() if expression else None
                    if expression == 'in':
                        query_strings = query_strings.split('|')
                        entry_query = Q(**{"%s__in" % (prefix + field_name): tuple(query_strings)})
                        return entry_query
                    elif expression == 'eq':
                        entry_query = Q(**{"%s" % (prefix + field_name): query_strings})
                        return entry_query
                    elif expression == 'ne':
                        entry_query = ~Q(**{"%s" % (prefix + field_name): query_strings})
                        return entry_query
                    elif expression == 'ni':
                        query_strings = query_strings.split('|')
                        entry_query = ~Q(**{"%s__in" % (prefix + field_name): tuple(query_strings)})
                        return entry_query
                    else:
                        entry_query = Q(**{"%s" % (prefix + field_name): query_strings})
                        return entry_query
                elif isinstance(field, models.BooleanField):
                    if query_strings.isdigit():
                        query_strings = int(query_strings)
                        query_strings = bool(query_strings)
                    else:
                        query_strings = json.loads(query_strings)
                    entry_query = Q(**{"%s" % (prefix + field_name): query_strings})
                    return entry_query
                else:
                    expression = expression.lower() if expression else None
                    if expression == 'in':
                        query_strings = query_strings.split('|')
                        entry_query = Q(**{"%s__in" % (prefix + field_name): tuple(query_strings)})
                        return entry_query
                    elif expression == 'eq':
                        entry_query = Q(**{"%s" % (prefix + field_name): query_strings})
                        return entry_query
                    elif expression == 'ne':
                        entry_query = ~Q(**{"%s" % (prefix + field_name): query_strings})
                        return entry_query
                    elif expression == 'ni':
                        query_strings = query_strings.split('|')
                        entry_query = ~Q(**{"%s__in" % (prefix + field_name): tuple(query_strings)})
                        return entry_query
                    else:
                        entry_query = Q(**{"%s__contains" % (prefix + field_name): query_strings})
                        return entry_query
            except Exception as exp:
                return None
        else:
            property_list = prop.split(':')
            m_fields = model._meta.get_fields()
            field = None
            for m_field in m_fields:
                if m_field.name == property_list[0]:
                    field = m_field
                    break
            entry_query = cls.build_single_filter(field.related_model, ":".join(property_list[1:]),
                                                  query_strings, prefix=property_list[0] + '__')
            return entry_query

    @classmethod
    def build_generic_model_search_filter(cls, request, queryset, search_request_params, *args, **kwargs):
        if not search_request_params:
            return None
        filter = None
        for param in search_request_params:
            search_key, search_value = next((k, v) for k, v in param.items())
            filter_expression = cls.build_single_filter(cls, search_key, search_value, prefix='', **kwargs)
            if not filter_expression:
                return queryset.model.objects.none()
            if filter is None:
                filter = filter_expression
            else:
                filter &= filter_expression
        return filter

    @classmethod
    def build_or_search_filter(cls, request, queryset, search_request_params, *args, **kwargs):
        if not search_request_params:
            return None
        filter = None
        try:
            # [{'or': '(id=10|name=Sohel)'}]
            or_filters = search_request_params[0]['or']
            or_filters = or_filters[1:len(or_filters) - 1]
            or_filters = or_filters.split('|')
            for or_filter in or_filters:
                or_filter_split = or_filter.split('=')
                search_key, search_value = or_filter_split[0], or_filter_split[1]
                filter_expression = cls.build_single_filter(cls, search_key, search_value, prefix='', **kwargs)
                if not filter_expression:
                    return queryset.model.objects.none()
                if filter is None:
                    filter = filter_expression
                else:
                    filter |= filter_expression
        except Exception as exp:
            return None
        return filter

    @classmethod
    def build_custom_search_filter(cls, request, queryset, search_request_params, *args, **kwargs):
        return queryset.model.filter_queryset(request, queryset, search_request_params, *args, **kwargs)

    @classmethod
    def apply_search(cls, request, queryset, model_fields, or_fields, extra_fields, *args, **kwargs):
        model_search_filter = cls.build_generic_model_search_filter(request, queryset, model_fields, *args, **kwargs)
        if model_search_filter:
            queryset = queryset.filter(model_search_filter)
        or_search_filter = cls.build_or_search_filter(request, queryset, or_fields, *args, **kwargs)
        if or_search_filter:
            queryset = queryset.filter(or_search_filter)
        queryset = cls.build_custom_search_filter(request, queryset, extra_fields, *args, **kwargs)
        return queryset

    @classmethod
    def apply_search_filter(cls, request, queryset, *args, **kwargs):
        searchable_model_fields = cls.get_searchable_model_fields(only_names=True)
        search_request_params = cls.collect_search_params(request)
        model_fields, or_fields, extra_fields = cls.extract_fields(search_request_params, searchable_model_fields)
        queryset = cls.apply_search(request, queryset, model_fields, or_fields, extra_fields, *args, **kwargs)
        return queryset
