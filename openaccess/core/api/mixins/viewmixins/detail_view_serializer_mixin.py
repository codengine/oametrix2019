from django.http.response import Http404


class DetailViewSerializerMixin(object):
    def get_object(self):
        try:
            action = self.action
            model = self.model
            get_object_method = self.object_retrieve_method_mapping.get(action, model.handle_object_retrieve)
            instance = get_object_method(**self.kwargs)
            if not instance:
                raise Http404
            return instance
        except Exception as exp:
            raise Http404