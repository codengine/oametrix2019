from core.api.authorization.is_authorized import IsAuthorized
from core.api.authorization.token_authentication import OpenAccessTokenAuthentication
from core.api.renderer.open_access_json_renderer import OpenAccessJSONRenderer


class ProtectedViewMixin(object):
    renderer_classes = (OpenAccessJSONRenderer,)
    authentication_classes = (OpenAccessTokenAuthentication,)
    permission_classes = (IsAuthorized,)
    # throttle_classes = (OpenAccessLoginRateThrottle, )

    def get_queryset(self, *args, **kwargs):
        queryset = self.model.objects.all()
        return queryset

    def filter_queryset(self, queryset, *args, **kwargs):
        queryset = queryset.filter_data()
        queryset = self.model.apply_search_filter(self.request, queryset)
        queryset = self.model.remove_duplicates(queryset, *args, **kwargs)
        queryset = self.model.apply_order_by(self.request, queryset, *args, **kwargs)
        return queryset

    def get_object(self):
        return super(ProtectedViewMixin, self).get_object()




