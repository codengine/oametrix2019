from django.db import transaction
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response
from core.api.constants.permission_code import PermCode
from core.api.decorators.permission_decorators import require_permissions
from core.api.decorators.viewset_postback_decorators import ensure_viewset_action_post_callback
from core.models.auth.group_role_model_permission import GroupRoleModelPermission
from core.constants.approval_status import ApprovalStatusEnum


class ViewActionHandlerMixin(object):

    @require_permissions(permission_list=[PermCode.CAN_VIEW.value])
    @ensure_viewset_action_post_callback("list")
    def list(self, request, *args, **kwargs):
        return super(ViewActionHandlerMixin, self).list(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save()
        self.instance = serializer.instance

    @require_permissions(permission_list=[PermCode.CAN_CREATE.value])
    @ensure_viewset_action_post_callback("create")
    def create(self, request, *args, **kwargs):
        return super(ViewActionHandlerMixin, self).create(request, *args, **kwargs)

    @require_permissions(permission_list=[PermCode.CAN_EDIT.value])
    @ensure_viewset_action_post_callback("update")
    def update(self, request, *args, **kwargs):
        return super(ViewActionHandlerMixin, self).update(request, *args, **kwargs)

    @require_permissions(permission_list=[PermCode.CAN_DELETE.value])
    @ensure_viewset_action_post_callback("delete")
    def destroy(self, request, *args, **kwargs):
        return super(ViewActionHandlerMixin, self).destroy(request, *args, **kwargs)

    @require_permissions(permission_list=[PermCode.CAN_VIEW.value])
    @ensure_viewset_action_post_callback("retrieve")
    def retrieve(self, request, *args, **kwargs):
        return super(ViewActionHandlerMixin, self).retrieve(request, *args, **kwargs)

    @require_permissions(permission_list=[PermCode.CAN_DOWNLOAD.value])
    @ensure_viewset_action_post_callback("download")
    def download(self, request, *args, **kwargs):
        return Response({"message": "Not implemented yet"})

    @require_permissions(permission_list=[PermCode.CAN_UPLOAD.value])
    @ensure_viewset_action_post_callback("upload")
    def upload(self, request, *args, **kwargs):
        return Response({"message": "Not implemented yet"})

    @require_permissions(permission_list=[PermCode.CAN_APPROVE.value])
    @ensure_viewset_action_post_callback("approve")
    def approve_handler(self, request, *args, **kwargs):
        instance_ids = kwargs.get('ids', '')
        instance_ids = [int(id) for id in instance_ids.split(',')]
        instance_ids = list(set(instance_ids))
        instances = self.model.objects.filter(pk__in=instance_ids)
        self.instances = instances
        if len(instance_ids) == instances.count():
            # try:
            can_approve = False
            if request.user and request.user.is_hub_superadmin():
                can_approve = True
            else:
                all_instances_can_approve = []
                for instance in instances:
                    if instance.counter_organisation.domain_id == request.user.organisation.domain_id:
                        permissions = GroupRoleModelPermission.read_permissions(user=request.user,
                                                                                model_name=instance.__class__.__name__)
                        if PermCode.CAN_APPROVE.value in permissions:
                            all_instances_can_approve += [True]
                        else:
                            all_instances_can_approve += [False]
                    else:
                        all_instances_can_approve += [False]

                can_approve = all(all_instances_can_approve)

            if can_approve:
                with transaction.atomic():
                    for instance in instances:
                        if instance.approval_status != ApprovalStatusEnum.PENDING.value:
                            return Response(status=400, data={"detail": "Decision has already been taken!"})

                        instance.approve(request, **kwargs)
                return Response(status=200, data={"detail": "Approval processed successfully"})
            else:
                raise PermissionDenied("You do not have access to this action")
            #
            # except Exception as exp:
            #     return Response(status=400, data={"detail": "No all approval processed successfully"})
        else:
            self.instances = None
            return Response(status=400, data={"detail": "Not all ids provided are correct!"})

    @require_permissions(permission_list=[PermCode.CAN_REJECT.value])
    @ensure_viewset_action_post_callback("reject")
    def reject_handler(self, request, *args, **kwargs):
        instance_ids = kwargs.get('ids', '')
        instance_ids = [int(id) for id in instance_ids.split(',')]
        instance_ids = list(set(instance_ids))
        instances = self.model.objects.filter(pk__in=instance_ids)
        self.instances = instances
        if len(instance_ids) == instances.count():
            try:
                can_reject = False
                if request.user and request.user.is_hub_superadmin():
                    can_reject = True
                else:
                    all_instances_can_approve = []
                    for instance in instances:
                        if instance.counter_organisation.domain_id == request.user.organisation.domain_id:
                            permissions = GroupRoleModelPermission.read_permissions(user=request.user,
                                                                                    model_name=instance.__class__.__name__)
                            if PermCode.CAN_APPROVE.value in permissions:
                                all_instances_can_approve += [True]
                            else:
                                all_instances_can_approve += [False]
                        else:
                            all_instances_can_approve += [False]

                    can_reject = all(all_instances_can_approve)

                if can_reject:
                    with transaction.atomic():
                        for instance in instances:
                            if instance.approval_status != ApprovalStatusEnum.PENDING.value:
                                return Response(status=400, data={"detail": "Decision has already been taken!"})

                            instance.reject(request, **kwargs)
                    return Response(status=200, data={"detail": "Declined processed successfully"})
                else:
                    raise PermissionDenied("You do not have access to this action")

            except Exception as exp:
                return Response(status=400, data={"detail": "No all delination processed successfully"})
        else:
            self.instances = None
            return Response(status=400, data={"detail": "Not all ids provided are correct!"})

    @require_permissions(permission_list=[PermCode.CAN_APPROVE.value, PermCode.CAN_REJECT.value])
    @ensure_viewset_action_post_callback("mutate_action")
    def mutate(self, request, *args, **kwargs):
        return Response({"message": "Not implemented yet"})

