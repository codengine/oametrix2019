class ViewSerializerMixin(object):
    def get_serializer_class(self):
        action = self.action
        model = self.model
        serializer_class = self.serializer_dict.get(action, model.get_serializer())
        return serializer_class

