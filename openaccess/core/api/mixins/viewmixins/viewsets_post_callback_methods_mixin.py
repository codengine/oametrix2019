class ViewSetsPostCallBackMethodsMixin(object):

    @classmethod
    def on_post_upload(cls, request, user, model, view_ref, **kwargs):
        pass

    @classmethod
    def on_post_download(cls, request, user, model, view_ref, **kwargs):
        pass

    @classmethod
    def on_post_create(cls, request, user, model, view_ref, **kwargs):
        pass

    @classmethod
    def on_post_list(cls, request, user, model, view_ref, **kwargs):
        pass

    @classmethod
    def on_post_retrieve(cls, request, user, model, view_ref, **kwargs):
        pass

    @classmethod
    def on_post_update(cls, request, user, model, view_ref, **kwargs):
        pass

    @classmethod
    def on_post_delete(cls, request, user, model, view_ref, **kwargs):
        pass

    @classmethod
    def on_post_approve(cls, request, user, model, view_ref, **kwargs):
        pass

    @classmethod
    def on_post_reject(cls, request, user, model, view_ref, **kwargs):
        pass




