from enum import Enum


class PermCode(Enum):
    CAN_VIEW = "can_view"
    CAN_CREATE = "can_create"
    CAN_EDIT = "can_edit"
    CAN_DELETE = "can_delete"
    CAN_APPROVE = "can_approve"
    CAN_REJECT = "can_reject"
    CAN_UPLOAD = "can_upload"
    CAN_DOWNLOAD = "can_download"

    @classmethod
    def all_perm_codes(cls):
        return [
            cls.CAN_VIEW.value,
            cls.CAN_CREATE.value,
            cls.CAN_EDIT.value,
            cls.CAN_DELETE.value,
            cls.CAN_APPROVE.value,
            cls.CAN_REJECT.value,
            cls.CAN_UPLOAD.value,
            cls.CAN_DOWNLOAD.value,
        ]

    @classmethod
    def combine(cls, perm_codes=[]):
        p_codes = [pc for pc in perm_codes]
        return ",".join(p_codes)

    @classmethod
    def combinations(cls):
        p_combinations = []
        p_combinations += [{"View-Only": cls.combine([cls.CAN_VIEW.value])}]
        p_combinations += [{"View-Create": cls.combine([cls.CAN_VIEW.value, cls.CAN_CREATE.value])}]
        p_combinations += [{"View-Create-Edit": cls.combine([cls.CAN_VIEW.value, cls.CAN_CREATE.value, cls.CAN_EDIT.value])}]
        p_combinations += [{"View-Create-Edit-Delete": cls.combine([cls.CAN_VIEW.value, cls.CAN_CREATE.value, cls.CAN_EDIT.value, cls.CAN_DELETE.value])}]
        p_combinations += [{"View-Approve-Reject": cls.combine([cls.CAN_VIEW.value, cls.CAN_APPROVE.value, cls.CAN_REJECT.value])}]
        p_combinations += [{"View-Create-Upload": cls.combine([cls.CAN_VIEW.value, cls.CAN_CREATE.value, cls.CAN_UPLOAD.value])}]
        p_combinations += [{"View-Download": cls.combine([cls.CAN_VIEW.value, cls.CAN_DOWNLOAD.value])}]
        p_combinations += [{"All": cls.combine([cls.CAN_VIEW.value, cls.CAN_CREATE.value, cls.CAN_EDIT.value,
                                                cls.CAN_DELETE.value, cls.CAN_APPROVE.value, cls.CAN_REJECT.value,
                                                cls.CAN_UPLOAD.value, cls.CAN_DOWNLOAD.value])}]
        return p_combinations


