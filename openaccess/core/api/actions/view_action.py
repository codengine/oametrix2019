from enum import Enum


class ViewAction(Enum):
    CREATE = "create"
    UPDATE = "update"
    DELETE = "delete"
    UPLOAD = "upload"
    DOWNLOAD = "download"
    APPROVE = "approve"
    REJECT = "reject"
    CORRECTION_REQUEST = "correction_request"
    CORRECTION_REQUESTED = "correction_requested"
    CORRECTION_REQUEST_APPROVE = "correction_request_approve"
    CORRECTION_REQUEST_REJECT = "correction_request_reject"
    AUTHOR_APC_FUND_REQUEST = "author_apc_fund_request"
    NO_ACTION = "no_action"

