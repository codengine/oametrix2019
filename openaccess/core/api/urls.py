from django.conf.urls import url, include
from core.api.viewsets.generic_api_viewset import OpenAccessGenericAPIViewSet
from engine.library.app_util import get_all_models
from core.api.views.auth.api_login_view import ApiLoginView, Logout
from core.api.views.auth.api_registration_view import ApiRegistrationView, ActiveAccount, Docs, ChangePassword, \
    ValidateToken
from core.api.views.common.common import PublicOrganisationView, PublicRegistrationView, ForgetPasswordView, ResetPasswordView
app_models = get_all_models()

app_urls = []

for app, models in app_models.items():
    for model in models:
        generate_auto_urls = model.generate_auto_urls()
        if generate_auto_urls:
            route_name = model.get_route_name()
            auto_urls = model.generate_auto_urls()
            if type(auto_urls) is not list:
                raise Exception("generate_auto_urls must return list. Error in model: %s" % model.__name__)

            auto_url_dict = {}
            for path_details in auto_urls:
                if path_details["path"] in auto_url_dict:
                    auto_url_dict[path_details["path"]] += [path_details]
                else:
                    auto_url_dict[path_details["path"]] = [path_details]

            for path, path_detail_list in auto_url_dict.items():
                serializer_dict = {}
                allowed_action_mapping = {}
                http_method_names = []
                object_retrieve_method_mapping = {}
                url_name = None
                for path_detail in path_detail_list:
                    serializer_class = path_detail.get('serializer_class', model.get_serializer())
                    serializer_dict[path_detail['action_handler']] = serializer_class
                    allowed_action_mapping[path_detail['method'].lower()] = path_detail['action_handler']
                    http_method_names += [path_detail['method'].lower()]
                    get_object_method = path_detail.get('get_object_method', model.handle_object_retrieve)
                    object_retrieve_method_mapping[path_detail['action_handler']] = get_object_method
                    if not url_name:
                        url_name = model.get_route_url_name() + "_" + path_detail["action_handler"]
                http_method_names = tuple(http_method_names)
                OverrideViewClass = model.override_viewset()
                if OverrideViewClass:
                    viewset_subclass = type(
                        'RunTime_' + OverrideViewClass.__name__ + '_ViewSet',
                        (OverrideViewClass,),
                        dict(
                            model=model,
                            queryset=model.objects.all(),
                            serializer_dict=serializer_dict,
                            http_method_names=http_method_names,
                            object_retrieve_method_mapping=object_retrieve_method_mapping
                        )
                    )
                else:
                    viewset_subclass = type(
                        'RunTime_' + model.__name__ + '_ViewSet',
                        (OpenAccessGenericAPIViewSet,),
                        dict(
                            model=model,
                            queryset=model.objects.all(),
                            serializer_dict=serializer_dict,
                            http_method_names=http_method_names,
                            object_retrieve_method_mapping=object_retrieve_method_mapping
                        )
                    )

                app_url = url(r'^%s/$' % (path,),
                              viewset_subclass.as_view(allowed_action_mapping),
                              name=url_name)
                app_urls += [app_url]

urlpatterns = [
    url(r'^login/$', ApiLoginView.as_view(), name="oa_api_login"),
    url(r'^registration/$', ApiRegistrationView.as_view(), name="oa_api_registration"),
    url(r'^active-account/$', ActiveAccount.as_view(), name="oa_active_account"),
    url(r'^change-password/$', ChangePassword.as_view(), name="oa_change_password"),
    url(r'^logout', view=Logout.as_view(), name='oa_logout'),
    url(r'^docs', view=Docs.as_view(), name='oa_guide'),
    url(r'^validate-token', view=ValidateToken.as_view(), name='validate_token'),
    url(r'^public-organisation-info/$', PublicOrganisationView.as_view({'get': 'list'}), name="oa_pub_organisation"),
    url(r'^public-registration/$', PublicRegistrationView.as_view(), name="oa_pub_registration"),
    url(r'^forget-password', ForgetPasswordView.as_view(), name='forget_password'),
    url(r'^reset-password', ResetPasswordView.as_view(), name='reset_password'),

]

urlpatterns += app_urls
