from core.api.viewsets.generic_api_viewset import OpenAccessGenericAPIViewSet
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.mixins import ListModelMixin


class BaseListAPIView(OpenAccessGenericAPIViewSet, ListModelMixin):
    http_method_names = ["get"]

    @method_decorator(csrf_exempt)
    def get(self, request, **kwargs):
        return super(BaseListAPIView, self).get(request, **kwargs)