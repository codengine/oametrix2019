from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from django.conf import settings
from django.http import HttpResponse
from rest_framework.response import Response
from core.api.views.base_api_view import BaseAPIView
from core.models.auth.user import User, AccountInfo
from core.api.serializers.auth.open_access_registration_serializer import OpenAccessRegistrationSerializer
from django.db import transaction
from datetime import datetime
import pytz
# from rest_framework.throttling import AnonRateThrottle
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.authtoken.models import Token


__author__ = 'expo_ashiq'


class ApiRegistrationView(BaseAPIView):
    serializer_class = OpenAccessRegistrationSerializer
    http_method_names = ['post']
    # throttle_classes = (AnonRateThrottle,)
    permission_classes = (AllowAny,)


    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    @csrf_exempt
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()

            user = User.objects.get(pk=serializer.data['id'])
            response = {"user": user.as_json(),
                        'success': True}
            return Response(response, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ActiveAccount(BaseAPIView):
    # throttle_classes = (AnonRateThrottle,)
    permission_classes = (AllowAny,)

    @csrf_exempt
    def post(self, request, format=None):
        security_code = request.data.get('security_code')
        account_info = AccountInfo.objects.filter(security_code=security_code).first()

        if account_info is not None:

            dt = datetime.utcnow()
            present = dt.replace(tzinfo=pytz.utc)

            if account_info.expire_date < present:
                # need not to store expired data.
                account_info.delete()
                response = {
                    "message": "Session expired.",
                    "success": False
                }
                return Response(response, status=status.HTTP_400_BAD_REQUEST)

            user = account_info.user
            if user.is_active is not False:
                return Response(status=status.HTTP_400_BAD_REQUEST)

            with transaction.atomic():
                user.is_active = True
                user.save()
                token, created = Token.objects.get_or_create(user=user)
                account_info.delete()

            response = {
                "message": "Account successfully activated.",
                'token': token.key,
                "success": True
            }
            return Response(response, status=status.HTTP_200_OK)
        else:
            response = {
                "message": "Invalid security code or Session expired.",
                "success": False
            }
            return Response(response, status=status.HTTP_400_BAD_REQUEST)



class Docs(BaseAPIView):
    permission_classes = (AllowAny,)
    def get(self, request, format=None):
        keyword = request.GET.get('q')

        response = "No Guide Found"

        if keyword == 'user':
            file_name = settings.BASE_DIR+'/openaccess/user_guide/user.md'
        if keyword == 'group':
            file_name = settings.BASE_DIR + '/openaccess/user_guide/group.md'
        if keyword == 'role':
            file_name = settings.BASE_DIR + '/openaccess/user_guide/role.md'

        handle = open(file_name, 'r', encoding="utf-8")
        content = handle.read()

        response = """
                        <html>
                        <head>
                        <title></title>
                        </head>  
                        <body>
                        <xmp>""" + content + """</xmp>
                        </body>
    
                        </html>
                    """

        return HttpResponse(response)


class ChangePassword(BaseAPIView):
    serializer_class = OpenAccessRegistrationSerializer
    http_method_names = ['post']
    permission_classes = (IsAuthenticated,)


    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    @csrf_exempt
    def post(self, request):
        user = request.user
        new_password = request.data.get('new_password')

        if new_password is None:
            return Response({'new_password': 'This field is required'}, status=status.HTTP_400_BAD_REQUEST)


        if new_password.strip() =="":
            return Response({'error':'Enter a valid password'}, status=status.HTTP_400_BAD_REQUEST)
        user.set_password(new_password)
        user.save()
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key, "user": user.as_json(), 'success': True} , status=status.HTTP_200_OK)


class ValidateToken(BaseAPIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        if request.user.id is not None:
            return Response({'success': True}, status=status.HTTP_200_OK)
        else:
            return Response({'success': False}, status=status.HTTP_400_BAD_REQUEST)

