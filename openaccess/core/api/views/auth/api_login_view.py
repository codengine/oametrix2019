from django.db import transaction
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response
from core.api.views.base_api_view import BaseAPIView
from core.models.auth.user import User
from core.api.serializers.auth.open_access_auth_token_serializer import OpenAccessAuthTokenSerializer
# from rest_framework.throttling import AnonRateThrottle
from rest_framework.permissions import AllowAny, IsAuthenticated

__author__ = 'Sohel'


class ApiLoginView(ObtainAuthToken):
    serializer_class = OpenAccessAuthTokenSerializer
    http_method_names = ['post']
    # throttle_classes = (AnonRateThrottle,)
    permission_classes = (AllowAny,)

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    @csrf_exempt
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            user = User.objects.filter(pk=serializer.validated_data['user'].id, is_active=True).first()

            if user is None:
                return Response(status=status.HTTP_400_BAD_REQUEST)

            with transaction.atomic():
                user.save()
                token, created = Token.objects.get_or_create(user=serializer.validated_data['user'])
                return Response({'token': token.key, "user": user.as_json(), 'success': True}, status=status.HTTP_200_OK)
        return Response({'message': 'Cannot login with provided credentials.', 'success': False},
                        status=status.HTTP_400_BAD_REQUEST)

class Logout(BaseAPIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request, format=None):

        request.user.auth_token.delete()
        return Response(status=status.HTTP_200_OK)
