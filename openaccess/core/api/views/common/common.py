from rest_framework.permissions import AllowAny, IsAuthenticated

from core.models.auth.role import Role
from core.models.organisation.organisation import Organisation
from rest_framework import viewsets
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from core.models.auth.user import User, AccountInfo
from core.api.views.base_api_view import BaseAPIView
from django.db import transaction, IntegrityError
from django.contrib.auth import authenticate
from engine.library.app_util import Mailer
from core.models.mail.email_base import EmailBase
from django.conf import settings
import uuid
from rest_framework import status
from core.api.serializers.auth.open_access_registration_serializer import OpenAccessRegistrationSerializer


class PublicOrganisationView(viewsets.ModelViewSet):
    serializer_class = Organisation.get_serializer()
    queryset = Organisation.objects.all()
    permission_classes = (AllowAny,)


class PublicRegistrationView(BaseAPIView):
    serializer_class = OpenAccessRegistrationSerializer
    http_method_names = ['post']
    permission_classes = (AllowAny,)

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    @csrf_exempt
    def post(self, request):
        user_role = Role.objects.filter(name='User').first()
        request.data['role'] = user_role.pk  # default user

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()

            user = User.objects.get(pk=serializer.data['id'])
            response = {"user": user.as_json(),
                        'success': True}
            return Response(response, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ForgetPasswordView(BaseAPIView):
    permission_classes = ()

    def post(self, request, format=None):
        email = request.data.get("email")
        user = User.objects.filter(email=email).first()

        if user is not None and user.is_active:
            security_code = str(uuid.uuid4())

            with transaction.atomic():
                info = AccountInfo.objects.get(user_id=user.id)
                info.security_code = security_code
                info.save()

            # Sending email start
            email_body = EmailBase.objects.filter(type='reset password').first().email_body
            email_body += settings.RESET_PASSWORD_URL + str(info.security_code)

            mailer = Mailer()
            response = mailer.send_email(recipient=email, message=email_body)
            if response is False:
                transaction.set_rollback(True)
                return Response({"error": "Email sending process failed."},
                                status.HTTP_500_INTERNAL_SERVER_ERROR)

            # Sending email end

            response = {'success': True}
            return Response(response, status=status.HTTP_200_OK)

        return Response(status=status.HTTP_409_CONFLICT)


class ResetPasswordView(BaseAPIView):
    permission_classes = ()
    authentication_classes = ()

    @transaction.atomic
    def post(self, request, format=None):
        security_code = request.data.get("security_code")
        new_password = request.data.get("new_password")

        # check security code from profile model
        info = AccountInfo.objects.filter(security_code=security_code).first()
        if info is not None and info.user.is_active:
            # get user data to update password from user id
            user = info.user
            user.set_password(raw_password=new_password)
            user.save()

            db_transaction = transaction.savepoint()

            info.security_code = None
            info.save()

            try:
                # open transaction still contains user.save() and userinfo.save()
                transaction.savepoint_commit(db_transaction)

            except IntegrityError:
                transaction.savepoint_rollback(db_transaction)

            response = {'success': True}

            return Response(response, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)
