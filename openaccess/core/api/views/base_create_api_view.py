from rest_framework import generics
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt


class BaseCreateAPIView(generics.CreateAPIView):
    @method_decorator(csrf_exempt)
    def post(self, request, **kwargs):
        return super(BaseCreateAPIView, self).post(request, **kwargs)
