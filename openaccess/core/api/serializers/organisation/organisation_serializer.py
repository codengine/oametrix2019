from core.api.serializers.base_model_serializer import BaseModelSerializer
from core.models.organisation.organisation import Organisation


class OrganisationModelSerializer(BaseModelSerializer):
    class Meta:
        model = Organisation
        fields = ('name', )
        exclude = ('id', )