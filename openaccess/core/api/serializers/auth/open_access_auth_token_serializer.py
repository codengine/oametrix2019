from rest_framework import serializers
from rest_framework.authtoken.serializers import AuthTokenSerializer

__author__ = 'Sohel'


class OpenAccessAuthTokenSerializer(AuthTokenSerializer):
    pass

