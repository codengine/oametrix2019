from rest_framework import serializers
from django.db import transaction
from rest_framework import status
from django.conf import settings
from core.models.auth.user import User, AccountInfo
from core.models.organisation.organisation import Organisation
from engine.library.app_util import Mailer
from core.models.mail.email_base import EmailBase
from core.models.contacts.user_domain_meta import UserDomainMeta
from core.models.contacts.address import Address
from core.models.contacts.city import City
from core.models.contacts.country import Country
from core.models.contacts.state import State
import uuid

__author__ = 'expo_ashiq'


class OpenAccessRegistrationSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id', 'username', 'email', 'password', 'group', 'role', 'organisation', 'salute', 'first_name',
            'middle_name', 'last_name',
            'multi_factor_enabled', 'domain_meta', 'user_meta', 'addresses', 'unique_id')

        extra_kwargs = {
            'password': {'write_only': True}
        }

    @transaction.atomic
    def create(self, validated_data):
        username = validated_data.get('username')
        email = validated_data.get('email')
        password = validated_data.get('password')
        group = validated_data.get('group')
        role = validated_data.get('role')
        organisation = validated_data.get('organisation')
        salute = validated_data.get('salute')
        first_name = validated_data.get('first_name')
        middle_name = validated_data.get('middle_name')
        last_name = validated_data.get('last_name')

        unique_id = str(uuid.uuid4().hex)[:10]

        user = User.objects.create_user(username=username, email=email, password=password, group=group, role=role,
                                        organisation=organisation, salute=salute, first_name=first_name,
                                        middle_name=middle_name, last_name=last_name, unique_id=unique_id)

        with transaction.atomic():
            account_info = AccountInfo(user=user)
            account_info.save()

        # Sending email start
        email_body = EmailBase.objects.filter(type='registration').first().email_body
        email_body += settings.ACCOUNT_ACTIVATION_URL + str(account_info.security_code)

        mailer = Mailer()
        response = mailer.send_email(recipient=user.email, message=email_body)
        if response is False:
            transaction.set_rollback(True)
            raise serializers.ValidationError({"error": "Email sending process failed."},
                                              status.HTTP_500_INTERNAL_SERVER_ERROR)

        # Sending email end

        return user

    def is_valid(self, raise_exception=False):
        organisation_id = self.initial_data.get('organisation', -1)
        organisation_instance = None
        try:
            organisation_id = int(organisation_id)
            organisation_instance = Organisation.objects.get(pk=organisation_id)
        except Exception as exp:
            raise serializers.ValidationError({"organisation": "This field can't be empty"},
                                              status.HTTP_400_BAD_REQUEST)
        self.initial_data['group'] = organisation_instance.domain_id
        return super(OpenAccessRegistrationSerializer, self).is_valid(raise_exception=raise_exception)

    def validate(self, data):
        email = data.get('email', None)
        organisation = data.get('organisation', None)
        if not email:
            raise serializers.ValidationError({"email": "This field can't be empty"},
                                              status.HTTP_400_BAD_REQUEST)
        if organisation is None:
            raise serializers.ValidationError({"organisation": "This field can't be empty"},
                                              status.HTTP_400_BAD_REQUEST)

        splited_data = email.split('@')

        if splited_data[1].strip() != organisation.email_domain.strip():
            raise serializers.ValidationError({"error": "Email domain doesn't match."},
                                              status.HTTP_400_BAD_REQUEST)
        data = super(OpenAccessRegistrationSerializer, self).validate(data)
        return data



class StateSerializer(serializers.ModelSerializer):
    class Meta:
        model = State
        fields = '__all__'


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = '__all__'


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'


class AddressSerializer(serializers.ModelSerializer):
    city = CitySerializer(required=False)
    state = StateSerializer(required=False)
    country = CountrySerializer(required=False)

    class Meta:
        model = Address
        fields = ('title', 'address1', 'address2', 'city', 'state', 'country', 'postal_code')


class UserCreateSerializer(serializers.ModelSerializer):
    addresses = AddressSerializer(required=False, many=True)

    class Meta:
        model = User
        fields = (
            'id', 'username', 'email', 'password', 'group', 'role', 'organisation', 'salute', 'first_name',
            'middle_name', 'last_name',
            'multi_factor_enabled', 'domain_meta', 'user_meta', 'addresses')

        extra_kwargs = {
            'password': {'write_only': True}
        }

    @transaction.atomic
    def create(self, validated_data):
        username = validated_data.get('username')
        email = validated_data.get('email')
        password = validated_data.get('password')
        group = validated_data.get('group')
        role = validated_data.get('role')
        organisation = validated_data.get('organisation')
        salute = validated_data.get('salute')
        first_name = validated_data.get('first_name')
        middle_name = validated_data.get('middle_name')
        last_name = validated_data.get('last_name')
        segment = validated_data.get('segment')
        domain_meta_title = validated_data.get('title')
        domain_meta_position = validated_data.get('position')
        domain_meta_home_tel = validated_data.get('home_tel')
        domain_meta_office_tel = validated_data.get('office_tel')
        domain_meta_alt_tel = validated_data.get('alt_tel')
        domain_meta_addresses = validated_data.get('addresses')

        user = User.objects.create_user(username=username, email=email, password=password, group=group, role=role,
                                        organisation=organisation, salute=salute, first_name=first_name,
                                        middle_name=middle_name, last_name=last_name)

        user.is_active = True

        # creating UserDomainMeta
        user_domain_meta = UserDomainMeta(segment=segment, title=domain_meta_title, position=domain_meta_position,
                                          home_tel=domain_meta_home_tel, office_tel=domain_meta_office_tel,
                                          alt_tel=domain_meta_alt_tel)
        user_domain_meta.save()

        user.domain_meta = user_domain_meta
        user.save()

        return user
