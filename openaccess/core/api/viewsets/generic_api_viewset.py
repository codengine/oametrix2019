from django.http.response import Http404

from core.api.actions.view_action import ViewAction
from core.api.constants.permission_code import PermCode
from core.api.mixins.viewmixins.detail_view_serializer_mixin import DetailViewSerializerMixin
from core.api.mixins.viewmixins.protected_view_mixin import ProtectedViewMixin
from core.api.mixins.viewmixins.view_action_handler_mixin import ViewActionHandlerMixin
from core.api.mixins.viewmixins.view_serializer_mixin import ViewSerializerMixin
from core.api.viewsets.base_model_viewset import BaseModelViewSet
from core.models.auth.group_role_model_permission import GroupRoleModelPermission


class OpenAccessGenericAPIViewSet(ProtectedViewMixin, ViewActionHandlerMixin,
                                  ViewSerializerMixin, DetailViewSerializerMixin,
                                  BaseModelViewSet):
    def render_actions(self, *args, **kwargs):

        view_actions = [
            ViewAction.CREATE.value,
            ViewAction.UPDATE.value,
            ViewAction.DELETE.value,
            ViewAction.UPLOAD.value,
            ViewAction.DOWNLOAD.value,
            ViewAction.APPROVE.value,
            ViewAction.REJECT.value
        ]

        actions = []

        view_action_label_mapping = self.model.map_view_action_to_label() or {}

        permissions = GroupRoleModelPermission.read_permissions(user=self.request.user, model_name=self.model.__name__)

        """
            {
                "label": "create",
                "request_method": "POST",
                "endpoint": "/endpoint",
                "parameter_required": True,
                "parameter_name": "body",
                "description": "The request body should contain the json body"
            },
            {
                "label": "Approve",
                "request_method": "PUT",
                "endpoint": "/endpoint",
                "parameter_required": True,
                "parameter_name": "id",
                "description": "Get parameter can be like ?id=1 or ?id=1,2,3 etc"
            }
        """

        for view_action in view_actions:
            if view_action == ViewAction.CREATE.value:
                action_label = view_action_label_mapping.get(view_action, view_action.capitalize())
                if PermCode.CAN_VIEW.value in permissions:
                    actions += [
                        {
                            "label": action_label,
                            "action": view_action,
                            "request_method": "POST"
                        }
                    ]
            elif view_action == ViewAction.UPDATE.value:
                action_label = view_action_label_mapping.get(view_action, view_action.capitalize())
                if PermCode.CAN_EDIT.value in permissions:
                    actions += [
                        {
                            "label": action_label,
                            "action": view_action,
                            "request_method": "PUT"
                        }
                    ]
            elif view_action == ViewAction.DELETE.value:
                action_label = view_action_label_mapping.get(view_action, view_action.capitalize())
                if PermCode.CAN_DELETE.value in permissions:
                    actions += [
                        {
                            "label": action_label,
                            "action": view_action,
                            "request_method": "DELETE"
                        }
                    ]
            elif view_action == ViewAction.UPLOAD.value:
                action_label = view_action_label_mapping.get(view_action, view_action.capitalize())
                if PermCode.CAN_UPLOAD.value in permissions:
                    actions += [
                        {
                            "label": action_label,
                            "action": view_action,
                            "request_method": "POST"
                        }
                    ]
            elif view_action == ViewAction.DOWNLOAD.value:
                action_label = view_action_label_mapping.get(view_action, view_action.capitalize())
                if PermCode.CAN_DOWNLOAD.value in permissions:
                    actions += [
                        {
                            "label": action_label,
                            "action": view_action,
                            "request_method": "GET"
                        }
                    ]

        approval_enabled_method_found = hasattr(self.model, 'approval_enabled') or False
        if approval_enabled_method_found:
            approval_enabled = getattr(self.model, 'approval_enabled')()
            if approval_enabled:
                workflow_actions = self.model.render_workflow_actions()
                if workflow_actions:
                    actions += workflow_actions

        return actions





















