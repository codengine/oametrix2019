from core.api.serializers.organisation.organisation_serializer import OrganisationModelSerializer
from core.api.views.base_list_api_view import BaseListAPIView
from core.models.organisation.organisation import Organisation


class OrganisationAPIView(BaseListAPIView):
    queryset = Organisation.objects.all()
    serializer_class = OrganisationModelSerializer
