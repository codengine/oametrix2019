from core.models.auth.user import User
from rest_framework import exceptions
from rest_framework.authentication import TokenAuthentication, get_authorization_header
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import APIException


__author__ = 'Sohel'


class OpenAccessTokenAuthentication(TokenAuthentication):
    def authenticate(self, request):
        auth = get_authorization_header(request).split()

        if not auth or len(auth) == 0 or auth[0].lower() != b'token':
            return None

        if len(auth) <= 1:
            msg = 'Invalid token header. No credentials provided.'
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = 'Invalid token header. Token string should not contain spaces.'
            raise exceptions.AuthenticationFailed(msg)

        try:
            auth_user, token = self.authenticate_credentials(auth[1].decode())
            user_instance = User.objects.get(pk=auth_user.pk)
        except Exception as exp:
            raise APIException(str(exp))
        return (auth_user, token)