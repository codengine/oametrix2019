import threading
from django_global_request.middleware import get_request
from rest_framework.permissions import BasePermission
from core.models.auth.group_role_model_permission import GroupRoleModelPermission

__author__ = 'Sohel'


class IsAuthorized(BasePermission):
    def has_permission(self, request, view):
        # Check the permission now
        model = view.queryset.model
        auth_required = model.require_auth()
        if not auth_required:
            return True
        # Check model permission
        return request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        return False

