import threading
from django_global_request.middleware import get_request
from rest_framework.permissions import BasePermission
from core.models.auth.group_role_model_permission import GroupRoleModelPermission

__author__ = 'Sohel'


class IsAuthorizedDashboard(BasePermission):
    def has_permission(self, request, view):
        # Check the permission now
        if request.user and request.user.is_authenticated:
            if request.user.is_hub_user():
                return True
            elif request.user.is_publisher_user():
                return True
            elif request.user.is_author_user():
                return False
            else:
                return True
        return False

    def has_object_permission(self, request, view, obj):
        return False

