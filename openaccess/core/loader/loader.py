import inspect
from django.apps import apps
from pydoc import locate


def get_app_name(app_name, include_class=False):
    type_ = locate(app_name)
    if inspect.isclass(type_):
        return type_.name
    if not include_class:
        return app_name, None
    else:
        return app_name, type_


def load_model(app_name, model_name):
    try:
        model = apps.get_model(app_name, model_name)
        return model
    except Exception as exp:
        return None

