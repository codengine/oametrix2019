import logging
from django.core.management.base import BaseCommand
from core.management.commands.utils.init_articles import init_content_types
from core.management.commands.utils.init_data_util import init_oa_deal_types, init_currencies
from core.management.commands.utils.init_email_templates import init_email_templates
from core.management.commands.utils.init_journals import init_journal_types
from core.management.commands.utils.init_locations import init_locations
from core.management.commands.utils.init_organisations import init_organisations
from core.management.commands.utils.init_permissions import init_permissions
from core.management.commands.utils.init_publications import init_publication_types
from core.management.commands.utils.init_roles import init_roles
from core.management.commands.utils.init_users import init_users
from core.management.commands.utils.init_fund_source import init_fund_source
from core.management.commands.utils.init_apc_options import init_apc_options

from core.management.commands.utils.init_licences import init_licence

__author__ = 'Sohel'

logger = logging.getLogger('core')


class Command(BaseCommand):
    def handle(self, *args, **options):
        logger.info("Running init data")

        init_locations()

        logger.info("Creating Organisations...")

        init_organisations()

        logger.info("Organisation Created.")

        logger.info("Creating roles")

        init_roles()

        logger.info("Roles created")

        logger.info("Creating Journal type ")

        init_journal_types()

        logger.info("Journal type  Created")

        logger.info("Creating Publication type ")

        init_publication_types()

        logger.info("Creating ContentType ")

        init_content_types()

        logger.info("ContentType Created")

        # =========================== Init currencies ===================================#
        init_currencies()
        # =========================== End Init currencies ===================================#

        # =========================== Init OA Deal Types ===================================#
        init_oa_deal_types()
        # =========================== End Init OA Deal Types ===================================#

        init_users()

        init_permissions()

        init_email_templates()

        init_licence()

        init_fund_source()

        init_apc_options()



