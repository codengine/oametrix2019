import logging
from django.db import transaction
from openaccess.models.policy.fund_source import APCFundSource

logger = logging.getLogger('core')

def init_fund_source():
    fund_sources = ["Department", "Funder", "Author","Publisher","Other"]
    with transaction.atomic():
        for fund_source in fund_sources:
            logger.info("Creating fund source")
            instance = APCFundSource.create_of_update(name=fund_source)