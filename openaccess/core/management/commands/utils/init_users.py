import logging
from core.models.auth.group import Group
from core.models.auth.role import Role
from core.models.auth.user import User
from core.models.organisation.organisation import Organisation

logger = logging.getLogger('core')


def init_hub_superadmin():

    super_admin_instance = Role.objects.filter(name='SuperAdmin').first()
    hub_group_instance = Group.objects.filter(name='Hub').first()
    hub_org_instance = Organisation.objects.filter(name='Hub Organisation').first()

    username = "monarch"
    email = "monadmin@gmail.com"
    user_instances = User.objects.filter(username=username, email=email)
    if user_instances.exists():
        user_instance = user_instances.first()
    else:
        user_instance = User()
    user_instance.username = username
    user_instance.email = email
    user_instance.set_password("2019")
    user_instance.salute = "Mr"
    user_instance.first_name = "Mon"
    user_instance.middle_name = "Admin"
    user_instance.last_name = "Mon"
    user_instance.is_active = True
    user_instance.group_id = hub_group_instance.pk
    user_instance.role_id = super_admin_instance.pk
    user_instance.organisation_id = hub_org_instance.pk
    user_instance.save()


def init_users():
    logger.info("Creating Hub Super admin")

    init_hub_superadmin()

    logger.info("Hub Superadmin created")
