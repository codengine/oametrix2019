import logging
import os
from core.models.contacts.city import City
from core.models.contacts.country import Country
from core.models.contacts.state import State

logger = logging.getLogger('core')

def init_locations():
    project_directory = os.path.abspath(".")
    data_directory = os.path.join(project_directory, "core", "data")

    logger.info("Initializing Countries")

    country_data_file = os.path.join(data_directory, "countries.json")
    Country.load_from_json(country_data_file)

    logger.info("Country initialized")

    logger.info("Initializing State and City")
    countries = Country.objects.all()[:3]

    for country in countries:
        for i in range(3):
            state_name = country.name + ' state' + str(i)
            state_instances = State.objects.filter(name=state_name)
            if state_instances.exists():
                state = state_instances.first()
            else:
                state = State()
            state.name = state_name
            state.short_name = state_name[4:7]
            state.is_active = True
            if i > 0:
                state.parent_id = i
            state.save()

    states = State.objects.all()[:3]

    for state in states:
        for j in range(3):
            city_name = state.name + ' city' + str(j)
            city_instances = City.objects.filter(name=city_name)
            if city_instances.exists():
                city = city_instances.first()
            else:
                city = City()
            city.name = city_name
            city.short_name = city_name[4:]
            city.is_active = True
            if j > 0:
                city.parent_id = j
            city.save()

    logger.info("State and City Initialized")