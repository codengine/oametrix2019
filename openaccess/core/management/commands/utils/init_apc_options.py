import logging
from django.db import transaction
from django.db.models.query_utils import Q
from core.constants.apc_option_enum import APCOptionEnum
from openaccess.models.policy.apc_option import APCOption

logger = logging.getLogger('core')


def init_apc_options():
    apc_options = [APCOptionEnum.DEPOSIT_CREDIT.value, APCOptionEnum.OFFSET_FUND.value, APCOptionEnum.OATOKEN.value]
    with transaction.atomic():
        for apc_option in apc_options:
            logger.info("Creating apc option")
            instance = APCOption.create_of_update(name=apc_option)