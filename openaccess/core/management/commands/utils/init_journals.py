import logging
from django.db import transaction
from django.db.models.query_utils import Q

from openaccess.models.publication.journal_type import JournalType

logger = logging.getLogger('core')

def init_journal_types():
    journal_types = ["Gold", "Hybrid", "Platinum"]
    j_include_ids = []
    logger.info("Creating roles")
    with transaction.atomic():
        for type_name in journal_types:
            logger.info("Creating Journal type : %s" % type_name)
            instance = JournalType.create_of_update(name=type_name)
            j_include_ids += [instance.pk]

        JournalType.objects.filter(~Q(pk__in=j_include_ids)).delete()
