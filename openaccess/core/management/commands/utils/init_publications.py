import logging
from django.db import transaction
from django.db.models.query_utils import Q

from openaccess.models.publication.publication_type import PublicationType

logger = logging.getLogger('core')


def init_publication_types():
    publication_types = ["Book", "Journal"]
    pt_include_ids = []
    logger.info("Creating publication types")
    with transaction.atomic():
        for type_name in publication_types:
            logger.info("Creating publication type : %s" % type_name)
            instance = PublicationType.create_of_update(name=type_name)
            pt_include_ids += [instance.pk]

        PublicationType.objects.filter(~Q(pk__in=pt_include_ids)).delete()

    logger.info("Publication type  Created")
