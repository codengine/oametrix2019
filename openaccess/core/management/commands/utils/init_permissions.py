import logging
from django.db.models.query_utils import Q
from core.api.constants.permission_code import PermCode
from core.models.auth.group import Group
from core.models.auth.group_role_model_permission import GroupRoleModelPermission
from core.models.auth.oapermission import OAPermission
from core.models.auth.role import Role
from engine.library.app_util import get_all_models

logger = logging.getLogger('core')


def init_permissions():
    include_ids = []
    perm_combinations = PermCode.combinations()

    # [{'View-Only': 'can_view'}, {'View-Create': 'can_view,can_create'},
    #  {'View-Create-Edit': 'can_view,can_create,can_edit'},
    #  {'View-Create-Edit-Delete': 'can_view,can_create,can_edit,can_delete'},
    #  {'View-Approve-Reject': 'can_view,can_approve,can_reject'},
    #  {'View-Create-Upload': 'can_view,can_create,can_upload'}, {'View-Download': 'can_view,can_download'},
    #  {'All': 'can_view,can_create,can_edit,can_delete,can_approve,can_reject,can_upload,can_download'}]

    for perm_combination in perm_combinations:
        for perm_name, perm_code in perm_combination.items():
            oa_perm_instances = OAPermission.objects.filter(name=perm_name)
            if oa_perm_instances.exists():
                oa_perm_instance = oa_perm_instances.first()
            else:
                oa_perm_instance = OAPermission(name=perm_name)

            oa_perm_instance.perm_code = perm_code
            oa_perm_instance.save()
            include_ids += [oa_perm_instance.pk]

    OAPermission.objects.filter(~Q(pk__in=include_ids)).delete()

    logger.info("Creating group role model permission")

    logger.info("Setting permission for Hub group")
    # group_instance
    logger.info("Setting permission Hub -> SuperAdmin role")
    app_models = get_all_models()

    super_admin_instance = Role.objects.filter(name='SuperAdmin').first()

    group_instance = Group.objects.get(name='Hub')
    permission = OAPermission.objects.get(name='All')
    for app, models in app_models.items():
        for model in models:
            model_name = model.__name__
            logger.info("%s for group: %s, role: %s, model: %s" % (permission.name, group_instance.name,
                                                                   super_admin_instance.name, model_name))
            GroupRoleModelPermission.create_or_update(group_id=group_instance.pk,
                                                      role_id=super_admin_instance.pk,
                                                      model_name=model_name,
                                                      permission=permission)
    logger.info("Permission configured")

    logger.info("Creating Permission for Publisher Admin")

    hub_instance = Group.objects.get(name='Hub')
    publisher_instance = Group.objects.get(name='Publisher')
    admin_role_instance = Role.objects.get(name='Admin')
    user_role_instance = Role.objects.get(name='User')
    view_approve_reject_permission = OAPermission.objects.get(name='View-Approve-Reject')
    view_only_permission = OAPermission.objects.get(name='View-Only')
    view_create_edit_delete_permission = OAPermission.objects.get(name='View-Create-Edit-Delete')

    ############################################# ============================== #####################################################
    ############################################# University Configuration start  #####################################################
    ############################################# ============================== #####################################################

    logger.info("Creating Permission for University Admin")

    university_instance = Group.objects.get(name='University')
    uni_admin_view_per_models = ['ArticleFull', 'Organisation', 'DealType', 'Country', 'Currency', 'Licence',
                                 'APCFundSource', 'APCOption']

    for model in uni_admin_view_per_models:
        GroupRoleModelPermission.create_or_update(group_id=university_instance.pk,
                                                  role_id=admin_role_instance.pk,
                                                  model_name=model,
                                                  permission=view_only_permission)

    uni_admin_view_create_edit_delete_permission_model = ['OADeal', 'DepositRequest', 'OAToken', 'OrganisationToken',
                                                          'OffsetFund',
                                                          'OrganisationOffsetFund', 'CorrectionRequest',
                                                          'ArticleApprove', 'ArticleReject',
                                                          'ArticleApproveTransactionBreakdown','CorrectionRequest']
    for model in uni_admin_view_create_edit_delete_permission_model:
        GroupRoleModelPermission.create_or_update(group_id=university_instance.pk,
                                                  role_id=admin_role_instance.pk,
                                                  model_name=model,
                                                  permission=view_create_edit_delete_permission)

    logger.info("Permission for University Admin configured")

    ############################################# ============================== #####################################################
    ############################################# University Configuration end  #####################################################
    ############################################# ============================== #####################################################

    ############################################# ============================== #####################################################
    ############################################# Publisher Configuration start  #####################################################
    ############################################# ============================== #####################################################

    logger.info("Creating Permission for Publisher Admin")

    pub_admin_view_per_models = ['ArticleFull', 'DealType', 'Currency', 'Country', 'Licence', 'APCFundSource',
                                 'APCOption','CorrectionRequest']

    for model in pub_admin_view_per_models:
        GroupRoleModelPermission.create_or_update(group_id=publisher_instance.pk,
                                                  role_id=admin_role_instance.pk,
                                                  model_name=model,
                                                  permission=view_only_permission)

    view_create_edit_delete_permission_models = ['OADeal', 'Organisation', 'OAToken', 'OrganisationToken', 'OffsetFund',
                                                 'OrganisationOffsetFund','Publication', 'JournalType', 'PublicationType',
                                                 'User', 'State', 'City', 'Role']

    for model in view_create_edit_delete_permission_models:
        GroupRoleModelPermission.create_or_update(group_id=publisher_instance.pk,
                                                  role_id=admin_role_instance.pk,
                                                  model_name=model,
                                                  permission=view_create_edit_delete_permission)

    logger.info("Permission for Publisher Admin configured")

    ############################################# ============================== #####################################################
    ############################################# Publisher Configuration end  #####################################################
    ############################################# ============================== #####################################################

    ############################################# ============================== #####################################################
    ############################################# Hub Configuration start  #####################################################
    ############################################# ============================== #####################################################

    logger.info("Creating Permission for Hub USER")
    hub_user_view_per_models = ['ArticleFull', 'DealType', 'Currency', 'Country']

    for model in hub_user_view_per_models:
        GroupRoleModelPermission.create_or_update(group_id=hub_instance.pk,
                                                  role_id=user_role_instance.pk,
                                                  model_name=model,
                                                  permission=view_only_permission)

    logger.info("Configured Permission for Hub USER")

    logger.info("Adding permission for all User role")

    all_groups = Group.objects.exclude(pk=hub_instance.pk)
    user_role_view_create_edit_delete_models = ['AuthorAPCFundRequest']
    user_view_only_models = ['ArticleFull']
    for model_name in user_role_view_create_edit_delete_models:
        for group in all_groups:
            GroupRoleModelPermission.create_or_update(group_id=group.pk,
                                                      role_id=user_role_instance.pk,
                                                      model_name=model_name,
                                                      permission=view_create_edit_delete_permission)

    for model_name in user_view_only_models:
        for group in all_groups:
            GroupRoleModelPermission.create_or_update(group_id=group.pk,
                                                      role_id=user_role_instance.pk,
                                                      model_name=model_name,
                                                      permission=view_only_permission)

    logger.info("Configured Permission for role USER")

    ############################################# ============================== #####################################################
    ############################################# Hub Configuration end  #####################################################
    ############################################# ============================== #####################################################

    ############################################# ============================== #####################################################
    ############################################# All Approve permission start  #####################################################
    ############################################# ============================== #####################################################

    logger.info("Creating Approve Reject Permission")

    uni_admin_view_approve_reject_permission_models = ['OADeal']

    for model in uni_admin_view_approve_reject_permission_models:  # University Admin
        GroupRoleModelPermission.create_or_update(group_id=university_instance.pk,
                                                  role_id=admin_role_instance.pk,
                                                  model_name=model,
                                                  permission=view_approve_reject_permission)

    pub_admin_view_approve_reject_permission_models = ['DepositRequest','CorrectionRequest']

    for model in pub_admin_view_approve_reject_permission_models:  # Publisher Admin
        GroupRoleModelPermission.create_or_update(group_id=publisher_instance.pk,
                                                  role_id=admin_role_instance.pk,
                                                  model_name=model,
                                                  permission=view_approve_reject_permission)

    logger.info("Configured Approve Reject Permission")

    ############################################# ============================== #####################################################
    ############################################# All Approve permission end  #####################################################
    ############################################# ============================== #####################################################
