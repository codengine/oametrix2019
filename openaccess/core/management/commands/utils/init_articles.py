import logging
from django.db import transaction
from django.db.models.query_utils import Q
from openaccess.models.article.content_type import ContentType

logger = logging.getLogger('core')


def init_content_types():
    content_types = ["Comentary", "Conference Paper", "Encyclopedia", "Monograph", "Research Article", ]
    ct_include_ids = []
    logger.info("Creating roles")
    with transaction.atomic():
        for type_name in content_types:
            logger.info("Creating content type : %s" % type_name)
            instance = ContentType.create_of_update(name=type_name)
            ct_include_ids += [instance.pk]

        ContentType.objects.filter(~Q(pk__in=ct_include_ids)).delete()
