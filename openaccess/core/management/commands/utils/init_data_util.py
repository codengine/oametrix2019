import os
import logging

from core.models.finance.currency import Currency
from core.openaccessio.file_manager import FileManager
from openaccess.models.contract.deal_type import DealType

logger = logging.getLogger('core')


def init_oa_deal_types():
    deal_types = ['pre-payment', 'read & publish']

    for deal_type in deal_types:
        dt_instances = DealType.objects.filter(name=deal_type)
        if not dt_instances.exists():
            dt_instance = DealType(name=deal_type)
        else:
            dt_instance = dt_instances.first()
        dt_instance.save()


def init_currencies():
    project_directory = os.path.abspath(".")
    data_directory = os.path.join(project_directory, "core", "data")

    logger.info("Initializing currencies")

    currency_data_file = os.path.join(data_directory, "currencies.json")
    json_data = FileManager.read_json(currency_data_file)

    for currency_code, item in json_data.items():
        short_name = currency_code
        name = item['name']
        symbol = item['symbol']
        currency_instances = Currency.objects.filter(short_name=short_name)
        if currency_instances.exists():
            currency_instance = currency_instances.first()
        else:
            currency_instance = Currency()
            currency_instance.short_name = short_name

        currency_instance.name = name
        currency_instance.symbol = symbol
        currency_instance.save()


