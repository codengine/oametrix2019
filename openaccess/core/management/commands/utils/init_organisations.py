import logging
from django.db import transaction
from django.db.models.query_utils import Q

from core.models.auth.group import Group
from core.models.organisation.organisation import Organisation

logger = logging.getLogger('core')


def init_organisations():
    logger.info("Creating Organisation Groups")

    groups = ["Hub", "Publisher", "University", "Funder", "Research"]
    include_ids = []

    with transaction.atomic():
        for group in groups:
            logger.info("Creating group: %s" % group)
            instance = Group.create_of_update(name=group)
            include_ids += [instance.pk]

        Group.objects.filter(~Q(pk__in=include_ids)).delete()

    logger.info("Groups created")

    logger.info("Creating HUB Organisation")
    hub_group_instance = Group.objects.filter(name='Hub').first()

    organisation_instances = Organisation.objects.filter(name='Hub Organisation')
    if organisation_instances.exists():
        hub_org = organisation_instances.first()
    else:
        hub_org = Organisation()
    hub_org.name = 'Hub Organisation'
    hub_org.email_domain = 'hub.pub.us'
    hub_org.domain_id = hub_group_instance.pk
    hub_org.is_active = True
    hub_org.save()

    logger.info("Creating Organisation")

    # Creating Sample Real Publisher

    publisher_group = Group.objects.filter(name='Publisher').first()
    university_group = Group.objects.filter(name='University').first()

    org_list = ['Harvard University Press', 'Taylor and Francis Group', 'Thames & Hudson', 'The Jama Network',
                'Magination Press', 'Beicon Press']

    for org_name in org_list:
        organisation_instances = Organisation.objects.filter(name=org_name)
        if organisation_instances.exists():
            org = organisation_instances.first()
        else:
            org = Organisation()
        org.name = org_name
        org.email_domain = org_name.lower()[:3] + '.pub.us'
        org.domain_id = publisher_group.pk
        org.is_active = True
        org.save()

    org_list = ['Adams State University', 'Asbury University', 'Cabrini University', 'Eastern Arizona College',
                'Gage Academy of Art']

    for org_name in org_list:
        organisation_instances = Organisation.objects.filter(name=org_name)
        if organisation_instances.exists():
            org = organisation_instances.first()
        else:
            org = Organisation()
        org.name = org_name
        org.email_domain = org_name.lower()[:3] + '.uni.us'
        org.domain_id = university_group.pk
        org.is_active = True
        org.save()
