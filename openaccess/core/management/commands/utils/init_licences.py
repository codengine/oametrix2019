import logging
from django.db import transaction
from django.db.models.query_utils import Q
from openaccess.models.policy.licence import Licence

logger = logging.getLogger('core')


def init_licence():
    names = ["CC-BY", "CC-BY-NC", "CC-BY-ND"]
    include_ids = []
    logger.info("Creating Licences")
    with transaction.atomic():
        for name in names:
            instance = Licence.create_of_update(name=name, description= name+" descripton")
            include_ids += [instance.pk]

        Licence.objects.filter(~Q(pk__in=include_ids)).delete()
