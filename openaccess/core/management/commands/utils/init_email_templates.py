from core.models.mail.email_base import EmailBase


def init_email_templates():
    email_body = '<strong> Welcome to OaMetrix! </strong> <p>You’ve joined OaMetrix successfully.' \
                 ' Please click given link below to activate your account.</p><p> Thank you </p>'

    email_type = 'registration'
    email_template_instances = EmailBase.objects.filter(type=email_type)
    if email_template_instances.exists():
        email_template_instance = email_template_instances.first()
    else:
        email_template_instance = EmailBase(type=email_type, email_body=email_body)
    email_template_instance.email_body = email_body
    email_template_instance.save()



    # saving forget password template

    forget_template_body = '<strong> Hi, OaMetrix is always ready to help you! </strong> <p> You have requested to reset your password. Please click given link below to reset your password.</p><p> Thank you </p>'
    email_type_2 = 'reset password'

    forget_template_instances = EmailBase.objects.filter(type=email_type_2)
    if forget_template_instances.exists():
        forget_template_instance = forget_template_instances.first()
    else:
        forget_template_instance = EmailBase(type=email_type_2, email_body=forget_template_body)
    forget_template_instance.email_body = forget_template_body
    forget_template_instance.save()