import logging
from django.db import transaction
from django.db.models.query_utils import Q

from core.models.auth.role import Role

logger = logging.getLogger('core')

def init_roles():
    roles = ["SuperAdmin", "Admin", "User"]
    include_ids = []
    with transaction.atomic():
        for role in roles:
            logger.info("Creating role: %s" % role)
            instance = Role.create_of_update(name=role)
            include_ids += [instance.pk]

        Role.objects.filter(~Q(pk__in=include_ids)).delete()
