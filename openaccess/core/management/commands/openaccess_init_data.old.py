import os
import json
import logging
from django.db import transaction
from django.db.models.query_utils import Q
from django.core.management.base import BaseCommand
from core.management.commands.utils.init_data_util import init_oa_deal_types, init_currencies
from core.models.auth.group_role_model_permission import GroupRoleModelPermission
from core.models.contacts.country import Country
from core.models.auth.group import Group
from core.models.auth.role import Role
from core.models.auth.oapermission import OAPermission
from core.models.auth.user import User
from core.models.organisation.organisation import Organisation
from engine.library.app_util import get_all_models
from core.models.mail.email_base import EmailBase
from core.models.contacts.state import State
from core.models.contacts.city import City
from openaccess.models.publication.journal_type import JournalType
from openaccess.models.article.content_type import ContentType
from openaccess.models.publication.publication_type import PublicationType

__author__ = 'Sohel'

logger = logging.getLogger('core')


class Command(BaseCommand):
    def handle(self, *args, **options):
        logger.info("Running init data")
        project_directory = os.path.abspath(".")
        data_directory = os.path.join(project_directory, "core", "data")

        logger.info("Initializing Countries")

        country_data_file = os.path.join(data_directory, "countries.json")
        Country.load_from_json(country_data_file)

        logger.info("Country initialized")

        logger.info("Initializing State and City")
        countries = Country.objects2.all()[:3]

        for country in countries:
            for i in range(3):
                state_name = country.name + ' state' + str(i)
                state_instances = State.objects.filter(name=state_name)
                if state_instances.exists():
                    state = state_instances.first()
                else:
                    state = State()
                state.name = state_name
                state.short_name = state_name[4:7]
                state.is_active = True
                if i > 0:
                    state.parent_id = i
                state.save()

        states = State.objects2.all()[:3]

        for state in states:
            for j in range(3):
                city_name = state.name + ' city' + str(j)
                city_instances = City.objects.filter(name=city_name)
                if city_instances.exists():
                    city = city_instances.first()
                else:
                    city = City()
                city.name = city_name
                city.short_name = city_name[4:]
                city.is_active = True
                if j > 0:
                    city.parent_id = j
                city.save()

        logger.info("State and City Initialized")

        logger.info("Creating Organisation Groups")

        groups = ["Hub", "Publisher", "University"]
        include_ids = []

        with transaction.atomic():
            for group in groups:
                logger.info("Creating group: %s" % group)
                instance = Group.create_of_update(name=group)
                include_ids += [instance.pk]

            Group.objects.filter(~Q(pk__in=include_ids)).delete()

        logger.info("Groups created")

        logger.info("Creating Organisation")

        # Creating Sample Real Publisher

        publisher_group = Group.objects.filter(name='Publisher').first()
        university_group = Group.objects.filter(name='University').first()

        org_list = ['Harvard University Press', 'Taylor and Francis Group', 'Thames & Hudson', 'The Jama Network',
                    'Magination Press', 'Beicon Press']

        for org_name in org_list:
            organisation_instances = Organisation.objects.filter(name=org_name)
            if organisation_instances.exists():
                org = organisation_instances.first()
            else:
                org = Organisation()
            org.name = org_name
            org.email_domain = org_name.lower()[:3] + '.pub.us'
            org.domain_id = publisher_group.pk
            org.is_active = True
            org.save()

        org_list = ['Adams State University', 'Asbury University', 'Cabrini University', 'Eastern Arizona College',
                    'Gage Academy of Art']

        for org_name in org_list:
            organisation_instances = Organisation.objects.filter(name=org_name)
            if organisation_instances.exists():
                org = organisation_instances.first()
            else:
                org = Organisation()
            org.name = org_name
            org.email_domain = org_name.lower()[:3] + '.uni.us'
            org.domain_id = university_group.pk
            org.is_active = True
            org.save()

        logger.info("Organisation Created.")

        roles = ["SuperAdmin", "Admin", "User"]
        include_ids = []
        logger.info("Creating roles")
        with transaction.atomic():
            for role in roles:
                logger.info("Creating role: %s" % role)
                instance = Role.create_of_update(name=role)
                include_ids += [instance.pk]

            Role.objects.filter(~Q(pk__in=include_ids)).delete()

        logger.info("Roles created")

        logger.info("Creating Journal type ")

        journal_types = ["Gold", "Hybrid", "Platinum"]
        j_include_ids = []
        logger.info("Creating roles")
        with transaction.atomic():
            for type_name in journal_types:
                logger.info("Creating Journal type : %s" % type_name)
                instance = JournalType.create_of_update(name=type_name)
                j_include_ids += [instance.pk]

            JournalType.objects.filter(~Q(pk__in=j_include_ids)).delete()

        logger.info("Journal type  Created")

        logger.info("Creating Publication type ")

        publication_types = ["Book", "Journal"]
        pt_include_ids = []
        logger.info("Creating publication types")
        with transaction.atomic():
            for type_name in publication_types:
                logger.info("Creating publication type : %s" % type_name)
                instance = PublicationType.create_of_update(name=type_name)
                pt_include_ids += [instance.pk]

            PublicationType.objects.filter(~Q(pk__in=pt_include_ids)).delete()

        logger.info("Publication type  Created")

        logger.info("Creating ContentType ")

        content_types = ["Comentary", "Conference Paper", "Encyclopedia", "Monograph", "Research Article", ]
        ct_include_ids = []
        logger.info("Creating roles")
        with transaction.atomic():
            for type_name in content_types:
                logger.info("Creating content type : %s" % type_name)
                instance = ContentType.create_of_update(name=type_name)
                ct_include_ids += [instance.pk]

            ContentType.objects.filter(~Q(pk__in=ct_include_ids)).delete()

        logger.info("ContentType Created")

        super_admin_instance = Role.objects.get(name="SuperAdmin")

        logger.info("Creating Permissions")

        # [View Create Update Delete]  as [1111]

        permissions = [
            ("Create", "0100"),
            ("View", "1000"),
            ("Create-View", "1100"),
            ("Update", "0010"),
            ("Create-Update", "0110"),
            ("View-Update", "1010"),
            ("View-Create-Update", "1110"),
            ("Delete", "0001"),
            ("Create-Delete", "0101"),
            ("View-Delete", "1001"),
            ("View-Create-Delete", "1101"),
            ("Update-Delete", "0011"),
            ("Create-Delete-Update", "0111"),
            ("View-Update-Delete", "1011"),
            ("Create-View-Update-Delete", "1111")
        ]
        include_ids = []
        with transaction.atomic():
            for (perm_name, perm_code) in permissions:
                logger.info("Creating permission: %s/%s" % (perm_name, perm_code))
                instance = OAPermission.create_of_update(name=perm_name, perm_code=perm_code)
                include_ids += [instance.pk]
            OAPermission.objects.filter(~Q(pk__in=include_ids)).delete()

        logger.info("Permissions created")

        logger.info("Creating the monarchy super admin")

        username = "monarch"
        email = "monadmin@gmail.com"
        user_instances = User.objects.filter(username=username, email=email)
        if user_instances.exists():
            user_instance = user_instances.first()
        else:
            user_instance = User()
        user_instance.username = username
        user_instance.email = email
        user_instance.set_password("2019")
        user_instance.salute = "Mr"
        user_instance.first_name = "Mon"
        user_instance.middle_name = "Admin"
        user_instance.last_name = "Mon"
        user_instance.is_active = True
        user_instance.group_id = 1
        user_instance.role_id = super_admin_instance.pk
        user_instance.organisation_id = 1
        user_instance.save()
        logger.info("Super admin created: %s" % user_instance)

        logger.info("Creating group role model permission")

        logger.info("Setting permission for Hub group")
        # group_instance
        logger.info("Setting permission Hub -> SuperAdmin role")
        # super_admin_instance
        app_models = get_all_models()

        group_instance = Group.objects.get(name='Hub')
        permission = OAPermission.objects.get(perm_code='1111')
        for app, models in app_models.items():
            for model in models:
                model_name = model.__name__
                logger.info("%s for group: %s, role: %s, model: %s" % (permission.name, group_instance.name,
                                                                       super_admin_instance.name, model_name))
                GroupRoleModelPermission.create_of_update(group_id=group_instance.pk,
                                                          role_id=super_admin_instance.pk,
                                                          model_name=model_name,
                                                          permission_id=permission.pk)

        # creating Email seed data
        email_body = '<strong> Welcome to OaMetrix! </strong> <p>You’ve joined OaMetrix successfully.' \
                     ' Please click given link below to activate your account.</p><p> Thank you </p>'
        email_type = 'registration'
        email_obj = EmailBase(type=email_type, email_body=email_body)
        email_obj.save()
        logger.info("Email Base for Registration Created.")

        logger.info("Permission configured")

        # =========================== Init OA Deal Types ===================================#
        init_oa_deal_types()
        # =========================== End Init OA Deal Types ===================================#

        # =========================== Init currencies ===================================#
        init_currencies()
        # =========================== End Init currencies ===================================#
