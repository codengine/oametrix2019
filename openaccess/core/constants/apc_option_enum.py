from enum import Enum


class APCOptionEnum(Enum):
    DEPOSIT_CREDIT = "DepositCredit"
    OFFSET_FUND = "OffsetFund"
    OATOKEN = "OAToken"
