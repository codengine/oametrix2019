from enum import Enum


class ViewAction(Enum):
    LIST = "list"
    CREATE = "create"
    UPDATE = "update"
    DELETE = "delete"
    UPLOAD = "upload"
    DOWNLOAD = "download"
    RETRIEVE = "retrieve"
    APPROVE = "approve"
    REJECT = "reject"
    MUTATE = "mutate"

    @classmethod
    def all(cls):
        return [
            cls.LIST.value,
            cls.CREATE.value,
            cls.UPDATE.value,
            cls.DELETE.value,
            cls.UPLOAD.value,
            cls.DOWNLOAD.value,
            cls.RETRIEVE.value,
            cls.APPROVE.value,
            cls.REJECT.value,
            cls.MUTATE.value
        ]
