from enum import Enum


class TransactionType(Enum):
    DEBIT = "DEBIT"
    CREDIT = "CREDIT"
    REVERSE_DEBIT = "REVERSE_DEBIT"
    REVERSE_CREDIT = "REVERSE_CREDIT"
