from enum import Enum


class DealTypeEnum(Enum):
    PRE_PAYMENT = "pre-payment"
    READ_AND_PUBLISH ='read & publish'
