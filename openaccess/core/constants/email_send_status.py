from enum import Enum


class EmailSendStatus(Enum):
    PENDING = "pending"
    SUCCESS = "success"
    FAILED = "failed"
