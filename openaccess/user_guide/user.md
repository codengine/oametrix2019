# OpenAccess Restfull API

## Base Url: http://127.0.0.1:8000/api/v1/  

## USER endpoints

| Endpoint name |  Link  | Method |  Purpose | DOC-CHECK |
|---|---|---|---|---|
|  User Registration | /registration |POST | registration for a new account | OK |
|  Active Account | /active-account |POST | For account activation| OK |
|  User Login | /login |POST | account login | OK |
|  User Logout| /logout|POST | account logout| OK |
|  Create User  | /user |POST | creating user like Publisher creating users| OK | 
|  All User  | /user |GET | user list | OK | 
|  Update User  | /user |PUT | updating user info| OK | 
|  Delete User  | /user |DELETE | For deleting single user | OK | 


## 1. Registration ✅

**Request**

```
POST /registration/
```

**Parameters**

Name|Type|Description
:-:|:-:|:-:
`username`|`string`|user email address as username
`email`|`string`|user email address
`password`|`string`|user password
`group`|`string`| group id
`role`|`string`| role id
`organisation`|`string`| organisation id
`salute`|`string`| salute 
`first_name`|`string`| user first name 
`middle_name`|`string`| user middle name 
`last_name`|`string`| user last name 

**Example**

```
curl -X POST \
  http://127.0.0.1:8000/api/1.1/registration/ \
  -H 'content-type: application/json\
  -F username=ashique00006@gmail.com \
  -F email=ashique00006@gmail.com \
  -F password=admin123123\
  -F group=1 \
  -F role=1 \
  -F organisation=1 \
  -F salute=string \
  -F first_name=Inzamamul \
  -F middle_name=Haque \
  -F last_name=Ashique
```

### Success Status : HTTP_201_CREATED
**Response**

```
{
    "user": {
        "username": "ashique0004@gmail.com",
        "organisation_id": 1,
        "role_name": "Admin",
        "role_id": 2,
        "id": 7,
        "email": "ashique0004@gmail.com",
        "organisation_name": "Hub Organisation",
        "group_name": "Publisher",
        "group_id": 2
    },
    "success": true
}
```

**Possible errors**

```
{
    "password": [
        "This field is required."
    ],
    "email": [
        "This field is required."
    ],
    "username": [
        "This field is required."
    ],
    "group": [
        "This field is required."
    ],
    "role": [
        "This field is required."
    ]
}
```

**Possible errors status**
1. HTTP_400_BAD_REQUEST



## 2. Active Account ✅

**Request**

```
POST /active-account/
```

**Parameters**

Name|Type|Description
:-:|:-:|:-:
`security_code`|`string`| security code given by registration 

**Example**

```
curl -X POST \
  http://127.0.0.1:8000/api/1.1/active-account/ \
  -H 'Content-Type: application/json' \ 
  -d '{
	"security_code":"48b4a41e-fe8e-440e-93e7-3a3702c37f27"
}'
```

### Success Status : HTTP_200_OK
**Response**

```
{
    "message": "Account successfully activated.",
    "success": true
}
```

**Possible errors**

```
{
    "message": "Invalid security code or Session expired.",
    "success": false
}

OR

{
    "message": "Session expired.",
    "success": false
}
```

**Possible errors status**
1. HTTP_400_BAD_REQUEST



## 3. Login ✅

**Request**

```
POST /login/
```

**Parameters**

Name|Type|Description
:-:|:-:|:-:
`username`|`string`|user email address as username
`email`|`string`|user email address

**Example**

```
curl -X POST \
  http://127.0.0.1:8000/api/1.1/login/ \
  -H 'Content-Type: application/json' \ 
  -d '{
"username":"ashique00003@gmail.com", 
"password":"admin123123"
}'
```

### Success Status : HTTP_200_OK
**Response**
  
```
{
    "user": {
        "username": "ashique00003@gmail.com",
        "organisation_id": 1,
        "role_name": "Admin",
        "role_id": 2,
        "id": 3,
        "email": "ashique00003@gmail.com",
        "organisation_name": "Hub Organisation",
        "group_name": "Publisher",
        "group_id": 2
    },
    "token": "76e9cb32a35fa8edf2d0238186f6a42816a21293",
    "success": true
}
```
**Possible errors**

```
{
    "success": false,
    "message": "Cannot login with provided credentials."
}
```

**Possible errors status**
1. HTTP_400_BAD_REQUEST


## 4. Logout ✅
### Auth header required (Token)

**Request**

```
POST /logout/
```


**Parameters**

Name|Type|Description
:-:|:-:|:-: 

**Example**

```
curl -X POST \
  http://127.0.0.1:8000/api/1.1/logout/  
```

### Success Status : HTTP_200_OK
**Response**
  
```
{
 
}
```
**Possible errors**

```
{
    "detail": "Invalid token."
}
```

**Possible errors status**
1. HTTP_401_UNAUTHORIZED


## 4. Create User ✅
### Auth header required (Token)

**Request**

```
POST /user/
```

**Parameters**

Name|Type|Description
:-:|:-:|:-:
`username`|`string`|user email address as username
`email`|`string`|user email address
`password`|`string`|user password
`group`|`string`| group id
`role`|`string`| role id
`organisation`|`string`| organisation id
`salute`|`string`| salute 
`first_name`|`string`| user first name 
`middle_name`|`string`| user middle name 
`last_name`|`string`| user last name 
`domain_meta`|`array`| Domain Meta
`user_meta`|`array`| User Meta (orcid_id, pmc_id, unique_id)


**Example**

```
curl -X POST \
  http://127.0.0.1:8000/api/v1/user/ \
  -H 'Authorization: token 76e9cb32a35fa8edf2d0238186f6a42816a21293' \
  -H 'Content-Type: application/json' \ 
  -d '{
"username":"ashique00007@gmail.com",
"email":"ashique00007@gmail.com",
"password":"admin123123",
"group":2,
"role":2,
"organisation":1,
"salute":"string",
"first_name":"Inzamamul",
"middle_name":"Haque",
"last_name":"Ashique",
"segment":1, 
"user_meta": {
	"orcid_id":"55886",
	"pmc_id":"85562",
	"unique_id":"456789"
}	
}'
```

### Success Status : HTTP_200_OK
**Response**
  
```
{
    "id": 9,
    "username": "ashique00007@gmail.com",
    "email": "ashique00007@gmail.com",
    "group": 2,
    "role": 2,
    "organisation": 1,
    "salute": "string",
    "first_name": "Inzamamul",
    "middle_name": "Haque",
    "last_name": "Ashique",
    "domain_meta": null,
    "user_meta": {
        "id": 1,
        "orcid_id": "55886",
        "pmc_id": "85562",
        "unique_id": "456789"
    },
    "addresses": []
}
```
**Possible errors**

```
{
    "username": [
        "This field is required."
    ],
    "group": [
        "This field is required."
    ],
    "email": [
        "This field is required."
    ],
    "password": [
        "This field is required."
    ],
    "role": [
        "This field is required."
    ]
}
```

**Possible errors status**
1. HTTP_400_BAD_REQUEST



## 5. Get User List ✅
### Auth header required (Token)

**Request**

```
GET /user/
```

**Parameters**

Name|Type|Description
:-:|:-:|:-:


**Example**

```
curl -X GET \
  http://127.0.0.1:8000/api/v1/user/ 
```

### Success Status : HTTP_200_OK
**Response**
  
```
{
    "count": 5,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 1,
            "username": "monarch",
            "email": "monadmin@gmail.com",
            "group": 1,
            "role": 1,
            "organisation": 1,
            "salute": "Mr",
            "first_name": "Mon",
            "middle_name": "Admin",
            "last_name": "Mon",
            "domain_meta": null,
            "user_meta": null,
            "addresses": []
        },
        {
            "id": 3,
            "username": "ashique00003@gmail.com",
            "email": "ashique00003@gmail.com",
            "group": 2,
            "role": 2,
            "organisation": 1,
            "salute": "string",
            "first_name": "Inzamamul",
            "middle_name": "Haque",
            "last_name": "Ashique",
            "domain_meta": null,
            "user_meta": null,
            "addresses": []
        } 
    ]
}
```
**Possible errors**

```
[]
```

**Possible errors status**
1. HTTP_400_BAD_REQUEST


## 6. Get single User ✅
### Auth header required (Token)

**Request**

```
GET /user/1/
```

**Parameters**

Name|Type|Description
:-:|:-:|:-: 
**Example**

```
curl -X GET \
  http://127.0.0.1:8000/api/v1/user/1/
```

### Success Status : HTTP_200_OK
**Response**
  
```
{
    "id": 1,
    "username": "monarch",
    "email": "monadmin@gmail.com",
    "group": 1,
    "role": 1,
    "organisation": 1,
    "salute": "Mr",
    "first_name": "Mon",
    "middle_name": "Admin",
    "last_name": "Mon",
    "domain_meta": null,
    "user_meta": null,
    "addresses": []
}
```
**Possible errors**

```
 {
    "detail": "Not found."
}
```

**Possible errors status**
1. HTTP_404_NOT_FOUND



## 7. Update User ✅
### Auth header required (Token)

**Request**

```
PUT /user/1/
```

**Parameters**

Name|Type|Description
:-:|:-:|:-:
`username`|`string`|user email address as username
`email`|`string`|user email address
`password`|`string`|user password
`group`|`string`| group id
`role`|`string`| role id
`organisation`|`string`| organisation id
`salute`|`string`| salute 
`first_name`|`string`| user first name 
`middle_name`|`string`| user middle name 
`last_name`|`string`| user last name 
`domain_meta`|`array`| Domain Meta
`user_meta`|`array`| User Meta (orcid_id, pmc_id, unique_id)


**Example**

```
curl -X PUT \
  http://127.0.0.1:8000/api/v1/user/1/ \
  -H 'Authorization: token 76e9cb32a35fa8edf2d0238186f6a42816a21293' \
  -H 'Content-Type: application/json'  
  -d '{
"username":"ashique00009@gmail.com",
"email":"ashique00009@gmail.com",
"password":"admin123123",
"group":2,
"role":2,
"organisation":1,
"salute":"string",
"first_name":"Inzamamul",
"middle_name":"Haque",
"last_name":"Ashique",
"segment":1,
"title":"A sample Domain meta title",
"position":"position",
"home_tel":"+8801726555438",
"office_tel":"8801926555788",
"alt_tel":"+88098665544",
"user_meta": {
	"orcid_id":"55886",
	"pmc_id":"85562",
	"unique_id":"456789"
}
	
}'
```

### Success Status : HTTP_200_OK
**Response**
  
```
{
    "id": 9,
    "username": "ashique00007@gmail.com",
    "email": "ashique00007@gmail.com",
    "group": 2,
    "role": 2,
    "organisation": 1,
    "salute": "string",
    "first_name": "Inzamamul",
    "middle_name": "Haque",
    "last_name": "Ashique",
    "domain_meta": null,
    "user_meta": {
        "id": 1,
        "orcid_id": "55886",
        "pmc_id": "85562",
        "unique_id": "456789"
    },
    "addresses": []
}
```
**Possible errors**

```
{
    "email": [
        "user with this email address already exists."
    ],
    "username": [
        "user with this username already exists."
    ]
}
```

or 
``` 
{
    "detail": "Not found."
}
```

**Possible errors status**
1. HTTP_400_BAD_REQUEST



## 8. Delete single User ✅
### Auth header required (Token)

**Request**

```
GET /user/1/
```

**Parameters**

Name|Type|Description
:-:|:-:|:-: 

**Example**

```
curl -X DELETE \
  http://127.0.0.1:8000/api/v1/user/1/
```

### Success Status : HTTP_204_NO_CONTENT
**Response**
  
```
{
   
}
```

**Possible errors**

```
{
    "detail": "Not found."
}
```

**Possible errors status**
1. HTTP_404_NOT_FOUND


