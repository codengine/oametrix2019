# OpenAccess Restfull API

## Base Url: http://127.0.0.1:8000/api/v1/  

## Role endpoints

| Endpoint name |  Link  | Method |  Purpose | DOC-CHECK |
|---|---|---|---|---|
|  Role | /role |POST | Creating an Role | OK | 
|  Role | /role |GET | Listing all Role | OK | 
|  Role | /role/<int:id> |GET | Single Role | OK | 
|  Role | /role/<int:id> |DELETE | Delete Role | OK | 
|  Role | /role/<int:id> |PUT | Update Role | OK | 



## 1. Role ✅
### Auth header required (Token)

**Request**

```
POST /role/
```

**Parameters**

Name|Type|Description | IsRequired
:-:|:-:|:-:
`name`|`string`| role name | true  
`is_active`|`boolean`| active status | true  

**Example**

```
curl -X POST \
  http://127.0.0.1:8000/api/v1/role/ \
  -H 'Content-Type: application/json' \ 
  -H 'cache-control: no-cache' \
  -d '{
    "name": "Test Role 5",
    "is_active": true    
}'

```

### Success Status : HTTP_201_CREATED
**Response**

```
{
    "id": 13,
    "name": "Test Role",
    "is_active": true
}
```
  
**Possible errors**

```
{
    "name": [
        "This field is required."
    ]
}
```

**Possible errors status**
1. HTTP_400_BAD_REQUEST


## 2. Role ✅
### Auth header required (Token)

**Request**

```
GET /role/
```

**Parameters**

Name|Type|Description | IsRequired
:-:|:-:|:-: 


**Example**

```
curl -X GET \
  http://127.0.0.1:8000/api/v1/role/ \ 
  -H 'cache-control: no-cache'
```

### Success Status : HTTP_200_OK
**Response**

```
{
    "count": 8,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 1,
            "name": "SuperAdmin",
            "is_active": true
        },
        {
            "id": 2,
            "name": "Test Role 6",
            "is_active": true
        } 
    ]
}
```
  
**Possible errors**

```
 
```

**Possible errors status** 




## 3. Single Role ✅
### Auth header required (Token)

**Request**

```
GET /role/4/
```

**Parameters**

Name|Type|Description | IsRequired
:-:|:-:|:-: 


**Example**

```
curl -X GET \
  http://127.0.0.1:8000/api/v1/role/4/ 
  -H 'cache-control: no-cache'
```

### Success Status : HTTP_200_OK
**Response**

```
    {
        "id": 2,
        "name": "Test Role 6",
        "is_active": true
    } 
```
  
**Possible errors**

```
{
    "detail": "Not found."
}
```

**Possible errors status** 
1. HTTP_404_NOT_FOUND




## 4. Single Role Update ✅
### Auth header required (Token)

**Request**

```
GET /role/4/
```

**Parameters**

Name|Type|Description | IsRequired
:-:|:-:|:-: 


**Example**

```
curl -X PUT \
  http://127.0.0.1:8000/api/v1/role/7/ \
  -H 'Content-Type: application/json' \ 
  -H 'cache-control: no-cache' \
  -d '{
    "name": "Test Role 6"
}'
```

### Success Status : HTTP_200_OK
**Response**

```
    {
        "id": 2,
        "name": "Test Role 6",
        "is_active": true
    } 
```
  
**Possible errors**

```
{
    "detail": "Not found."
}
```

**Possible errors status** 
1. HTTP_404_NOT_FOUND



## 5. Single Role DELETE ✅
### Auth header required (Token)

**Request**

```
DELETE /role/4/
```

**Parameters**

Name|Type|Description | IsRequired
:-:|:-:|:-: 


**Example**

```
curl -X DELETE \
  http://127.0.0.1:8000/api/v1/role/7/  
```

### Success Status : HTTP_204_NO_CONTENT
**Response**

```
{
    
}
```
  
**Possible errors**

```
{
    "detail": "Not found."
}
```

**Possible errors status** 
1. HTTP_404_NOT_FOUND


