# OpenAccess Restfull API

## Base Url: http://127.0.0.1:8000/api/v1/  

## Group endpoints

| Endpoint name |  Link  | Method |  Purpose | DOC-CHECK |
|---|---|---|---|---|
|  Group | /group |POST | Creating an Group | OK | 
|  Group | /group |GET | Listing all Group | OK | 
|  Group | /group/<int:id> |GET | Single group | OK | 
|  Group | /group/<int:id> |DELETE | Delete group | OK | 
|  Group | /group/<int:id> |PUT | Update group | OK | 



## 1. Group ✅
### Auth header required (Token)

**Request**

```
POST /group/
```

**Parameters**

Name|Type|Description | IsRequired
:-:|:-:|:-:
`name`|`string`| group name | true 
`parent`|`string`| parent group | false 
`is_active`|`boolean`| active status | false 

**Example**

```
curl -X POST \
  http://127.0.0.1:8000/api/v1/group/ \
  -H 'Content-Type: application/json' \ 
  -d '{
	"name":"Test Group 3",
	"parent":30,
	"is_active":true
}

```

### Success Status : HTTP_201_CREATED
**Response**

```
{
    "id": 42,
    "name": "Test Group 3",
    "parent": 1,
    "is_active": false
}
```

or 

```
{
    "id": 42,
    "name": "Test Group 3",
    "parent": null,
    "is_active": false
}
```



**Possible errors**

```
{
    "detail": "Not found."
}
```

**Possible errors status**
1. HTTP_400_BAD_REQUEST



## 2. Group ✅
### Auth header required (Token)

**Request**

```
GET /group/
```

**Parameters**

Name|Type|Description | IsRequired
:-:|:-:|:-: 

**Example**

```
curl -X GET \
  http://127.0.0.1:8000/api/v1/group/  

```

### Success Status : HTTP_200_OK
**Response**

```
{
    "count": 4,
    "next": null,
    "previous": null,
    "results": [
        [
        {
            "id": 1,
            "name": "Hub",
            "parent": null,
            "is_active": true
        },
        {
            "id": 2,
            "name": "Publisher",
            "parent": null,
            "is_active": true
        }
    ]
}
```
 

**Possible errors**
```
 
```

**Possible errors status** 



## 3. Group ✅
### Auth header required (Token)

**Request**

```
GET /group/<int:id>/
```

**Parameters**

Name|Type|Description | IsRequired
:-:|:-:|:-: 

**Example**

```
curl -X GET \
  http://127.0.0.1:8000/api/v1/group/1/

```

### Success Status : HTTP_200_OK
**Response**

```     
{
    "id": 2,
    "name": "Publisher",
    "parent": null,
    "is_active": true
}
```
 

**Possible errors**

```
{
    "detail": "Not found."
}
```

**Possible errors status**
1. HTTP_404_NOT_FOUND



## 4. Group ✅
### Auth header required (Token)

**Request**

```
PUT /group/<int:id>/
```

**Parameters**

Name|Type|Description | IsRequired
:-:|:-:|:-: 

**Example**

```
curl -X PUT \
  http://127.0.0.1:8000/api/v1/group/37/ \
  -H 'Content-Type: application/json' \ 
  -H 'cache-control: no-cache' \
  -d '{
	"name":"Test Group updated",
	"parent":370
}'

```

### Success Status : HTTP_200_OK
**Response**


```
[
{
    "id": 1,
    "name": "Hub",
    "parent": null,
    "is_active": true
} 
```
 

**Possible errors**

```
{
    "detail": "Not found."
}
```

**Possible errors status**
1. HTTP_404_NOT_FOUND


## 5. Group ✅
### Auth header required (Token)

**Request**

```
DELETE /group/<int:id>/
```

**Parameters**

Name|Type|Description | IsRequired
:-:|:-:|:-: 

**Example**

```
curl -X DELETE \
  http://127.0.0.1:8000/api/v1/group/1/

```

### Success Status : HTTP_204_NO_CONTENT
**Response**

```
{
     
}
```
 

**Possible errors**

```
{
    "detail": "Not found."
}
```

**Possible errors status**
1. HTTP_404_NOT_FOUND

